import {useState, useEffect} from 'react';
import {useLocation, useParams, Link} from 'react-router-dom';

import axios from 'axios';

const Pagination = props => {

    const [data, setData] = useState([]);
    const {search} = useLocation();
    const page = new URLSearchParams(search).get('page');


    const itemsPerPage = 10;

    const [currentPage, setCurrentPage] = useState(page);
    const [total, setTotal] = useState(0);
    const [lastPage, setLastPage] = useState(0);
    const [nextPage, setNextPage] = useState(0);
    const [previousPage, setPreviousPage] = useState(0);

    const getPage = () => {


        axios.get(`http://localhost:3000/posts?_page=${currentPage}`).then(res => {

            const {data, headers} = res;

            //get total & last
            const _total = headers['x-total-count'];
            const _lastPage = _total / itemsPerPage;

            //get next
            const _nextHipotetical = Number(currentPage) + 1;
            const _nextPage = _nextHipotetical <= _lastPage ? _nextHipotetical : null;

            //get previous
            const _previousHipotetical = Number(currentPage) - 1;
            const _previousPage = _previousHipotetical >= 1 ? _previousHipotetical : null;


            setTotal(_total);
            setLastPage(_lastPage);
            setNextPage(_nextPage);
            setPreviousPage(_previousPage);
            setData(data);

        })
        .catch(err => {});
    }

    useEffect(
        () => {
            getPage();
        }, [currentPage]
    );

    return(
        <div className="p-6" key={props.keyId}>
            <div className="coolscroll hblue" style={{maxHeight: '500px', overflow: 'auto'}}>
            {
                data.map( (item, i) =>
                    <div className="box has-background-hblue-light" key={i}>
                        <h1 className="title has-text-centered has-text-hblue">{item.id}</h1>
                    </div>
                )
            }
            </div>

            {
                <div>
                    total: {total} <br/>
                    lastPage: {lastPage} <br/>
                    nextPage: {nextPage} <br/>
                    previousPage: {previousPage} <br/>


                </div>
            }

            <br/><br/><br/>

            <nav class="pagination is-small is-centered" role="navigation" aria-label="pagination">
                <Link to={`/pagination?page=${previousPage}`} class="pagination-previous" disabled={previousPage == null} onClick={e => setCurrentPage(previousPage)}>{'<'}</Link>
                <Link to={`/pagination?page=${nextPage}`} class="pagination-next" disabled={nextPage == null} onClick={e => setCurrentPage(nextPage)}>{'>'}</Link>
                <ul class="pagination-list">
                    <li><Link to={`/pagination?page=${previousPage}`} class={`pagination-link ${previousPage ? '' : 'is-hidden'}`} onClick={e => setCurrentPage(previousPage)}>{previousPage}</Link></li>
                    <li><a class="pagination-link is-current">{currentPage}</a></li>
                    <li><Link to={`/pagination?page=${nextPage}`} class={`pagination-link ${nextPage ? '' : 'is-hidden'}`} onClick={e => setCurrentPage(nextPage)}>{nextPage}</Link></li>
                </ul>
            </nav>
        </div>
    );
}

export default Pagination;
