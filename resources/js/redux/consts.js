export const OPEN_SIDEBAR = '1';
export const CLOSE_SIDEBAR = '2';

export const ADD_FILES = '3';

export const SET_MODAL_ON = 'SET_MODAL_ON';
export const SET_MODAL_OFF = 'SET_MODAL_OFF';
