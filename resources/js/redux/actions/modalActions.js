import {
    SET_MODAL_ON,
    SET_MODAL_OFF
} from '../consts';

export const set_modal_on = modalData => {
    return{
        type: SET_MODAL_ON,
        payload: modalData
    }
}

export const set_modal_off = () => {
    return{
        type: SET_MODAL_OFF
    }
}
