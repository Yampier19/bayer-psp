import {
    ADD_FILES
} from '../consts.js';

export const add_files = files => {
    return{
        type: ADD_FILES,
        payload: [...files]
    }
}
