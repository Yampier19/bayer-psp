import {
    SET_MODAL_ON,
    SET_MODAL_OFF
} from '../consts';

const icons = {
    CHECK_PRIMARY: "CHECK_PRIMARY",
    CHECK_HORANGE: "CHECK_HORANGE",
    CHECK_HRED: "CHECK_HRED",
    CHECK_HBLACK: "CHECK_HBLACK",

    WARNING_HRED: "WARNING_HRED",

    TRASH_CAN_HBLACK: "TRASH_CAN_HBLACK"
}

const types = {
    INFO: 'INFO',
    CONFIRM: 'CONFIRM',
    EMAIL: 'EMAIL'
}


const defaultState = {
    modalActive: false,
    modalData: null,
    _types: types,
    _icons: icons
};

const reducer = (state = defaultState, action) => {

    switch (action.type) {

        case SET_MODAL_ON:
            return{
                ...state,
                modalActive: true,
                modalData: action.payload
            };

        case SET_MODAL_OFF:
            return{
                ...state,
                modalActive: false,
                modalData: null
            };

        default:
            return {...state};
    }
};

export default reducer;
