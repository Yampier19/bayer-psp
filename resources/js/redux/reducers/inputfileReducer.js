import {
    ADD_FILES
} from '../consts.js';

const defaultState = {
    files: []
};

const reducer = (state = defaultState, action) => {

    switch (action.type) {

        case ADD_FILES:
            return{
                ...state,
                files: action.payload
            };

        default:
            return {...state};
    }
}

export default reducer;
