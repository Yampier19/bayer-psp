import {Link} from 'react-router-dom';

const MenuCard = props => {
    return(
        <Link className="box pt-0" to={props.to}>


            <figure className={`image is-48x48 has-background-${props.color}`}>
                <img src="https://img.utdstc.com/icon/f66/034/f66034fa23b58f44b4f01393ffcb2bc191788a8fa43bfd2b4ac034ef8af09dfb:200" style={{transform: 'scale(0.8)'}}/>
            </figure>
            <br/>

            <h1 className="subtitle has-text-black is-5">{props.title}</h1>
        </Link>
    );
}

export default MenuCard;
