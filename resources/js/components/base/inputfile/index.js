import {useState, useEffect, useRef} from 'react';

import './inputfile.scss';

import {
    IMAGE, VIDEO, PDF, EXCEL, DOC,
    ZIP, checkType} from './checktype';

const Inputfile = props => {

    const inputfile = useRef(null); //reference to html input

    const [files, setFiles] = useState([]);   //files selected


    const onDeleteClicked = e => {  //deletes a file from files array

        const elem = e.currentTarget;

        const index = elem.dataset.fileindex;

        let _files = [...files];

        if (index > -1){
            _files.splice(index, 1);
        }

        setFiles( [..._files] );

    }

    //recognize the type of file
    const onFileChanged = e => {    //add file to filesArray, and set the display icon

        const input = e.target;

        const ifiles = input.files;

        const _files = [];

        for(let i = 0; i < ifiles.length; i++){

            const type = checkType( ifiles[i] );

            const blob = new Blob([ ifiles[i] ]);
            const url  = URL.createObjectURL(blob);


            let display = () => null;

            switch (type) {
                case IMAGE:
                    display = () => <span className="icon preview-icon is-size-2 has-text-light2"><i className="far fa-image"></i></span>;
                    break;

                case VIDEO:
                    display = () => <span className="icon preview-icon is-size-2 has-text-light2"><i className="fas fa-film"></i></span>;
                    break;


                case PDF:
                    display = () => <span className="icon preview-icon is-size-2 has-text-light2"><i className="fas fa-file-pdf"></i></span>;
                    break;


                case EXCEL:
                    display = () => <span className="icon preview-icon is-size-2 has-text-light2"><i className="fas fa-file-excel"></i></span>;
                    break;

                case DOC:
                    display = () => <span className="icon preview-icon is-size-2 has-text-light2"><i className="fas fa-file-word"></i></span>;
                    break;

                case ZIP:
                    display = () => <span className="icon preview-icon is-size-2 has-text-light2"><i className="fas fa-file-archive"></i></span>;
                    break;

                default:
                    break;
            }




            _files.push({
                name: ifiles[i].name,
                display: display,
                blob: blob,
                url: url
            });
        }



        setFiles( _files );
    }

    return(
        <div className="mb-3">

            {/* input containng all allowed files */}
            <input id={`fileinput${props.id}`} type="file"
                ref={inputfile}
                onChange={onFileChanged}
                className="is-hidden"
                accept=".doc, .docx, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document,
                    .png, .jpeg, .gif,
                    application/pdf, .pdf,
                    .xlsx, .xls, .csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,
                    .zip,
                    .mov, .mp4 "
                multiple
            />

            { /* file displayer */ }
            <ul className="scrolldisplay py-2">
                {
                    files.map( (file, i) =>
                        <li className="box  p-0 mx-2 filee has-background-light" href={''/*file.url*/}  key={i}>
                            <div className="level is-mobile p-0 m-0">
                                <div className="level-left ">
                                    <a className="download-btn has-text-white" href={file.url} target="_blank" download={file.name}>
                                        <span className="icon is-size-6"><i class="fas fa-long-arrow-alt-down"></i></span>
                                    </a>
                                </div>
                                <div className="level-right">
                                    <button class="delete delete-btn" data-fileindex={i} onClick={onDeleteClicked}></button>
                                </div>

                            </div>

                            <figure class="display">
                                {file.display()}
                            </figure>

                            <div className="file-info has-text-centered py-0">
                                <h1 className="subtitle is-6 has-text-white">{file.name.length <= 6 ? file.name : file.name.substring(0, 6) + '...'  }</h1>
                            </div>
                        </li>

                    )
                }
            </ul>

        </div>
    );
}

export default Inputfile;
