/*
* recognize the type of @file
* params:
* @file : input.file 
*
*/

export const VIDEO = 'VIDEO';
export const IMAGE = 'IMAGE';
export const DOC = 'DOC';
export const PDF = 'PDF';
export const EXCEL = 'EXCEL';
export const ZIP = 'ZIP';

export function checkType(file) {

    const imageTypes = [
      "image/gif",
      "image/jpeg",
      "image/png"
    ];

    const mswordTypes = [
        ".doc",
        ".docx",
        "application/msword",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    ];

    const pdfTypes = [
        "application/pdf"
    ];

    const excelTypes = [
         "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    ];

    const zipType = [
        "application/x-zip-compressed"
    ];

    const videoType = [
        "video/mp4",
        "video/mov",
        "video/quicktime"
    ];


    if( imageTypes.includes(file.type) )
        return IMAGE;

    if( mswordTypes.includes(file.type) )
        return DOC;

    if( pdfTypes.includes(file.type) )
        return PDF;

    if( excelTypes.includes(file.type) )
        return EXCEL;

    if( zipType.includes(file.type) )
        return ZIP;

    if( videoType.includes(file.type) )
        return VIDEO;

    return -1
}
