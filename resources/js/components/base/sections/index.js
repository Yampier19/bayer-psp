import {useState, useEffect, useRef} from 'react';
import './sections.scss';

const Sections = props => {

    const form = useRef(null);
    const [tracking, setTracking] = useState(false);
    const [products, setProducts] = useState(false);
    const [reports, setReports] = useState(false);

    useEffect(
        () => {

            window.addEventListener('keyup', e => {
                if(e.which == 13)
                    submit();
            });

        }, []
    );

    const submit = e => {

        const elements = Array.from(form.current.elements);

        //get which elements are selected
        for (let i = 0; i < elements.length; i++) {
            if(elements[i].type == "checkbox"){
                if(elements[i].checked){
                    console.log(elements[i].name);
                }
            }
        }
    }

    const onChange = e => {

        const checkbox = e.currentTarget;
        const targetDivSelector = checkbox.dataset.target;

        const $targetDiv = document.querySelector(targetDivSelector);

        if(checkbox.checked)
            $targetDiv.classList.add('is-active');
        else
            $targetDiv.classList.remove('is-active');
    }


    return(
        <div className="sections p-3">
            <form id="form.sections" ref={form}>
                <ul>
                    <label>
                        <input className="checkbox-config" type="checkbox" name="psp" data-target="#sections" onChange={onChange}/> &nbsp;
                        Aplicativo PSP
                    </label>
                    <ul className="ml-5 sec-list" id="sections">
                        <li>
                            <label>
                                <input className="checkbox-config" type="checkbox" name="create"/> &nbsp;
                                Crear
                            </label>
                        </li>

                        <li>
                            <label>
                                <input className="checkbox-config" type="checkbox" name="news"/> &nbsp;
                                Novedades
                            </label>
                        </li>

                        <li>
                            <label>
                                <input className="checkbox-config" type="checkbox" name="tracking" data-target="#tracking" onChange={onChange} onClick={e => setTracking(e.target.checked)}/> &nbsp;
                                Seguimiento
                                &nbsp;

                            </label>

                            <a className="has-text-hblack">
                                <span className="icon has-background-light" style={{borderRadius: '50%'}}>
                                    <i class="far fa-eye"></i>
                                </span>
                            </a>
                            &nbsp;
                            <a className="has-text-hblack">
                                <span className="icon has-background-light" style={{borderRadius: '50%'}}>
                                    <i class="fas fa-edit"></i>
                                </span>
                            </a>

                            <ul className="ml-5 sec-list" id="tracking">
                                <li>
                                    <label>
                                        <input className="checkbox-config" type="checkbox" name="tracking.patient" checked={tracking}/> &nbsp;
                                        Paciente
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input className="checkbox-config" type="checkbox" name="tracking.tratment" checked={tracking}/> &nbsp;
                                        Tratamiento
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input className="checkbox-config" type="checkbox" name="tracking.patient" checked={tracking}/> &nbsp;
                                        Comunicaciones
                                    </label>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <label>
                                <input className="checkbox-config" type="checkbox" name="products" data-target="#products" onChange={onChange} onClick={e => setProducts(e.target.checked)}/> &nbsp;
                                Productos
                            </label>

                            <ul className="ml-5 sec-list" id="products">
                                <li>
                                    <label>
                                        <input className="checkbox-config" type="checkbox" name="products.record" checked={products}/> &nbsp;
                                        Registrar material
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input className="checkbox-config" type="checkbox" name="products.inventory"  data-target="#inventory" onChange={onChange} checked={products}/> &nbsp;
                                        Inventario
                                    </label>

                                    <ul className="ml-5 sec-list" id="inventory">
                                        <li>
                                            <label>
                                                <input className="checkbox-config" type="checkbox" name="products.inventory.movement" checked={products}/> &nbsp;
                                                Movimiento
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <input className="checkbox-config" type="checkbox" name="products.inventory.details" checked={products}/> &nbsp;
                                                Detalles inventario
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <input className="checkbox-config" type="checkbox" name="products.inventory.inventories" checked={products}/> &nbsp;
                                                Inventarios
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <input className="checkbox-config" type="checkbox" name="products.inventory.downloads" checked={products}/> &nbsp;
                                                Descargas
                                            </label>
                                        </li>
                                    </ul>
                                </li>

                            </ul>
                        </li>

                        <li>
                            <label>
                                <input className="checkbox-config" type="checkbox" name="reports" data-target="#reports" onChange={onChange} onClick={e => setReports(e.target.checked)}/> &nbsp;
                                Reportes
                            </label>

                            <ul className="ml-5 sec-list" id="reports">
                                <li>
                                    <label>
                                        <input className="checkbox-config" type="checkbox" name="reports.patients" checked={reports}/> &nbsp;
                                        Pacientes
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input className="checkbox-config" type="checkbox" name="reports.demands" checked={reports}/> &nbsp;
                                        Reclamación
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input className="checkbox-config" type="checkbox" name="reports.conciliation" checked={reports}/> &nbsp;
                                        Conciliación
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input className="checkbox-config" type="checkbox" name="reports.terapy" checked={reports}/> &nbsp;
                                        Terapias
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input className="checkbox-config" type="checkbox" name="reports.walls" checked={reports}/> &nbsp;
                                        Barreras
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input className="checkbox-config" type="checkbox" name="reports.terapy" checked={reports}/> &nbsp;
                                        Terapias
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input className="checkbox-config" type="checkbox" name="reports.samples" checked={reports}/> &nbsp;
                                        Muestras medicas
                                    </label>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <label>
                                <input className="checkbox-config" type="checkbox" name="configuration"/> &nbsp;
                                Configuración
                            </label>
                        </li>
                    </ul>
                </ul>
            </form>
        </div>
    );
}

export default Sections;
