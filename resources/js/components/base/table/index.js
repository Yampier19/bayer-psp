import {useEffect, useState, useRef} from 'react';

import './table.scss';

export const getIgnored = (dataStructure, formatColumns) => {

    const keys = Object.keys( dataStructure );
    let ignore = formatColumns.filter( col => !keys.includes(col.key) );
    ignore = ignore.map( col => col.key );
    return ignore;
}

const Table = props => {

    const header = useRef(null);
    const header1 = useRef(null);

    useEffect(() => {
        header.current.style.width = header1.current.offsetWidth + 'px';
    }, []);


    //props
    const data = props.data;
    const ignore = props.ignore;
    const columns = props.format.columns;
    const colors = props.format.colors;

    return(
        <div className="custom-table is-inline-flex is-flex-direction-row hred is-flex-wrap-nowrap coolscroll" style={{overflow: 'visible', height:"100%", width: "100%"}} >
            <div ref={header} style={{width: '100%'}}>

                <div className="table-head1" style={{minWidth: '100%'}}>
                    {props.name ? <div className={`box has-background-hred p-1 is-size-4`}>{props.name}</div> : null}
                    <div className={`box is-inline-flex is-flex-wrap-nowrap ${colors.header} py-3 is-justify-content-space-between`} style={{minWidth: '100%'}} ref={header1}>
                        {
                            columns.map( (col, i, arr) =>
                                ignore.includes(col.key) ? null :
                                <span className={`p-3 table-item is-${col.size} ${colors.header_text} ${arr.length-1 == i ? 'no-border' : ''}`} key={i}>
                                    {col.name}
                                </span>
                            )
                        }
                    </div>
                </div>

                <div className="table-body1 coolscroll hred pt-3">
                        {
                            data.map( (item, i, arr) =>
                                <div className="mb-3">
                                    <div className="box is-inline-flex is-flex-wrap-nowrap  py-2 is-justify-content-space-between" style={{minWidth: '100%'}}>
                                        {
                                            columns.map( (col, j, arr2) =>
                                                ignore.includes(col.key) ? null :
                                                <span className={`p-3 table-item is-${col.size} ${arr.length-1 == j ? 'no-border' : ''}`} >
                                                    {item[col.key]}
                                                </span>
                                            )
                                        }
                                    </div>
                                    <div>{props.extras}</div>
                                </div>
                            )
                        }
                </div>
            </div>
        </div>
    );
}

export default Table;
