import {Link} from 'react-router-dom';

import './create-btn.scss';

const CreateBtn = props => {
    return(
        <div className="dropdown is-hoverable is-right" style={{width: "100%"}}>
            <div className="dropdown-trigger" style={{width: "100%"}}>
                <button className="button is-primary is-fullwidth is-size-5 has-text-left btn">
                    <span className="icon mr-1"><i className="fas fa-plus-circle"></i></span> &nbsp;
                    CREAR
                </button>
            </div>
            <div className="dropdown-menu py-0" role="menu" style={{width: '100%'}}>
                <div className="dropdown-content">
                    <Link to="/create/new" className={`dropdown-item ${props.active == 1 ? 'is-active' : ''}`}>
                        Novedad
                    </Link>

                    <Link to="/create/patient" className={`dropdown-item ${props.active == 2 ? 'is-active' : ''}`}>
                        Paciente nuevo
                    </Link>

                    <Link to="/create/eps-opl" className={`dropdown-item ${props.active == 3 ? 'is-active' : ''}`}>
                        EPS / OPL
                    </Link>
                </div>
            </div>
        </div>
    );
}

export default CreateBtn;
