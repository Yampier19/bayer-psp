import {useState, useEffect} from 'react';

import FilterBar from '../../../base/filter-bar';
import Table from '../../../base/table';

import axios from 'axios';


import {Link, useLocation, useParams} from 'react-router-dom';


// import './general.scss';
import './../table-list.scss';
import './consult.scss';

import flag from './flag.png';

const format = {
    columns: [
        {
            key: 'pap',
            name: 'PAP',
            size: 1
        },
        {
            key: 'idTrat',
            name: 'Id trat.',
            size: 1
        },
        {
            key: 'asunto',
            name: 'Asunto',
            size: 2
        },
        {
            key: 'producto',
            name: 'Producto',
            size: 1
        },
        {
            key: 'novedad',
            name: 'Novedad',
            size: 1
        },
        {
            key: 'observaciones',
            name: 'Observacines',
            size: 3
        },
        {
            key: 'reportes',
            name: 'Reportes',
            size: 1
        },
        {
            key: 'respuesta',
            name: 'Respuesta',
            size: 1
        },
        {
            key: 'responsable',
            name: 'Responsable',
            size: 2
        }
    ],
    colors: {
        header: "has-background-hgreen-light",
        header_text: "has-text-hgreen"
    }
};


const Consult = props => {

    const itemsPerPage = 10;
    const {search} = useLocation();
    const page = new URLSearchParams(search).get('page');
    const [currentPage, setCurrentPage] = useState(page);
    const [total, setTotal] = useState(0);
    const [lastPage, setLastPage] = useState(0);
    const [nextPage, setNextPage] = useState(0);
    const [previousPage, setPreviousPage] = useState(0);


    const url = 'https://jsonplaceholder.typicode.com/posts';
    const [news, setNews] = useState([]);
    const [ignore, setIgnore] = useState([]);



    const pushNewData = data => {

        let _data = [];
        data.map( (d, i) => {
            _data.push({
                pap: 'pap'+i,
                idTrat: d.id,
                asunto: d.author,
                producto: 'ventavis',
                novedad: 'varios',
                observaciones: d.title,
                reportes: '2019-11-10',
                respuesta: 'AAAA-MM-DD',
                responsable: {
                    nombre: 'nombre del responsable',
                    pais: 'C',
                    estado:'has-background-hblue'
                },

            });
        });
        console.log(_data);

        const keys = Object.keys( _data[0] );
        const _ignore = format.columns.filter( col => !keys.includes(col.key) ).map( col => col.key );

        setNews(_data);
        setIgnore(_ignore);
    }
    const fetchPage = () => {

        axios.get(`http://localhost:3000/posts?_page=${currentPage}`).then(res => {

            const {data, headers} = res;

            //get total & last
            const _total = headers['x-total-count'];
            const _lastPage = _total / itemsPerPage;

            //get next
            const _nextHipotetical = Number(currentPage) + 1;
            const _nextPage = _nextHipotetical <= _lastPage ? _nextHipotetical : null;

            //get previous
            const _previousHipotetical = Number(currentPage) - 1;
            const _previousPage = _previousHipotetical >= 1 ? _previousHipotetical : null;


            setTotal(_total);
            setLastPage(_lastPage);
            setNextPage(_nextPage);
            setPreviousPage(_previousPage);
            pushNewData(data);

        })
        .catch(err => {});
    }

    useEffect(
        () => {
            fetchPage();
        }, [currentPage]
    );


    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section px-0 py-0">
                <h1 className="title has-text-hgreen is-2">Novedades</h1>
                <h2 className="subtitle has-text-hgreen">Consultar</h2>
            </section>

            {/* first level - filters */}
            <section className="section px-0 py-6">

                <div className="columns">
                    <div className="column">
                        <FilterBar
                        textColorClass="has-text-hgreen"
                        filters={[
                            {
                                name: "PAP",
                                options: [{name: "op1"}, {name: "op2"}]
                            },
                            {
                                name: "Novedad",
                                options: []
                            },
                            {
                                name: "Prioridad",
                                options: []
                            },
                            {
                                name: "Estado",
                                options: []
                            },
                            {
                                name: "País",
                                options: []
                            },
                            {
                                name: "Responsable",
                                options: []
                            },
                        ]}
                        moreFiltersOn={false}
                        tags={null}
                    />
                    </div>
                    <div className="column is-2-desktop has-text-right has-text-left-mobile">
                        <div className="field">
                            <div className="control has-icons-right">
                                <input className="input searchbar" type="text" placeholder="Buscar" />
                                <span className="icon is-small is-right">
                                    <i className="fas fa-search"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

            </section>





            {/* second level - table */}
            <section className="is-flex-grow-1" style={{minHeight: '500px', overflow: 'hidden'}}>
                <div className="coolscroll hgreen " style={{overflow: 'auto', height:"100%", width: "100%"}} >
                    <div className="box table-header has-background-hgreen-light" style={{minWidth: '1450px'}}>
                        <div className="columns has-text-hgreen is-mobile fila" >

                            <div className="column is-1">PAP</div>
                            <div className="column is-1">ID Trat</div>
                            <div className="column is-1" style={{minWiddiv: '200px'}}>Asunto</div>
                            <div className="column is-1">Producto</div>
                            <div className="column is-1">Novedad</div>
                            <div className="column is-3" style={{minWiddiv: '200px'}}>Observaciones</div>
                            <div className="column">Reporte</div>
                            <div className="column">Respuesta</div>
                            <div className="column is-2 no-border">Responsable</div>

                        </div>
                    </div>
                    <br/>

                    {
                        news.map( (neww,i) =>

                            <div className="mb-6" style={{position: 'relative'}}>
                                <div className="box" key={i} style={{overflow: 'hidden', minWidth: '1450px',borderRadius: '7px !important'}}>
                                    <div className="columns fila2 is-mobile">
                                        <div className="column is-1 ">{neww.pap}</div>
                                        <div className="column is-1">{neww.idTrat}</div>
                                        <div className="column is-1" style={{minWiddiv: '200px'}}>{neww.asunto}</div>
                                        <div className="column is-1">{neww.producto}</div>
                                        <div className="column is-1">{neww.novedad}</div>
                                        <div className="column is-3" style={{minWiddiv: '200px'}}>{neww.observaciones}</div>
                                        <div className="column">{neww.reportes}</div>
                                        <div className="column"><span className="tag is-hred">AAAA-MM-DD</span></div>
                                        <div className="column is-2 no-border py-0 pr-0" >
                                            <div className="columns is-mobile" >
                                                <div className="column">
                                                    {neww.responsable.nombre}
                                                </div>
                                                <div className={`column is-1 p-0 py-2 has-text-centered ${neww.responsable.estado}`} style={{minWidth: '30px'}}>
                                                    <figure class="image is-16x16" style={{left: '5px'}}>
                                                        <img src={flag} alt=''/>
                                                    </figure>
                                                    <br/>
                                                    <span className="has-text-white">C</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="has-text-right is-hidden-mobile" style={{width: '100%'}}>
                                    <Link to={`/news/update?row=${neww.idTrat}`} className="button has-background-transparent has-no-border">
                                        <span className="icon">
                                            <i class="fas fa-pen"></i>
                                        </span>
                                        <span><u>
                                            Editar
                                        </u></span>
                                    </Link>
                                    &nbsp;
                                    <Link to={`/news/comments?row=${neww.idTrat}`} className="button has-background-transparent has-no-border">
                                        <span className="icon">
                                            <i class="fas fa-comments"></i>
                                        </span>
                                        <span>
                                            <u>Comentarios</u>
                                        </span>
                                    </Link>
                                </div>
                                <div className=" is-hidden-desktop has-background-hblue" style={{position: 'absolute', top: '0'}}>
                                    <button className="button">
                                        <span className="icon">
                                            <i class="fas fa-pen"></i>
                                        </span>
                                    </button>
                                    <br/>
                                    <button className="button">
                                        <span className="icon">
                                            <i class="fas fa-comments"></i>
                                        </span>
                                    </button>
                                </div>
                            </div>
                        )
                    }
                </div>
            </section>

            <section className="section px-0 pb-0">


                <h1 className="subtitle has-text-hgreen">
                    se encontraron {news.length} registros
                </h1>

                <nav class="pagination is-small is-centered" role="navigation" aria-label="pagination">
                    <Link to={`/news/consult?page=${previousPage}`} class="pagination-previous" disabled={previousPage == null} onClick={e => setCurrentPage(previousPage)}>{'<'}</Link>
                    <Link to={`/news/consult?page=${nextPage}`} class="pagination-next" disabled={nextPage == null} onClick={e => setCurrentPage(nextPage)}>{'>'}</Link>
                    <ul class="pagination-list">
                        <li><Link to={`/news/consult?page=${previousPage}`} class={`pagination-link ${previousPage ? '' : 'is-hidden'}`} onClick={e => setCurrentPage(previousPage)}>{previousPage}</Link></li>
                        <li><a class="pagination-link is-current">{currentPage}</a></li>
                        <li><Link to={`/news/consult?page=${nextPage}`} class={`pagination-link ${nextPage ? '' : 'is-hidden'}`} onClick={e => setCurrentPage(nextPage)}>{nextPage}</Link></li>
                    </ul>
                </nav>


            </section>

        </div>
    );
}

export default Consult;
