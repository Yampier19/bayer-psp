import {useState, useEffect} from 'react';

import TextEditor from '../../../base/text-editor';

import {Link, useLocation} from 'react-router-dom'

import axios from 'axios';

import './reply.scss';

const Comments = props => {


    const {search} = useLocation();
    const row = new URLSearchParams(search).get('row');

    const url = `http://localhost:3000/posts/${row}?_embed=comments`;
    const [comments, setComments] = useState([]);
    const [done, setDone] = useState(false);

    useEffect(
        () => {

            axios.get(url)
            .then(res => {

                const comments = res.data.comments;

                comments.map( (c, i) => {

                    let r = Math.round(Math.random() * 3)

                    const replies = [];

                    for(let i = 0; i < r; i++)
                        replies.push({
                            from: 'Nombre doctor',
                            body: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. '
                        });

                    c.replies = replies;

                });

                setComments(comments);
                setTimeout( () => setDone(true) , 100);
            })
            .then(err => {});

        }, []
    );

    return(

        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>


            {/* header */}
            <section className="section px-0 py-0 pb-6">
                <h1 className="title has-text-hgreen is-2">Novedades</h1>
                <h2 className="subtitle has-text-hgreen">Historial</h2>
            </section>

            {/* tabs */}
            <section className="section px-0 py-0 pb-6">
                <div className="columns is-mobile is-gapless is-hidden-desktop">
                    <div className="column"><Link to="/news/history" className="button is-small is-fullwidth is-rounded has-text-dark">Historial</Link></div>
                    <div className="column"><Link to={`/news/update?row=${row}`} className="button is-small is-fullwidth is-rounded has-text-dark">Editar</Link></div>
                    <div className="column"><button className="button is-small is-fullwidth is-hgreen is-rounded has-text-white"><strong>Comentarios</strong></button></div>
                </div>
                <div className="columns is-mobile is-hidden-touch">
                    <div className="column is-2 is-offset-6"><Link to="/news/history" className="button is-fullwidth is-rounded has-text-dark">Historial</Link></div>
                    <div className="column is-2"><Link to={`/news/update?row=${row}`} className="button is-fullwidth is-rounded has-text-dark">Editar</Link></div>
                    <div className="column is-2"><button to="/temp" className="button is-fullwidth is-hgreen is-rounded has-text-white"><strong>Comentarios</strong></button></div>
                </div>
            </section>



            {/* comments */}
            <section className="section px-0 py-0 pb-3 is-flex-grow-1 coolscroll hgreen pr-1" style={{overflowY: 'scroll', overflowX: 'hidden'}}>
                <div className="container is-max-widescreen">

                    <div className="columns is-mobile">

                        <div className="column is-1" style={{minWidth: '60px'}}>
                            <figure class="image is-48x48">
                                <img className="is-rounded" src="https://bulma.io/images/placeholders/128x128.png"/>
                            </figure>
                            <h2 className="subtitle is-6">27 may</h2>
                        </div>
                        <div className="column" style={{overflow: 'hidden'}}>
                            {
                                done ? <TextEditor id={0} placeholder="Responder comentario..."/> : null
                            }
                        </div>
                    </div>
                    <hr/>
                    {
                        comments.map( (comment, i) =>
                            <div className="mb-6 py-3" key={i}>
                                <div className="columns is-mobile">

                                    <div className="column is-1" style={{minWidth: '60px'}}>
                                        <section className="">
                                            <figure class="image is-48x48">
                                                <img className="is-rounded" src="https://bulma.io/images/placeholders/128x128.png"/>
                                            </figure>
                                            <h2 className="subtitle is-6">27 may</h2>

                                        </section>
                                    </div>
                                    <div className="column" style={{overflow: 'hidden'}}>
                                        <section>
                                            <h1 className="title has-text-hgreen is-3">Catalina Loss</h1>
                                            <p>
                                                {comment.body}
                                            </p>
                                            <br/>
                                            {
                                                done ? <TextEditor id={(i+1)} placeholder="Responder comentario..."/> : null
                                            }

                                        </section>

                                        <section className="mt-6">



                                            {
                                                comment.replies.map( (rp, j) =>
                                                    <div className="box is-hgreen-light reply" key={j}>

                                                        <div className="columns is-mobile">
                                                            <div className="column is-1" style={{minWidth: '60px'}}>
                                                                <figure class="image is-48x48 is-pulled-left" style={{zIndex: 100}}>
                                                                    <img className="is-rounded" src="https://bulma.io/images/placeholders/128x128.png"/>
                                                                </figure>
                                                            </div>

                                                            <div className="column">
                                                                <h1 className="title has-text-hgreen is-3">{rp.from}</h1>

                                                            </div>
                                                        </div>
                                                        <p className="has-text-black">{rp.body}</p>
                                                    </div>
                                                )
                                            }
                                        </section>
                                    </div>
                                </div>
                                <hr/>
                            </div>
                        )
                    }
                </div>
            </section>

        </div>
    );
}

export default Comments;
