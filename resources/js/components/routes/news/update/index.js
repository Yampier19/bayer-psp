import {useState} from 'react';
import FormField from '../../../form/formfield';

import {Link, useLocation} from 'react-router-dom';
import Form from './form';

const Update = props => {

    const {search} = useLocation();
    const row = new URLSearchParams(search).get('row');

    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            { /* header */ }
            <section className="section p-0 pb-6">
                <h1 className="title has-text-hgreen is-2">Novedades</h1>
                <h2 className="subtitle has-text-hgreen">Consultar - Actualización</h2>
            </section>

            {/* tabs */}
            <section className="section px-0 py-0 pb-6">
                <div className="columns is-mobile is-gapless is-hidden-desktop">
                    <div className="column"><Link to="/news/history" className="button is-small is-fullwidth  is-rounded has-text-dark">Historial</Link></div>
                    <div className="column"><button className="button is-small is-fullwidth is-rounded is-hgreen has-text-white"><strong>Editar</strong></button></div>
                    <div className="column"><Link to={`/news/comments?page=${row}`} className="button is-small is-fullwidth is-rounded has-text-dark">Comentarios</Link></div>
                </div>
                <div className="columns is-mobile is-hidden-touch">
                    <div className="column is-2 is-offset-6"><Link to="/news/history" className="button is-fullwidth is-rounded has-text-dark">Historial</Link></div>
                    <div className="column is-2"><button className="button is-fullwidth is-rounded is-hgreen has-text-white"><strong>Editar</strong></button></div>
                    <div className="column is-2"><Link to={`/news/comments?row=${row}`} className="button is-fullwidth is-rounded has-text-dark">Comentarios</Link></div>
                </div>
            </section>

            <section className="section px-0 py-0 pb-3 is-flex-grow-1 coolscroll" style={{overflowY: 'hidden', overflowX: 'hidden'}}>
                <div className="coolscroll hgreen" style={{height: '100%', overflow: 'auto'}}>
                    <Form row={row}/>
                </div>
            </section>

        </div>
    );
}

export default Update;
