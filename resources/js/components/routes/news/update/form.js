import {useState, useEffect} from 'react';

import FormField from '../../../form/formfield';

import {useFormik} from 'formik';
import * as Yup from 'yup';

import axios from 'axios';
import {useLocation} from 'react-router-dom';

import {InfoModal} from '../../../base/modal-system/models/modal';
import {set_modal_on} from '../../../../redux/actions/modalActions';
import {connect} from 'react-redux';

export const formSchema = Yup.object().shape({
    pap: Yup.string().required('Campo obligatorio'),
    asunto: Yup.string().required('Campo obligatorio'),
    producto: Yup.string().required('Campo obligatorio'),
    novedad: Yup.string().required('Campo obligatorio'),
    fechaReporte: Yup.string().required('Campo obligatorio'),
    fechaRespuesta: Yup.string().required('Campo obligatorio'),
    observaciones: Yup.string(),
    observacionesRespuesta: Yup.string(),
    estado: Yup.string().required('Campo obligatorio'),
    solicitudAdicional: Yup.string().required('Campo obligatorio')
});



const Form = props => {

    const {row} = props;

    const [initialValues, setInitialValues] = useState({
        pap: '',
        asunto: '',
        producto: '',
        novedad: '',
        fechaReporte: '',
        fechaRespuesta: '',
        observaciones: '',
        observacionesRespuesta: '',
        estado: '',
        solicitudAdicional: ''
    });

    const formik = useFormik({
        enableReinitialize: true,
        initialValues: initialValues,
        validateOnChange: true,
        validationSchema: formSchema,
        onSubmit: async values => {
            alert(JSON.stringify(values, null, 2));
            const {_icons} = props.modalReducer;
            props.set_modal_on( new InfoModal('La novedad ha sido actualizada éxitosamente', _icons.CHECK_PRIMARY) );
        }
    });

    useEffect(
        () => {
            axios.get(`http://localhost:3000/posts/${row}`)
            .then(res => {


                setInitialValues({
                    pap: 'pap'+res.data.id,
                    asunto: res.data.id,
                    producto: 'producto',
                    novedad: 'novedad',
                    fechaReporte: 'DD-MM-AAAA',
                    fechaRespuesta: 'DD-MM-AAAA',
                    observaciones: 'algunas obervaciones',
                    observacionesRespuesta: 'respuesta a las observaciones',
                    estado: 'estado',
                    solicitudAdicional: '...'
                });
            })
            .catch(e => {});
        },
        []
    );


    return(
        <form id="form" onSubmit={formik.handleSubmit} className="coolscroll primary pr-3 py-3">

            <FormField label="PAP" number="1" name="pap" checked={formik.values.pap}>
                <input className="input cool-input" type="text" name="pap" onChange={formik.handleChange} value={formik.values.pap} />
                {formik.touched.userCode && formik.errors.userCode ? <div className="help is-danger"> {formik.errors.userCode}</div> : null }
            </FormField>

            <FormField label="Asunto" number="2" name="asunto" checked={formik.values.asunto}>
                <input className="input cool-input" type="text" name="asunto" onChange={formik.handleChange} value={formik.values.asunto} />
                {formik.touched.asunto && formik.errors.asunto ? <div className="help is-danger"> {formik.errors.asunto}</div> : null }
            </FormField>

            <FormField label="Producto" number="3" name="producto" checked={formik.values.producto}>
                <input className="input cool-input" type="text" name="producto" onChange={formik.handleChange} value={formik.values.producto} />
                {formik.touched.producto && formik.errors.producto ? <div className="help is-danger"> {formik.errors.producto}</div> : null }
            </FormField>

            <FormField label="Novedad" number="4" name="novedad" checked={formik.values.novedad}>
                <input className="input cool-input" type="text" name="novedad" onChange={formik.handleChange} value={formik.values.novedad} />
                {formik.touched.novedad && formik.errors.novedad ? <div className="help is-danger"> {formik.errors.novedad}</div> : null }
            </FormField>

            <FormField label="Fecha de reporte" number="5" name="fechaReporte" checked={formik.values.fechaReporte}>
                <input className="input cool-input" type="text" name="fechaReporte" onChange={formik.handleChange} value={formik.values.fechaReporte} />
                {formik.touched.fechaReporte && formik.errors.fechaReporte ? <div className="help is-danger"> {formik.errors.fechaReporte}</div> : null }
            </FormField>

            <FormField label="Fecha de respuesta" number="6" name="fechaRespuesta" checked={formik.values.fechaRespuesta}>
                <input className="input cool-input" type="text" name="fechaRespuesta" onChange={formik.handleChange} value={formik.values.fechaRespuesta} />
                {formik.touched.fechaRespuesta && formik.errors.fechaRespuesta ? <div className="help is-danger"> {formik.errors.fechaRespuesta}</div> : null }
            </FormField>

            <FormField label="Observaciones" number="7" name="observaciones" checked={formik.values.observaciones}>
                <input className="input cool-input" type="text" name="observaciones" onChange={formik.handleChange} value={formik.values.observaciones} />
                {formik.touched.observaciones && formik.errors.observaciones ? <div className="help is-danger"> {formik.errors.observaciones}</div> : null }
            </FormField>

            <FormField label="Observaciones Respuesta" number="8" name="observacionesRespuesta" checked={formik.values.observacionesRespuesta}>
                <input className="input cool-input" type="text" name="observacionesRespuesta" onChange={formik.handleChange} value={formik.values.observacionesRespuesta} />
                {formik.touched.observacionesRespuesta && formik.errors.observacionesRespuesta ? <div className="help is-danger"> {formik.errors.observacionesRespuesta}</div> : null }
            </FormField>

            <FormField label="Estado" number="9" name="estado" checked={formik.values.estado}>
                <input className="input cool-input" type="text" name="estado" onChange={formik.handleChange} value={formik.values.estado} />
                {formik.touched.estado && formik.errors.estado ? <div className="help is-danger"> {formik.errors.estado}</div> : null }
            </FormField>

            <FormField label="Solicitud adicional" number="10" name="solicitudAdicional" checked={formik.values.solicitudAdicional} noline>
                <input className="input cool-input" type="text" name="solicitudAdicional" onChange={formik.handleChange} value={formik.values.solicitudAdicional} />
                {formik.touched.solicitudAdicional && formik.errors.solicitudAdicional ? <div className="help is-danger"> {formik.errors.solicitudAdicional}</div> : null }
            </FormField>


            <br/>

            <div className="field">
                <div className="control has-text-right has-text-centered-mobile">
                    <button
                        className="button is-hgreen"
                        type="submit"
                        style={{width: '150px'}}
                    >
                        ACTUALIZAR
                    </button>
                </div>
            </div>

        </form>
    );
}

const mapStateToPros = state => ({
    modalReducer: state.modalReducer
})

export default connect(
    mapStateToPros,
    {
        set_modal_on
    }
)(Form);
