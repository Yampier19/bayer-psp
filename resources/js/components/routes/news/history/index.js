import {useState, useEffect} from 'react';

import {Link} from 'react-router-dom'

import axios from 'axios';

const History = props => {

    const url = 'https://jsonplaceholder.typicode.com/posts';
    const [news, setNews] = useState([]);

    useEffect(
        () => {

            axios.get(url)
            .then(res => {
                setNews(res.data);
            })
            .then(err => console.log(err));

        }, []
    );


    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section px-0 py-0 pb-6">
                <h1 className="title has-text-hgreen is-2">Novedades</h1>
                <h2 className="subtitle has-text-hgreen">Historial</h2>
            </section>

            {/* tabs */}
            <section className="section px-0 py-0 pb-6">
                <div className="columns is-mobile is-gapless is-hidden-desktop">
                    <div className="column"><button className="button is-small is-fullwidth is-hgreen is-rounded has-text-white"><strong>Historial</strong></button></div>
                    <div className="column"><Link to="/news/update" className="button is-small is-fullwidth is-rounded has-text-dark">Editar</Link></div>
                    <div className="column"><Link to="/news/comments" className="button is-small is-fullwidth is-rounded has-text-dark">Comentarios</Link></div>
                </div>
                <div className="columns is-mobile is-hidden-touch">
                    <div className="column is-2 is-offset-6"><button className="button is-fullwidth is-hgreen is-rounded has-text-white"><strong>Historial</strong></button></div>
                    <div className="column is-2"><Link to="/news/update" className="button is-fullwidth is-rounded has-text-dark">Editar</Link></div>
                    <div className="column is-2"><Link to="/news/comments" className="button is-fullwidth is-rounded has-text-dark">Comentarios</Link></div>
                </div>
            </section>

            {/* history */}
            <section className="section px-0 py-0 pb-3 is-flex-grow-1 coolscroll" style={{overflowY: 'scroll', overflowX: 'hidden'}}>
                <h1 className="subtitle has-text-hgreen is-6">Actividad</h1>
                <hr/>

                {
                    news.map( (neww, i) =>
                        <div>
                            <div className="columns is-mobile">

                                <div className="column is-1" style={{minWidth: '80px'}}>
                                    <figure class="image is-64x64">
                                        <img className="is-rounded" src="https://bulma.io/images/placeholders/128x128.png"/>
                                    </figure>
                                </div>

                                <div className="column">
                                    <div className="columns">
                                        <div className="column">
                                            {neww.body}
                                        </div>
                                        <div className="column is-4 has-text-right has-text-left-mobile pr-6">
                                            Asesor -> admin
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <hr/>
                        </div>
                    )
                }
            </section>




        </div>
    );
}

export default History;
