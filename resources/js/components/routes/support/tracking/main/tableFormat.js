export const format = {
    columns: [
        {
             key: 'pap',
             name: 'PAP',
             size: 1
        },
        {
             key: 'codigo_usuario',
             name: 'Cod. Usuario',
             size: 1
        },
        {
             key: 'id_tratamiento',
             name: 'Id trat.',
             size: 1
        },
        {
             key: 'producto',
             name: 'Producto',
             size: 2
        },
        {
             key: 'gestion',
             name: 'Gestión',
             size: 1
        },
        {
             key: 'proximo_contacto',
             name: 'Prox. contacto',
             size: 1
        },
        {
             key: 'responsable',
             name: 'Responsable',
             size: 2
        }
    ],
    colors: {
        header: "has-background-hgreen-light",
        header_text: "has-text-hgreen"
    }
}
