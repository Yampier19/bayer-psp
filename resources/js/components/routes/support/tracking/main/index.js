import {useState, useEffect} from 'react';

import FilterBar from '../../../../base/filter-bar';

import Table, {getIgnored} from '../../../../base/table';

import {Link} from 'react-router-dom';

import axios from 'axios';
import {format} from './tableFormat';


const Tracking = props => {

    const url = 'https://jsonplaceholder.typicode.com/posts';
    const [news, setNews] = useState([]);

    useEffect(
        () => {

            axios.get(url)
            .then(res => {

                let data = [];
                res.data.splice(0, 50).map( (d,i) => {
                    data.push({
                        pap: 'pap'+i,
                        codigo_usuario: Math.floor(Math.random()*500),
                        id_tratamiento: Math.floor(Math.random()*500),
                        producto: d.body.substring(0, 40),
                        gestion: 'DD-MM-AAAA',
                        proximo_contacto: 'DD-MM-AAAA',
                        responsable: 'Nombres y appelidos del responsable'
                    });
                });

                setNews(data);

            })
            .then(err => console.log(err));

        }, []
    );

    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section px-0 py-0">
                <h1 className="title has-text-hgreen is-2">Seguimiento</h1>
            </section>

            {/* first level - filters */}
            <section className="section px-0 py-6">

                <div className="columns">
                    <div className="column">
                        <FilterBar
                        textColorClass="has-text-hgreen"
                        filters={[
                            {
                                name: "PAP",
                                options: [{name: "op1"}, {name: "op2"}]
                            },
                            {
                                name: "Código de usuario",
                                options: []
                            },
                            {
                                name: "Id tratamiento",
                                options: []
                            },
                            {
                                name: "Estado",
                                options: []
                            },
                        ]}
                        moreFiltersOn
                        tags={null}
                    />
                    </div>
                    <div className="column is-2-desktop has-text-right has-text-left-mobile">
                        <div className="field">
                            <div className="control has-icons-right">
                                <input className="input searchbar" type="text" placeholder="Buscar" />
                                <span className="icon is-small is-right">
                                    <i className="fas fa-search"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>





            </section>





            {/* second level - table */}
            <section className="is-flex-grow-1" style={{minHeight: '500px', overflowY: 'hidden'}}>
                <div className="coolscroll hgreen " style={{overflow: 'auto', height:"100%", width: "100%"}} >
                    <Table format={format} data={news} ignore={[]}/>
                </div>
            </section>

            <section className="section px-0 pb-0">

                <h1 className="subtitle has-text-hgreen">
                    se encontraron {news.length} registros
                </h1>

            </section>

        </div>
    );
}

export default Tracking;
