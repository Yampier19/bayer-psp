import MenuCard from '../../../cards/home/menucard';
import CreateBtn from '../../../base/buttons/create-btn';

import {Link} from 'react-router-dom';

import './home.scss';

const Home = props => {
    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>


            {/* header */}
            <section className="section px-0 pt-0">
                <div className="columns">
                    <div className="column is-3-desktop ">
                        <h1 className="title has-text-hpurple is-2">Apoyo Diagnóstico</h1>
                    </div>
                    <div className="column is-3-desktop is-offset-6-desktop">
                        <div className="field">
                            <div className="control has-icons-right">
                                <input className="input searchbar support" type="text" placeholder="Buscar" />
                                <span className="icon is-small is-right has-text-hyellow">
                                    <i className="fas fa-search"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {/* first level */}
            <div className="columns is-mobile">
                <div className="column is-3-desktop is-offset-9-desktop is-6-mobile is-offset-6-mobile">
                    <Link to="/support/create/patient" className="button is-hred is-fullwidth is-size-5 has-text-left btn2">
                        <span className="icon mr-1"><i className="fas fa-plus-circle"></i></span> &nbsp;
                        Crear
                    </Link>
                </div>
            </div>

            <br/>

            {/* second level */}
            <div className="columns">

                <div className="column mb-6">
                    <div className="notification py-2 is-hgreen"><h1 className="subtitle is-6">SEGUIMIENTO</h1></div>
                    <MenuCard
                        title="Consultar Seguimiento"
                        color="hgreen"
                        to="/support/tracking"
                    />
                </div>

                <div className="column mb-6">
                    <div className="notification py-2 is-hblue"><h1 className="subtitle is-6">REPORTES</h1></div>
                    <MenuCard
                        title="Todos los reportes"
                        color="hblue"
                        to="/support/reports"
                    />
                    <MenuCard
                        title="Gráficas"
                        color="hblue"
                        to="/support/reports/graphics"
                    />
                </div>

            </div>

        </div>
    );
}

export default Home;
