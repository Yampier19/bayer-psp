import {useEffect, useState} from 'react';

import LoginCardEnter from '../../cards/loginCardEnter';
import LoginCardPassword from '../../cards/loginCardPassword';
import LoginCardCode from '../../cards/loginCardCode';
import LoginCardRecover from '../../cards/loginCardRecover';

import './login.scss';

import banner from '../../../temp/banner.png';

const Login = props => {

    const [state, setState] = useState(1);


    return(
        <div className="App">

            <div className="hero is-fullheight login-bg" style={{backgroundImage: ""}}>
                <div className="hero-header py-6 px-3 mt-6">
                    <div className="container">

                        <div className="columns">
                            <div className="column is-5">
                                {
                                    state == 0 ?
                                    <LoginCardEnter/> :
                                    state == 1 ?
                                    <LoginCardPassword/> :
                                    state == 2 ?
                                    <LoginCardCode/> :
                                    state == 3 ?
                                    <LoginCardRecover/> : null
                                }

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    );
}

export default Login;
