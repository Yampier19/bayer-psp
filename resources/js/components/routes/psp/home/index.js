import Sidebar from '../../../commons/sidebar';

import MenuCard from '../../../cards/home/menucard';
import CreateBtn from '../../../base/buttons/create-btn';

import {Link} from 'react-router-dom';

const Home = props => {
    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <h1 className="title has-text-primary is-2">Aplicación de PSP</h1>
            <h2 className="subtitle has-text-primary">Programa de seguimiento de pacientes</h2>

            {/* first level */}
            <div className="columns is-mobile">
                <div className="column is-3-desktop is-offset-9-desktop is-6-mobile is-offset-6-mobile">
                    <CreateBtn />
                </div>
            </div>

            <br/>

            {/* second level */}
            <div className="columns">
                <div className="column mb-6">
                    <div className="notification py-2 is-hgreen"><h1 className="subtitle is-6">NOVEDADES</h1></div>
                    <MenuCard
                        title="Consultar Novedad"
                        color="hgreen"
                        to="/news/consult"
                    />
                </div>

                <div className="column mb-6">
                    <div className="notification py-2 is-hblue"><h1 className="subtitle is-6">SEGUIMIENTO</h1></div>
                    <MenuCard
                        title="Consultar Seguimiento"
                        color="hblue"
                        to="/tracking"
                    />
                </div>

                <div className="column mb-6">
                    <div className="notification py-2 is-horange"><h1 className="subtitle is-6">PRODUCTOS</h1></div>

                    <MenuCard
                        title="Registrar Producto"
                        color="horange"
                        to="/temp"
                    />
                    <MenuCard
                        title="Inventario"
                        color="horange"
                        to="/temp"
                    />
                </div>

                <div className="column mb-6">
                    <div className="notification py-2 is-hred"><h1 className="subtitle is-6">REPORTES</h1></div>
                    <MenuCard
                        title="Todos los reportes"
                        color="hred"
                        to="/temp"
                    />
                    <MenuCard
                        title="Gráficas"
                        color="hred"
                        to="/temp"
                    />
                </div>

            </div>

        </div>
    );
}

export default Home;
