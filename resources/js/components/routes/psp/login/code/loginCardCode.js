const LoginCardCode = props => {
    return(
        <div id="loginCard" className="box has-background-primary">

            {/* title */}
            <header className="hero">
                <div className="hero-body my-5">
                    <div className="container">
                        <h1 className="title has-text-white has-text-centered">Olvidaste la contraseña</h1>
                    </div>
                </div>
            </header>

            <div className="px-4">

                <p className="has-text-centered has-text-light px-6 mb-6 is-size-5">
                    Por favor digite el código de confirmación enviado a su correo
                </p>


                <form>

                    {/* password */}
                    <div className="field">
                        <div className="control">
                            <input className="input is-rounded linput pl-5 py-5" type="text" placeholder="Código de confirmación"/>
                        </div>
                    </div>

                    <br/>

                    {/* submit */}
                    <div className="field">
                        <div className="control has-text-centered">

                            <a className="button is-light has-text-primary py-5 loginb" style={{width: '50%'}}>
                                <span className="icon"><i className="fas fa-chevron-right"></i></span>
                                <strong className="is-pulled-right">Continuar</strong>
                            </a>
                        </div>
                    </div>
                </form>

                <br/>
                <br/>
                <br/>


                <div className="login-footer has-text-centered has-text-light is-size-6">
                    By People Marketing
                </div>

            </div>


        </div>
    );
}

export default LoginCardCode;
