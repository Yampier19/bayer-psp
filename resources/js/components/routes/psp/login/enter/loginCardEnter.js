import '../loginCard.scss';

const LoginCardEnter = props => {
    return(
        <div id="loginCard" className="box has-background-primary">

            {/* title */}
            <header className="hero">
                <div className="hero-body my-5">
                    <div className="container">
                        <h1 className="title has-text-white has-text-centered">INGRESAR</h1>
                    </div>
                </div>
            </header>

            <div className="px-4">
                <form>

                    {/* user */}
                    <div className="field">
                        <div className="control">
                            <input className="input is-rounded linput pl-5 py-5" type="text" placeholder="Usuario" name="user"/>
                        </div>
                    </div>

                    <br/>

                    {/* password */}
                    <div className="field">
                        <div className="control">
                            <input className="input is-rounded linput pl-5 py-5" type="password" placeholder="Contraseña" name="password"/>
                        </div>
                    </div>

                    <br/>

                    {/* submit */}
                    <div className="field">
                        <div className="control has-text-centered">

                            <a className="button is-light has-text-primary py-5 loginb" style={{width: '50%'}}>
                                <span className="icon"><i className="fas fa-sign-in-alt"></i></span>
                                <strong className="is-pulled-right">LOGIN</strong>
                            </a>
                        </div>
                    </div>
                </form>

                <br/>
                {/* recover */}
                <div className="field has-text-centered">
                    <a className="has-text-white is-underlined is-size-6 ">Olvidaste tu contraseña</a>
                </div>
                <br/>
                <br/>


                <div className="login-footer has-text-centered has-text-light is-size-6">
                    By People Marketing
                </div>

            </div>


        </div>
    );
}

export default LoginCardEnter;
