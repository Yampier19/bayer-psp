import {useState} from 'react';

import FormField from '../../../form/formfield';
import FilterBar from '../../../base/filter-bar';
import CreateUserBtn from '../../../base/buttons/create-user';

import {Link} from 'react-router-dom';

import Table from '../../../base/table';

import './users.scss';

const areAllFieldFilled = (form, fields) => {

    for(let i = 0; i < fields; i++)
        if(!form[`check_field${i+1}`].checked)
            return false;

    return true;
}


const ConfigurationUsers = props => {

    const [submitDisabled, setSubmitDisabled] = useState(true);

    const handleSubmit = e => {
        e.preventDefault();
    }

    const onChange = e => {



        const form = document.getElementById('form');

        //get the name of current field
        const name = e.target.name;

        //look for the linked check
        const check = form[`check_${name}`];

        check.checked = e.target.value != '' ? true : false;

        const allFilled = areAllFieldFilled(form, 8);

        setSubmitDisabled( allFilled ? false : true );
    }



    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section px-0 pt-0 pb-3 ">
                <div className="columns ">
                    <div className="column is-3-desktop ">
                        <h1 className="subtitle has-text-hred is-3 mb-1"  style={{ whiteSpace: 'nowrap'}}>
                            <strong className="has-text-hblack">Configuración</strong>
                        </h1>
                        <h1 className="subtitle has-text-hblack-light">Usuarios</h1>
                    </div>
                    <div className="column is-3-desktop is-offset-6-desktop">
                        <CreateUserBtn/>
                    </div>
                </div>
            </section>

            {/* filter bar */}
            <section className="section p-0 pb-1 ">
                <div className="columns">
                    <div className="column">
                        <FilterBar
                        textColorClass="has-text-hblack"
                        filters={[
                            {
                                name: "Nombre de usuario",
                                options: [{name: "op1"}, {name: "op2"}]
                            },
                            {
                                name: "Perfil",
                                options: []
                            },
                            {
                                name: "Acción",
                                options: []
                            },
                        ]}
                        moreFiltersOn={false}
                        tags={null}
                    />
                    </div>
                    {/*}<div className="column is-2-desktop has-text-right has-text-left-mobile">

                    </div> */}
                </div>

            </section>


            {/* form */}
            <section className="section px-0 is-flex-grow-1 pt-0 coolscroll" style={{minHeight: '500px', overflowY: 'scroll'}}>
                <div className="" style={{minWidth: '1450px', overflow: 'scroll'}}>
                    <div className="table-header pb-3 users" style={{boderBottom: '1px solid #cccccc'}}>
                        <div className="columns has-text-hblack is-mobile fila" >

                            <div className="column is-3 no-border"><strong>Usuario</strong></div>
                            <div className="column no-border"><strong>Estado</strong></div>
                            <div className="column no-border"><strong>Razón inactivo</strong></div>
                            <div className="column no-border"><strong>Rol</strong></div>
                            <div className="column is-2 no-border"></div>


                        </div>
                    </div>
                    <br/>

                    {
                        [0, 1, 2, 3].map( (user,i) =>

                            <div className="mb-6">
                                <div className="" key={i}>
                                    <div className="columns fila2 is-mobile">
                                        <div className="column is-3 no-border ">
                                            <div className="columns is-mobile is-vcentered">
                                                <div className="column no-border is-3">
                                                    <figure class="image is-64x64">
                                                        <img class="is-rounded" src="https://bulma.io/images/placeholders/128x128.png"/>
                                                    </figure>
                                                </div>
                                                <div className="column no-border">
                                                    <section>
                                                        <h1 className="title is-6">Nombres y Apellidos del asesor</h1>
                                                        <h2 className="subtitle is-6">Perfil del asesor</h2>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="column no-border">Activo / inactivo</div>
                                        <div className="column no-border">lorem ipsum dolor sit amet lorem ipsum dolor sit amet </div>
                                        <div className="column no-border">Rol del usuario</div>
                                        <div className="column is-2">

                                            <div className="level is-mobile">
                                                <div className="level-left"/>
                                                <div className="level-right">
                                                    <div className="level-item mr-4">
                                                        <a><span className="icon has-text-dark is-size-3"><i class="far fa-edit"></i></span></a>
                                                    </div>

                                                    <div className="level-item mr-3">
                                                        <a><span className="icon has-text-hred is-size-3"><i class="fas fa-trash-alt"></i></span></a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        )
                    }
                </div>
            </section>

            <section className="section px-0 pb-0 has-text-right has-text-centered-mobile">

                <a className="subtitle has-text-hblack">

                    <span className="icon has-text-hblack is-size-4"><i class="far fa-eye"></i></span> &nbsp;
                    <span><u><strong>Ver plantillas</strong></u></span>
                </a>

            </section>

        </div>
    );
}

export default ConfigurationUsers;
