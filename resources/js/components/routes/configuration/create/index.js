import {useState} from 'react';

import FormField from '../../../form/formfield';

import ProgressIcon from '../../../base/progress-icon';
import ProgressBar from '../../../base/progress-bar';

import Sections from '../../../base/sections';
import RolAssigmentCrs from '../../../base/rol-assigment/crs';
import Support from '../../../base/rol-assigment/support';

import {Link} from 'react-router-dom';

import {tabClicked} from '../../../commons/tabs/tabClicked';
import {areAllFieldFilled} from '../../../../utils/forms';

import './rol-assignment.scss';

const countProgress = (form, fields) => {

    let count = 0;

    for(let i = 0; i < fields; i++)
        if(form[`check_field${i+1}`].checked)
            count++;

    return count / fields * 100;
}



const ConfigurationCreate = props => {

    const [submitDisabled, setSubmitDisabled] = useState(true);
    const [progress, setProgress] = useState(0);

    const handleSubmit = e => {
        e.preventDefault();

        // const form = e.target;
        //
        // const formData = {};
        //
        // for(let i = 0; i < form.length; i++){
        //
        //     if(form[i].id != '')
        //         Object.defineProperty(formData, form[i].id, {value: form[i].value});
        // }
        //
        // console.log(formData);
    }

    const onChange = e => {



        const form = e.target.form;

        //get the name of current field
        const name = e.target.name;

        //look for the linked check
        const check = form[`check_${name}`];

        check.checked = e.target.value != '' ? true : false;

        // //submit btn
        // const allFilled = areAllFieldFilled(form, 3);
        //
        // setSubmitDisabled( allFilled ? false : true );
        //
        // //progress
        // const _progress = countProgress(form, 3);
        // setProgress( _progress );
    }


    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>



                {/* header */}
                <section className="section p-0">
                    <h1 className="title is-3 mb-5"><strong className="has-text-hblack">CONFIGURACIÓN</strong></h1>
                    <h2 className="subtitle has-text-hblack-light">Creación y asignación de roles</h2>

                </section>

                <hr/>

                {/* tabs */}
                <section className="section p-0 m-0">
                    <div className="columns is-vcentered">

                        <div className="column">
                            <div className="tabs is-boxed rolAssignmentTabs ">
                                <ul>
                                    <li className={`formTab is-active`} onClick={tabClicked} data-tab="tab1">
                                        <a>
                                            <ProgressIcon className="has-background-hblack has-text-white" value={progress} icon={<i className="fas fa-check"></i>}></ProgressIcon> &nbsp;
                                            General
                                        </a>
                                    </li>
                                    <li className={`formTab`} onClick={tabClicked} data-tab="tab2">
                                        <a>
                                            <ProgressIcon className="has-background-hblack has-text-white" value={progress} icon={<i className="fas fa-pills"></i>}></ProgressIcon> &nbsp;
                                            Asignación de rol
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="column is-3">

                            <ProgressBar
                                className="has-background-hblack"
                                value={progress}
                            ></ProgressBar>
                        </div>
                    </div>
                </section>


                {/* forms */}
                <section className="section px-0 is-flex-grow-1 py-4 coolscroll" style={{height: '1px', overflow: 'hidden', minHeight: '500px'}}>
                    <div id="tab1" className="tab is-active" style={{height: '100%'}}>

                        <form id="form1.1" onSubmit={handleSubmit} className="coolscroll pr-3 py-3" style={{height: '100%', overflowX: 'scroll'}}>


                            <FormField label="Nombres y Apellidos" number="1" name="field1">
                                <input id="name" className="input cool-input" type="text" name="field1"  onChange={onChange}/>
                            </FormField>

                            <FormField label="Contraseña" number="2" name="field2">
                                <input id="password" className="input cool-input" type="text" name="field2"  onChange={onChange}/>
                            </FormField>

                            <FormField label="Correo electronico" number="3" name="field3">
                                <input id="email" className="input cool-input" type="text" name="field3"  onChange={onChange}/>
                            </FormField>


                            <FormField label="Cargo" number="4" name="field4">
                                <input id="charge" className="input cool-input" type="text" name="field4"  onChange={onChange}/>
                            </FormField>

                            <FormField label="Número de contacto" number="5" name="field5" noline>
                                <input id="phone" className="input cool-input" type="text" name="field5"  onChange={onChange}/>
                            </FormField>



                            <br/>

                            <div className="field has-text-right has-text-centered-mobile">
                                <div className="control">
                                    <button
                                        className="button is-hblack"
                                        type="submit"
                                        style={{width: '150px'}}
                                        disabled={false}
                                    >
                                        SIGUIENTE
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>

                    <div id="tab2" className="tab"  style={{height: '100%'}}>
                        <form id="form1.2" onSubmit={handleSubmit} className="coolscroll pr-3 py-3" style={{height: '100%', overflowX: 'hidden'}}>

                            <div className="columns">
                                <hr/>
                                <div className="column">
                                    <RolAssigmentCrs/>
                                </div>
                                <div className="column">
                                    <Support/>
                                </div>
                                <div className="column">
                                    <Sections/>
                                </div>
                            </div>

                            <br/>

                            <div className="field has-text-right has-text-centered-mobile">
                                <div className="control">
                                    <button
                                        className="button is-hblack"
                                        type="submit"
                                        style={{width: '150px'}}
                                        disabled={submitDisabled}
                                    >
                                        CREAR ROL
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>

                </section>



        </div>
    );
}

export default ConfigurationCreate;
