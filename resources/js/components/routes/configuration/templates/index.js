import {useState, useEffect} from 'react';

import CreateFilter from '../../../base/buttons/create-filter';

import DraggablePanel from './draggable-panel';

import {tabClicked} from '../../../commons/tabs/tabClicked';

import {
    templatesData
} from './userTemplates';


const ConfigurationTemplates = props => {

    const [templates, setTemplates] = useState(templatesData);

    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

        {/* header */}
        <section className="section p-0">
            <h1 className="title is-3 mb-5"><strong className="has-text-hblack">CONFIGURACIÓN</strong></h1>
            <h2 className="subtitle has-text-hblack-light">Historial de plantillas</h2>
        </section>



            <br/>
            <br/>



            {/* forms */}
            <section className="section px-0 is-flex-grow-1 py-4 coolscroll" style={{height: '1px', overflow: 'hidden', minHeight: '500px'}}>
                <div className="py-4 coolscroll" style={{height: '100%', overflowY: 'scroll', overflowX: 'hidden'}}>
                    <DraggablePanel items={templates} setItems={setTemplates}/>
                </div>
            </section>



        </div>
    );
}

export default ConfigurationTemplates;
