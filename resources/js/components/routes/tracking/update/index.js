import {useState} from 'react';


import CreateBtn from '../../../base/buttons/create-btn';
import FormField from '../../../form/formfield';
import FormField2 from '../../../form/formfield2';

import ProgressIcon from '../../../base/progress-icon';
import ProgressBar from '../../../base/progress-bar';

import Dropdown from '../../../form/dropdown';
import Radio from '../../../form/radio';
import Date from '../../../form/date';

import {Link} from 'react-router-dom';

import {tabClicked} from '../../../commons/tabs/tabClicked';
import {areAllFieldFilled} from '../../../../utils/forms';

import Form1 from './form1';

import styles from './trackingUpdate.scss';

const FormFieldBlock = props => (
    <div>
        <div className="columns">
            <div className="column is-one-fifth">
                <Date name="asd" onChange={null} placeholder="DD-MM-AAAA"/>
            </div>
        </div>
        <div className="columns">

            <div className="column">
                <textarea className="textarea areacool" placeholder="Descripción"></textarea>
            </div>
            <div className="column">
                <input className="input cool-input" type="text" placeholder="Próximo contacto"/>
                <br/>
                <br/>
                <input className="input cool-input" type="text" placeholder="Autor"/>
            </div>
            <div className="column">
                <Date name="asd" onChange={null} placeholder="Ultima recolección"/>
                <br/>
                <br/>
                <Date name="asd" onChange={null} placeholder="Proxima recolección"/>
            </div>
            <div className="column">
                <Date name="asd" onChange={null} placeholder="Inicio PAAP"/>
                <br/>
                <br/>
                <Date name="asd" onChange={null} placeholder="Fin PAAP"/>
            </div>
            <div className="column">
                <input className="input cool-input" type="text" placeholder="Codigo argus"/>
                <br/>
                <br/>
                <input className="input cool-input" type="text" placeholder="Próximo contacto"/>

            </div>
        </div>
    </div>
);



const countProgress = (form, fields) => {

    let count = 0;

    for(let i = 0; i < fields; i++)
        if(form[`check_field${i+1}`].checked)
            count++;

    return count / fields * 100;
}


const TrackingUpdatePatient = props => {

    const [submitDisabled, setSubmitDisabled] = useState(true);
    const [progress, setProgress] = useState(0);

    const handleSubmit = e => {
        e.preventDefault();

        const form = e.target;

        const formData = {};

        for(let i = 0; i < form.length; i++){

            if(form[i].id != '')
                Object.defineProperty(formData, form[i].id, {value: form[i].value});
        }

        console.log(formData);
    }

    const onChange = e => {



        const form = e.target.form;

        //get the name of current field
        const name = e.target.name;

        //look for the linked check
        const check = form[`check_${name}`];

        check.checked = e.target.value != '' ? true : false;

        // //submit btn
        // const allFilled = areAllFieldFilled(form, 3);
        //
        // setSubmitDisabled( allFilled ? false : true );
        //
        // //progress
        // const _progress = countProgress(form, 3);
        // setProgress( _progress );
    }


    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>



                {/* header */}
                <section className="section p-0">
                    <h1 className="subtitle is-3 mb-3"><strong className="has-text-hblue">SEGUIMIENTO</strong></h1>
                    <h2 className="subtitle has-text-hblue">EDITAR</h2>

                </section>

                <hr/>

                {/* tabs */}
                <section className="section p-0 m-0">
                    <div className="columns is-vcentered">

                        <div className="column">
                            <div className="tabs is-boxed trackingTabs">
                                <ul>
                                    <li className={`formTab is-active ${styles.tabs}`} onClick={tabClicked} data-tab="tab1">
                                        <a>
                                            <ProgressIcon className="has-background-hblue has-text-white" value={progress} icon={<i className="fas fa-check"></i>}></ProgressIcon> &nbsp;
                                            Paciente
                                        </a>
                                    </li>
                                    <li className={`formTab ${styles.tabs}`} onClick={tabClicked} data-tab="tab2">
                                        <a>
                                            <ProgressIcon className="has-background-hblue has-text-white" value={progress} icon={<i className="fas fa-pills"></i>}></ProgressIcon> &nbsp;
                                            Tratamiento
                                        </a>
                                    </li>
                                    <li className={`formTab ${styles.tabs}`} onClick={tabClicked} data-tab="tab3">
                                        <a>
                                            <ProgressIcon className="has-background-hblue has-text-white" value={progress} icon={<i className="far fa-comment-dots"></i>}></ProgressIcon> &nbsp;
                                            Comunicaciones
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="column is-3">

                            <ProgressBar
                                className="has-background-hblue"
                                value={progress}
                            ></ProgressBar>
                        </div>
                    </div>
                </section>


                {/* forms */}
                <section className="section px-0 is-flex-grow-1 py-4 coolscroll" style={{height: '1px', overflow: 'hidden', minHeight: '500px'}}>
                    <div id="tab1" className="tab is-active" style={{height: '100%'}}>
                        <Form1/>
                    </div>

                    <div id="tab2" className="tab"  style={{height: '100%'}}>
                        <form id="form1.2" onSubmit={handleSubmit} className="coolscroll pr-3 py-3" style={{height: '100%', overflowX: 'scroll'}}>

                            <FormField2
                                number1="1" number2="1.1" name1="field1" name2="field1.1"
                                label1="Visita inicial"
                                input1={<input className="input cool-input" type="date" name="field1" onChange={onChange}/>}
                                label2="Visita inicial efectiva* "
                                input2={<Radio name="field1.1" onChange={onChange}/>}
                            />

                            <FormField2
                                number1="2" number2="2.1" name1="field2" name2="field2.1"
                                label1="Reclamo*"
                                input1={<Radio name="field2" onChange={onChange}/>}
                                label2="cambio* "
                                input2={<Radio name="field2.1" onChange={onChange}/>}
                            />

                            <FormField label="Medicamento hasta*" number="3" name="field3">
                                <input className="input cool-input" type="date" name="field3"  onChange={onChange}/>
                            </FormField>

                            <FormField2
                                number1="4" number2="4.1" name1="field4" name2="field4.1"
                                label1="Se logro la comunicación*"
                                input1={<Radio name="field4" onChange={onChange}/>}
                                label2="Motivo de la comunicación*"
                                input2={<Dropdown name="field4.1" onChange={onChange} options={["op1", "op2", "op3"]} arrowColor="has-text-hblue"/>}
                            />

                            <FormField2
                                number1="5" number2="5.1" name1="field5" name2="field5.1"
                                label1="Medio de contacto*"
                                input1={<Dropdown name="field5" onChange={onChange} options={["op1", "op2", "op3"]} arrowColor="has-text-hblue"/>}
                                label2="Tipo de llamada*"
                                input2={<Dropdown name="field5.1" onChange={onChange} options={["op1", "op2", "op3"]} arrowColor="has-text-hblue"/>}
                            />

                            <FormField2
                                number1="6" number2="6.1" name1="field6" name2="field6.1"
                                label1="Motivo de no comunicación*"
                                input1={<Dropdown name="field6" onChange={onChange} options={["op1", "op2", "op3"]} arrowColor="has-text-hblue"/>}
                                label2="Número de intentos*"
                                input2={<input className="input cool-input" type="text" name="field6.1"  onChange={onChange}/>}
                            />

                            <FormField2
                                number1="7" number2="7.1" name1="field7" name2="field7.1"
                                label1="Asegurador*"
                                input1={<Dropdown name="field7" onChange={onChange} options={["op1", "op2", "op3"]} arrowColor="has-text-hblue"/>}
                                label2="IPS que atiende*"
                                input2={<Dropdown name="field7.1" onChange={onChange} options={["op1", "op2", "op3"]} arrowColor="has-text-hblue"/>}
                            />

                            <FormField2
                                number1="8" number2="8.1" name1="field8" name2="field8.1"
                                label1="Medico*"
                                input1={<Dropdown name="field8" onChange={onChange} options={["op1", "op2", "op3"]} arrowColor="has-text-hblue"/>}
                                label2="Operador logistico*"
                                input2={<Dropdown name="field8.1" onChange={onChange} options={["op1", "op2", "op3"]} arrowColor="has-text-hblue"/>}
                            />

                            <FormField2
                                number1="9" number2="9.1" name1="field9" name2="field9.1"
                                label1="Punto de entrega"
                                input1={<input className="input cool-input" type="text" name="field9"  onChange={onChange}/>}
                                label2="Ciudad de entrega*"
                                input2={<Dropdown name="field9.1" onChange={onChange} options={["op1", "op2", "op3"]} arrowColor="has-text-hblue"/>}
                            />

                            <FormField label="Numero de autorización*" number="10" name="field10">
                                <Dropdown name="field10"  onChange={onChange} options={["op1", "op2", "op3"]} arrowColor="has-text-hblue"/>
                            </FormField>

                            <FormField2
                                number1="11" number2="11.1" name1="field11" name2="field11.1"
                                label1="Estado farmacia"
                                input1={<Dropdown name="field11" onChange={onChange} options={["op1", "op2", "op3"]} arrowColor="has-text-hblue"/>}
                                label2="Dificultad en el acceso"
                                input2={<Radio name="field11.1" onChange={onChange}/>}
                            />

                            <FormField label="Tipo de dificultad*" number="12" name="field12">
                                <input className="input cool-input" type="text" name="field12"  onChange={onChange}/>
                            </FormField>

                            <FormField2
                                number1="13" number2="13.1" name1="field13" name2="field13.1"
                                label1="Autor"
                                input1={<input className="input cool-input" type="text" name="field13"  onChange={onChange}/>}
                                label2="Genera solicitud*"
                                input2={<Radio name="field13.1" onChange={onChange}/>}
                            />

                            <FormField label="Evento adverso*" number="14" name="field14">
                                <Radio name="field14" onChange={onChange}/>
                            </FormField>

                            <FormField2
                                number1="15" number2="15.1" name1="field15" name2="field15.1"
                                label1="Fecha de la proxima llamada*"
                                input1={<input className="input cool-input" type="date" name="field15"  onChange={onChange}/>}
                                label2="Motivo de la proxima llamada*"
                                input2={<Dropdown name="field15.1" onChange={onChange} options={["op1", "op2", "op3"]} arrowColor="has-text-hblue"/>}
                            />

                            <FormField label="Observaciones próxima llamada*" number="16" name="field16">
                                <input className="input cool-input" type="text" name="field16"  onChange={onChange}/>
                            </FormField>

                            <FormField2
                                number1="17" number2="17.1" name1="field17" name2="field17.1"
                                label1="Consecutivo"
                                input1={<input className="input cool-input" type="date" name="field17"  onChange={onChange}/>}
                                label2="Paciente hace parte del PAAP*"
                                input2={<Dropdown name="field17.1" onChange={onChange} options={["op1", "op2", "op3"]} arrowColor="has-text-hblue"/>}
                            />

                            <FormField label="Número de cajas / unidades" number="18" name="field18">
                                <input className="input cool-input" type="text" name="field18"  onChange={onChange}/>
                            </FormField>

                            <FormField2
                                number1="19" number2="19.1" name1="field19" name2="field19.1"
                                label1="Medicamento"
                                input1={<input className="input cool-input" type="text" name="field19"  onChange={onChange}/>}
                                label2="Dosis tratamiento*"
                                input2={<Dropdown name="field19.1" onChange={onChange} options={["op1", "op2", "op3"]} arrowColor="has-text-hblue"/>}
                            />

                            <FormField label="Número lotes de los dispositivos*" number="20" name="field20">
                                <input className="input cool-input" type="text" name="field20"  onChange={onChange}/>
                            </FormField>

                            <FormField2
                                number1="21" number2="21.1" name1="field21" name2="field21.1"
                                label1="Estado del paciente"
                                input1={<input className="input cool-input" type="text" name="field21"  onChange={onChange}/>}
                                label2="Envios"
                                input2={<Radio name="field21.1" onChange={onChange}/>}
                            />

                            <FormField label="Tipo de envio" number="22" name="field22">
                                <Dropdown name="field22.1" onChange={onChange} options={["op1", "op2", "op3"]} arrowColor="has-text-hblue"/>
                            </FormField>

                            <FormField label="Descripción de comunicación" number="23" name="field23">
                                <input className="input cool-input" type="text" name="field23"  onChange={onChange}/>
                            </FormField>


                            <FormField label="Notas" number="24" name="field24">
                                <input className="input cool-input" type="text" name="field24"  onChange={onChange}/>
                            </FormField>

                            <FormField label="seleccionar archivo" number="25" name="field25" noline>
                                <input className="input cool-input" type="text" name="field25"  onChange={onChange}/>
                            </FormField>








                            <br/>

                            <div className="field has-text-right has-text-centered-mobile">
                                <div className="control">
                                    <button
                                        className="button is-hblue"
                                        type="submit"
                                        style={{width: '150px'}}
                                        disabled={submitDisabled}
                                    >
                                        CREAR
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>

                    <div id="tab3" className="tab"  style={{height: '100%'}}>
                        <form id="form1.3" onSubmit={handleSubmit} className="coolscroll pr-3 py-3" style={{height: '100%', overflowX: 'scroll'}}>
                            {
                                [1,2,3,4,5,6,7,8,9,10].map((el, i) =>
                                    <div key={i}>
                                        <FormFieldBlock/>
                                        <hr/>
                                    </div>
                                )
                            }
                        </form>
                    </div>

                </section>



        </div>
    );
}

export default TrackingUpdatePatient;
