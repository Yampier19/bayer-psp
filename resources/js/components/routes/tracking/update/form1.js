import FormField from '../../../form/formfield';
import FormField2 from '../../../form/formfield2';
import Dropdown from '../../../form/dropdown';
import Radio from '../../../form/radio';

import * as Yup from 'yup';
import {useFormik} from 'formik';

import {connect} from 'react-redux';
import {set_modal_on} from '../../../../redux/actions/modalActions';
import {InfoModal} from '../../../base/modal-system/models/modal';

const Form = props => {

    const formSchema = Yup.object().shape({
        codigoUsuario: Yup.string().required('Este campo es obligatorio'),
        estadoPaciente: Yup.string().required('Este campo es obligatorio'),
        fechaActivacion: Yup.string().required('Este campo es obligatorio'),
    });

    const formik = useFormik({
        initialValues:  {
            codigoUsuario: '',
            estadoPaciente: '',
            fechaActivacion: '',
            solicitudCambioEstadoPaciente: '',
            fechaRetiro: '',
            motivoRetiro: '',
            observacionesMotivoRetiro: '',
            nombre: '',
            apellidos: '',
            tipoDeDocumentos: '',
            numeroIdentificacion: '',
            telefono: '',
            correoElectronico: '',
            departamento: '',
            ciudad: '',
            barrio: '',
            direccion: '',
            fechaNacimiento: '',
            edad: '',
            acudiente: '',
            telefonoAcudiente: '',
            clasificacionPatolofica: '',
            fechaInicioTerapia: ''
        },
        validateOnChange: true,
        validationSchema: formSchema,
        onSubmit: async values => {
            alert(JSON.stringify(values, null, 2));
            // const {_icons} = props.modalReducer;
            // props.set_modal_on( new InfoModal('La EPS ha sido creada éxitosamente', _icons.CHECK_PRIMARY) );
        }
    });

    return(
        <form id="form" onSubmit={formik.handleSubmit} className="pr-3 py-3">
            <FormField label="Codigo usuario" number="1" name="codigoUsuario" checked={formik.values.codigoUsuario}>
                <input className="input cool-input" type="text" name="codigoUsuario" onChange={formik.handleChange} value={formik.values.codigoUsuario} />
                {formik.touched.codigoUsuario && formik.errors.codigoUsuario ? <div className="help is-danger"> {formik.errors.codigoUsuario}</div> : null }
            </FormField>

            <FormField label="Estado paciente*" number="2" name="estadoPaciente" checked={formik.values.estadoPaciente}>
                <Dropdown id="id" name="estadoPaciente" onChange={formik.handleChange} options={['op1']} arrowColor="has-text-hblue"/>
                {formik.touched.estadoPaciente && formik.errors.estadoPaciente ? <div className="help is-danger"> {formik.errors.estadoPaciente}</div> : null }
            </FormField>

            <FormField2
                number1="3" number2="3.1" name1="fechaActivacion" name2="solicitudCambioEstadoPaciente"
                checked1={formik.values.fechaActivacion} checked2={formik.values.solicitudCambioEstadoPaciente}
                label1="Fecha de activación*" label2="Solicitar cambio de estado paciente"
                input1={
                    <div>
                        <input className="input cool-input" type="date" name="fechaActivacion" onChange={formik.handleChange} value={formik.values.fechaActivacion}/>
                        {formik.touched.fechaActivacion && formik.errors.fechaActivacion ? <div className="help is-danger"> {formik.errors.fechaActivacion}</div> : null }
                    </div>
                }

                input2={
                    <div>
                        <Radio name="solicitudCambioEstadoPaciente" onChange={formik.handleChange} value={formik.values.solicitudCambioEstadoPaciente}/>
                        {formik.touched.solicitudCambioEstadoPaciente && formik.errors.solicitudCambioEstadoPaciente ? <div className="help is-danger"> {formik.errors.solicitudCambioEstadoPaciente}</div> : null }
                    </div>
                }
            />

            <FormField2
                noline
                number1="4" number2="4.1" name1="fechaActivacion" name2="motivoRetiro"
                checked1={formik.values.fechaRetiro} checked2={formik.values.motivoRetiro}
                label1="Fecha de retiro" label2="Motivo de retiro"
                input1={
                    <div>
                        <input className="input cool-input" type="date" name="fechaRetiro" onChange={formik.handleChange} value={formik.values.fechaRetiro}/>
                        {formik.touched.fechaRetiro && formik.errors.fechaRetiro ? <div className="help is-danger"> {formik.errors.fechaRetiro}</div> : null }
                    </div>
                }

                input2={
                    <div>
                        <Dropdown name="motivoRetiro" onChange={formik.handleChange} options={['asd']} arrowColor="has-text-hblue"/>
                        {formik.touched.motivoRetiro && formik.errors.motivoRetiro ? <div className="help is-danger"> {formik.errors.motivoRetiro}</div> : null }
                    </div>
                }
            />




            <br/>

            <div className="field">
                <div className="control has-text-right has-text-centered-mobile">
                    <button
                        className="button is-hblue"
                        type="submit"
                        style={{width: '150px'}}
                    >
                        CONTINUAR
                    </button>
                </div>
            </div>

        </form>
    );
}
const mapStateToPros = state => ({
    modalReducer: state.modalReducer
})

export default connect(
    mapStateToPros,
    {
        set_modal_on
    }
)(Form);
