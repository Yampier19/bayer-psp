import {useState, useEffect} from 'react';

import FilterBar from '../../../base/filter-bar';

import {Link} from 'react-router-dom';

import axios from 'axios';
import splitEvery from '../../../../utils/columnsSplitter';

import './main.scss';

const Tracking = props => {

    const url = 'https://jsonplaceholder.typicode.com/posts';
    const [news, setNews] = useState([]);

    useEffect(
        () => {

            axios.get(url)
            .then(res => {

                setNews(res.data.splice(0, 50));

            })
            .then(err => console.log(err));

        }, []
    );

    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section px-0 py-0">
                <h1 className="title has-text-hblue is-2">Seguimiento</h1>
            </section>

            {/* first level - filters */}
            <section className="section px-0 py-6">

                <div className="columns">
                    <div className="column">
                        <FilterBar
                        textColorClass="has-text-hblue"
                        filters={[
                            {
                                name: "PAP",
                                options: [{name: "op1"}, {name: "op2"}]
                            },
                            {
                                name: "Código de usuario",
                                options: []
                            },
                            {
                                name: "Id tratamiento",
                                options: []
                            },
                            {
                                name: "Estado",
                                options: []
                            },
                        ]}
                        moreFiltersOn
                        tags={null}
                    />
                    </div>
                    <div className="column is-2-desktop has-text-right has-text-left-mobile">
                        <div className="field">
                            <div className="control has-icons-right">
                                <input className="input searchbar" type="text" placeholder="Buscar" />
                                <span className="icon is-small is-right">
                                    <i className="fas fa-search"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>





            </section>



            <br/>

            {/* second level - table */}
            <section className="is-flex-grow-1" style={{minHeight: '500px', overflowY: 'hidden'}}>
                <div className="coolscroll hblue" style={{overflowX: 'scroll', height:"100%", width: "100%"}}>
                    <div className="columns">
                        <div className="column">
                            <div className="box table-header has-background-hblue-light"  style={{minWidth: '1450px'}}>
                            <div className="columns has-text-hblue is-mobile fila" >

                                <div className="column is-1 hblue">PAP</div>
                                <div className="column is-1 hblue">Cod. usuario</div>
                                <div className="column is-1 hblue">Id Trát.</div>
                                <div className="column is-3 hblue" style={{minWidth: '200px'}}>Producto</div>
                                <div className="column is-2 hblue">Gestión</div>
                                <div className="column is-2 hblue">Próx. Contacto</div>
                                <div className="column is-2 no-border">Responsable</div>



                            </div>
                            </div>
                            <br/>

                            {
                            news.map( (neww,i) =>

                                <div className="mb-6" style={{position: 'relative'}}>
                                    <div className="box" key={i} style={{minWidth: '1450px'}}>
                                        <div className="columns fila2 is-mobile">
                                            <div className="column is-1">{`PAP${i}`}</div>
                                            <div className="column is-1">{Math.floor(Math.random() * 500000) }</div>
                                            <div className="column is-1">{Math.floor(Math.random() * 500000) }</div>
                                            <div className="column is-3" style={{minWidth: '200px'}}>{neww.body.substring(0, 40)}</div>
                                            <div className="column is-2">DD-MM-AAAA</div>
                                            <div className="column is-2">DD-MM-AAAA</div>
                                            <div className="column is-2 no-border">
                                                Nombre y apellidos del asesor

                                            </div>
                                        </div>
                                    </div>
                                    <div className="has-text-right edit-btn">
                                        <Link to={`/tracking/update?row=${neww.id}`} className="button has-background-transparent has-no-border">
                                            <span className="icon">
                                                <i class="fas fa-pen"></i>
                                            </span>
                                        </Link>
                                    </div>

                                </div>
                            )
                        }
                        </div>
                        <div className="column is-1" style={{maxWidth: '60px'}}>

                        </div>
                    </div>

                </div>
            </section>

            <section className="section px-0 pb-0">

                <h1 className="subtitle has-text-hblue">
                    se encontraron {news.length} registros
                </h1>

            </section>

        </div>
    );
}

export default Tracking;
