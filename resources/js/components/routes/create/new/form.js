import FormField from '../../../form/formfield';

import {useFormik} from 'formik';
import * as Yup from 'yup';

import {InfoModal} from '../../../base/modal-system/models/modal';
import {set_modal_on} from '../../../../redux/actions/modalActions';
import {connect} from 'react-redux';

export const formSchema = Yup.object().shape({
    pap: Yup.string().required('Campo obligatorio'),
    asunto: Yup.string().required('Campo obligatorio'),
    producto: Yup.string().required('Campo obligatorio'),
    novedad: Yup.string().required('Campo obligatorio'),
    fechaReporte: Yup.string().required('Campo obligatorio'),
    fechaRespuesta: Yup.string().required('Campo obligatorio'),
    observaciones: Yup.string(),
    responsable: Yup.string().required('Campo obligatorio')
});
const Form = props => {

    const formik = useFormik({
        initialValues: {
            pap: '',
            asunto: '',
            producto: '',
            novedad: '',
            fechaReporte: '',
            fechaRespuesta: '',
            observaciones: '',
            responsable: ''
        },
        validateOnChange: true,
        validationSchema: formSchema,
        onSubmit: async values => {
            // alert(JSON.stringify(values, null, 2));
            const {_icons} = props.modalReducer;
            props.set_modal_on( new InfoModal('La novedad ha sido creada éxitosamente', _icons.CHECK_PRIMARY) );
        }
    });

    return(
        <form id="form" onSubmit={formik.handleSubmit} className="coolscroll primary pr-3 py-3">

            <FormField label="PAP" number="1" name="pap" checked={formik.values.pap}>
                <input className="input cool-input" type="text" name="pap" onChange={formik.handleChange} value={formik.values.pap} />
                {formik.touched.userCode && formik.errors.userCode ? <div className="help is-danger"> {formik.errors.userCode}</div> : null }
            </FormField>

            <FormField label="Asunto" number="2" name="asunto" checked={formik.values.asunto}>
                <input className="input cool-input" type="text" name="asunto" onChange={formik.handleChange} value={formik.values.asunto} />
                {formik.touched.asunto && formik.errors.asunto ? <div className="help is-danger"> {formik.errors.asunto}</div> : null }
            </FormField>

            <FormField label="Producto" number="3" name="producto" checked={formik.values.producto}>
                <input className="input cool-input" type="text" name="producto" onChange={formik.handleChange} value={formik.values.producto} />
                {formik.touched.producto && formik.errors.producto ? <div className="help is-danger"> {formik.errors.producto}</div> : null }
            </FormField>

            <FormField label="Novedad" number="4" name="novedad" checked={formik.values.novedad}>
                <input className="input cool-input" type="text" name="novedad" onChange={formik.handleChange} value={formik.values.novedad} />
                {formik.touched.novedad && formik.errors.novedad ? <div className="help is-danger"> {formik.errors.novedad}</div> : null }
            </FormField>

            <FormField label="Fecha de reporte" number="5" name="fechaReporte" checked={formik.values.fechaReporte}>
                <input className="input cool-input" type="text" name="fechaReporte" onChange={formik.handleChange} value={formik.values.fechaReporte} />
                {formik.touched.fechaReporte && formik.errors.fechaReporte ? <div className="help is-danger"> {formik.errors.fechaReporte}</div> : null }
            </FormField>

            <FormField label="Fecha de respuesta" number="6" name="fechaRespuesta" checked={formik.values.fechaRespuesta}>
                <input className="input cool-input" type="text" name="fechaRespuesta" onChange={formik.handleChange} value={formik.values.fechaRespuesta} />
                {formik.touched.fechaRespuesta && formik.errors.fechaRespuesta ? <div className="help is-danger"> {formik.errors.fechaRespuesta}</div> : null }
            </FormField>

            <FormField label="Observaciones" number="7" name="fechaRespuesta" checked={formik.values.observaciones}>
                <input className="input cool-input" type="text" name="observaciones" onChange={formik.handleChange} value={formik.values.observaciones} />
                {formik.touched.observaciones && formik.errors.observaciones ? <div className="help is-danger"> {formik.errors.observaciones}</div> : null }
            </FormField>

            <FormField label="Responsable" number="8" name="responsable" checked={formik.values.responsable} noline>
                <input className="input cool-input" type="text" name="responsable" onChange={formik.handleChange} value={formik.values.responsable} />
                {formik.touched.responsable && formik.errors.responsable ? <div className="help is-danger"> {formik.errors.responsable}</div> : null }
            </FormField>


            <br/>

            <div className="field">
                <div className="control has-text-right has-text-centered-mobile">
                    <button
                        className="button is-primary"
                        type="submit"
                        style={{width: '150px'}}
                    >
                        CREAR
                    </button>
                </div>
            </div>



        </form>
    );
}

const mapStateToPros = state => ({
    modalReducer: state.modalReducer
})

export default connect(
    mapStateToPros,
    {
        set_modal_on
    }
)(Form);
