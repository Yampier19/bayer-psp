import {useState, useEffect} from 'react';
import Sidebar from '../../../commons/sidebar';

import CreateBtn from '../../../base/buttons/create-btn';
import FormField from '../../../form/formfield';
import Dropdown from '../../../form/dropdown';
import Radio from '../../../form/radio';
import FormField2 from '../../../form/formfield2';

import ProgressIcon from '../../../base/progress-icon';
import ProgressBar from '../../../base/progress-bar';

import {Link} from 'react-router-dom';

import {tabClicked} from '../../../commons/tabs/tabClicked';
import {getRequired} from '../../../../utils/forms';

import {formPart1, formPart1Schema,
formPart2, formPart2Schema} from './formData';
import {useFormik} from 'formik';


//import {formik} from './formData';

import './tabs.scss';

const CreatePatient = props => {

    const [currentTab, setCurrentTab] = useState(0);

    /* *~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~* */
    const [progress, setProgress] = useState(0);

    const formik = useFormik({
        initialValues: formPart1.initialValues,
        validateOnChange: true,
        validationSchema: formPart1Schema,
        onSubmit: async values => {
            alert(JSON.stringify(values, null, 2));            
        }
    });

    const requiredFields = getRequired(formPart1Schema);

    const customHandleChange = e => {

        //handle formik change
        formik.handleChange(e);
        const form = e.target.form;
        //check if element is required and them count progress
        const count = requiredFields.filter( fieldName => form[fieldName].value != '').length;
        setProgress(count/requiredFields.length * 100);
    }


    /* *~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~* */
    const [progress2, setProgress2] = useState(0);

    const formik2 = useFormik({
        initialValues: formPart2.initialValues,
        validateOnChange: true,
        validationSchema: formPart2Schema,
        onSubmit: async values => {
            console.log(JSON.stringify(values, null, 2));
            alert(JSON.stringify(values, null, 2));
        }
    });

    const requiredFields2 = getRequired(formPart2Schema);

    const customHandleChange2 = e => {

        //handle formik change
        formik2.handleChange(e);
        const form = e.target.form;
        console.log(e.target);
        //check if element is required and them count progress
        const count = requiredFields2.filter( fieldName => form[fieldName].value != '').length;
        setProgress2(count/requiredFields2.length * 100);
    }



    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>



            {/* header */}
            <section className="section px-0 pt-0 pb-3 ">
                <div className="columns is-mobile">
                    <div className="column is-3-desktop ">
                        <h1 className="subtitle has-text-primary is-3 mb-1"  style={{ whiteSpace: 'nowrap'}}>
                            <strong className="has-text-primary">CREAR</strong>
                            <span className="is-hidden-touch"> - PACIENTE NUEVO</span >
                        </h1>
                        <h1 className="subtitle has-text-primary is-hidden-desktop is-6">Paciente nuevo</h1>
                    </div>
                    <div className="column is-3-desktop is-offset-6-desktop is-6-mobile">
                        <CreateBtn active="2"/>
                    </div>
                </div>
            </section>

            <hr className="has-background-dark is-hidden-touch"/>

            {/* tabs */}
            <section className="section p-0 m-0 px-1">
                <div className="columns is-vcentered">

                    <div className="column">
                        <div className="tabs is-boxed createTab">
                            <ul>
                                <li className="formTab is-active" onClick={e => {tabClicked(e); setCurrentTab(1)}} data-tab="tab1">
                                    <a>
                                        <ProgressIcon className="has-background-primary has-text-white" value={progress} color="#519EA1" icon={<i class="fas fa-user"></i>}/>&nbsp;
                                        <span className="is-hidden-touch">General</span>
                                    </a>
                                </li>
                                <li className="formTab" onClick={e => {tabClicked(e); setCurrentTab(2)}} data-tab="tab2">
                                    <a>
                                        <ProgressIcon className="has-background-primary has-text-white" value={progress2} color="#519EA1" icon={<i class="fas fa-pills"></i>}></ProgressIcon> &nbsp;
                                        <span className="is-hidden-mobile">Información de tratamiento</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div className="column is-hidden-desktop">
                        <span className="title has-text-primary is-5">{currentTab == 1 ? 'General' : 'Información del tratamiento'}</span>
                    </div>

                    <div className="column is-3">

                        <ProgressBar className="has-background-primary" value={currentTab == 1 ? progress : progress2}></ProgressBar>
                    </div>
                </div>
            </section>

            {/* forms */}
            <section className="section px-0 is-flex-grow-1 py-4 coolscroll" style={{height: '1px', overflow: 'hidden', minHeight: '500px'}}>
                <div id="tab1" className="tab is-active">
                    <form id="form" onSubmit={formik.handleSubmit} className="coolscroll primary pr-3 py-3">
                        <FormField label="Código de usuario" number="1" name="userCode" checked={formik.values.userCode != ''}>
                            <input className="input cool-input" type="text" name="userCode" onChange={customHandleChange} value={formik.values.userCode} />
                            {formik.touched.userCode && formik.errors.userCode ? <div className="help is-danger"> {formik.errors.userCode}</div> : null }
                        </FormField>

                        <FormField label="Estado del paciente*" number="2" name="patientStatus" checked={formik.values.patientStatus != ''}>
                            <Dropdown options={["bien", "mal"]} name="patientStatus" arrowColor="has-text-primary" onChange={customHandleChange} value={formik.values.patientStatus}/>
                            {formik.touched.patientStatus && formik.errors.patientStatus ? <div className="help is-danger"> {formik.errors.patientStatus}</div> : null }
                        </FormField>

                        <FormField label="Fecha de activación*" number="3" name="activationDate" checked={formik.values.activationDate != ''}>
                            <input className="input cool-input" type="date" name="activationDate" onChange={customHandleChange} value={formik.values.activationDate}/>
                            {formik.touched.activationDate && formik.errors.activationDate ? <div className="help is-danger"> {formik.errors.activationDate}</div> : null }
                        </FormField>

                        <FormField label="Correo electrónico*" number="4" name="email" checked={formik.values.email != ''}>
                            <input className="input cool-input" type="text" name="email" onChange={customHandleChange} value={formik.values.email}/>
                            {formik.touched.email && formik.errors.email ? <div className="help is-danger"> {formik.errors.email}</div> : null }
                        </FormField>

                        <FormField2
                            number1="5" number2="5.1" name1="name" name2="surname" checked1={formik.values.name != ''} checked2={formik.values.surname != ''}
                            label1="Nombres*"
                            input1={
                                <div>
                                    <input className="input cool-input" type="text" name="name" onChange={customHandleChange} value={formik.values.name}/>
                                    {formik.touched.name && formik.errors.name ? <div className="help is-danger"> {formik.errors.name}</div> : null }
                                </div>
                            }
                            label2="Apellidos*"
                            input2={
                                <div>
                                    <input className="input cool-input" type="text" name="surname" onChange={customHandleChange} value={formik.values.surname}/>
                                    {formik.touched.surname && formik.errors.surname ? <div className="help is-danger"> {formik.errors.surname}</div> : null }
                                </div>
                            }
                        />

                        <FormField label="Identificación*" number="6" name="identification" checked={formik.values.identification != ''}>
                            <input className="input cool-input" type="text" name="identification" onChange={customHandleChange} value={formik.values.identification}/>
                            {formik.touched.identification && formik.errors.identification ? <div className="help is-danger"> {formik.errors.identification}</div> : null }
                        </FormField>

                        <FormField label="Telefono*" number="7" name="phone" checked={formik.values.phone != ''}>
                            <input className="input cool-input" type="text" name="phone" onChange={customHandleChange} value={formik.values.phone}/>
                            <div class="help">
                                <div className="level is-mobile">
                                    <div className="level-right has-text-danger">{formik.touched.phone && formik.errors.phone ? formik.errors.phone : ''}</div>
                                    <div className="level-left"><a><u>Agregar telefono</u></a></div>
                                </div>
                            </div>
                        </FormField>

                        <FormField2
                            number1="8" number2="8.1" name1="department" name2="city" checked1={formik.values.department != ''} checked2={formik.values.city != ''}
                            label1="Departamento*"
                            input1={
                                <div>
                                    <Dropdown options={["op1", "op2"]} name="department" arrowColor="has-text-primary" onChange={customHandleChange} value={formik.values.department}/>
                                    {formik.touched.department && formik.errors.department ? <div className="help is-danger"> {formik.errors.department}</div> : null }
                                </div>
                            }
                            label2="Ciudad* "
                            input2={
                                <div>
                                    <Dropdown options={["op1", "op2"]} name="city" arrowColor="has-text-primary"  onChange={customHandleChange}  value={formik.values.city}/>
                                    {formik.touched.city && formik.errors.city ? <div className="help is-danger"> {formik.errors.city}</div> : null }
                                </div>
                            }
                        />

                        <FormField2
                          number1="9" number2="9.1" name1="neighborhood" name2="address" checked1={formik.values.neighborhood != ''} checked2={formik.values.address != ''}
                          label1="Barrio*"
                          input1={
                              <div>
                                  <input className="input cool-input" type="text" name="neighborhood" onChange={customHandleChange} value={formik.values.neighborhood}/>
                                  {formik.touched.neighborhood && formik.errors.neighborhood ? <div className="help is-danger"> {formik.errors.neighborhood}</div> : null }
                              </div>
                          }
                          label2="Dirección* "
                          input2={
                              <div>
                                  <input className="input cool-input" type="text" name="address" onChange={customHandleChange}  value={formik.values.address}/>
                                  {formik.touched.address && formik.errors.address ? <div className="help is-danger"> {formik.errors.address}</div> : null }
                              </div>
                          }
                      />

                      <FormField2
                          number1="10" number2="10.1" name1="via" name2="viaDetails" checked1={formik.values.via != ''} checked2={formik.values.viaDetails != ''}
                          label1="Via"
                          input1={
                              <div>
                                  <Dropdown options={["op1", "op2"]} name="via" arrowColor="has-text-primary" onChange={customHandleChange} value={formik.values.via}/>
                                  {formik.touched.via && formik.errors.via ? <div className="help is-danger"> {formik.errors.via}</div> : null }
                              </div>
                          }
                          label2="Detalles via"
                          input2={
                              <div>
                                  <Dropdown options={["op1", "op2"]} name="viaDetails" arrowColor="has-text-primary"  onChange={customHandleChange}  value={formik.values.viaDetails}/>
                                  {formik.touched.viaDetails && formik.errors.viaDetails ? <div className="help is-danger"> {formik.errors.viaDetails}</div> : null }
                              </div>
                          }
                      />

                      <FormField2
                          number1="11" number2="11.1" name1="numberplus" name2="mobileNumber" checked1={formik.values.numberplus != ''} checked2={formik.values.mobileNumber != ''}
                          label1="Numero"
                          input1={
                              <div>
                                  <Dropdown options={["op1", "op2"]} name="numberplus" arrowColor="has-text-primary" onChange={customHandleChange} value={formik.values.numberplus}/>
                                  {formik.touched.numberplus && formik.errors.numberplus ? <div className="help is-danger"> {formik.errors.numberplus}</div> : null }
                              </div>
                          }
                          label2=""
                          input2={
                              <div>
                                  <input className="input cool-input" type="text" name="mobileNumber" onChange={customHandleChange}  value={formik.values.mobileNumber}/>
                                  {formik.touched.mobileNumber && formik.errors.mobileNumber ? <div className="help is-danger"> {formik.errors.mobileNumber}</div> : null }
                              </div>
                          }
                      />

                      <FormField2
                          number1="12" number2="12.1" name1="interior" name2="interiorDetails" checked1={formik.values.interior != ''} checked2={formik.values.interiorDetails != ''}
                          label1="Interior"
                          input1={
                              <div>
                                  <input className="input cool-input" type="text" name="interior" onChange={customHandleChange} value={formik.values.interior}/>
                                  {formik.touched.interior && formik.errors.interior ? <div className="help is-danger"> {formik.errors.interior}</div> : null }
                              </div>
                          }
                          label2="Detalles interior"
                          input2={
                              <div>
                                  <input className="input cool-input" type="text" name="interiorDetails" onChange={customHandleChange} value={formik.values.interiorDetails}/>
                                  <div class="help">
                                      <div className="level is-mobile">
                                          <div className="level-right has-text-danger">{formik.touched.interiorDetails && formik.errors.interiorDetails ? formik.errors.interiorDetails : ''}</div>
                                          <div className="level-left"><a><u>Agregar interior</u></a></div>
                                      </div>
                                  </div>
                              </div>
                          }
                      />

                      <FormField label="Genero*" number="13" name="gender" checked={formik.values.gender != ''}>
                          <Dropdown options={["Masculino", "Femenino"]} name="gender" arrowColor="has-text-primary" onChange={customHandleChange} value={formik.values.gender}/>
                          {formik.touched.gender && formik.errors.gender ? <div className="help is-danger"> {formik.errors.gender}</div> : null }
                      </FormField>

                      <FormField2
                          number1="14" number2="14.1" name1="birthdate" name2="age" checked1={formik.values.birthdate != ''} checked2={formik.values.age != ''}
                          label1="Fecha de nacimiento*"
                          input1={
                              <div>
                                  <input className="input cool-input" type="date" name="birthdate" onChange={customHandleChange} value={formik.values.birthdate}/>
                                  {formik.touched.birthdate && formik.errors.birthdate ? <div className="help is-danger"> {formik.errors.birthdate}</div> : null }
                              </div>
                          }
                          label2="Edad"
                          input2={
                              <div>
                                  <input className="input cool-input" type="text" name="age" onChange={customHandleChange} value={formik.values.age}/>
                                  {formik.touched.age && formik.errors.age ? <div className="help is-danger"> {formik.errors.age}</div> : null }
                              </div>
                          }
                      />

                      <FormField2
                          number1="15" number2="15.1" name1="guardian" name2="guardianPhone" checked1={formik.values.guardian != ''} checked2={formik.values.guardianPhone != ''}
                          label1="Acudiente"
                          input1={
                              <div>
                                  <input className="input cool-input" type="text" name="guardian" onChange={customHandleChange} value={formik.values.guardian}/>
                                  {formik.touched.guardian && formik.errors.guardian ? <div className="help is-danger"> {formik.errors.guardian}</div> : null }
                              </div>
                          }
                          label2="Telefono acudiente"
                          input2={
                              <div>
                                  <input className="input cool-input" type="text" name="guardianPhone" onChange={customHandleChange} value={formik.values.guardianPhone}/>
                                  {formik.touched.guardianPhone && formik.errors.guardianPhone ? <div className="help is-danger"> {formik.errors.guardianPhone}</div> : null }
                              </div>
                          }
                      />

                      <FormField label="Notas" number="16" name="notes" checked={formik.values.notes != ''}>
                          <input className="input cool-input" type="text" name="notes" onChange={customHandleChange} value={formik.values.notes} />
                          {formik.touched.notes && formik.errors.notes ? <div className="help is-danger"> {formik.errors.notes}</div> : null }
                      </FormField>

                      <FormField label="" number="17" name="file" checked={formik.values.file != ''} noline>
                          <button className="button is-rounded is-primary">Seleccionar archivo</button>
                      </FormField>


                      <br/>

                      <div className="field has-text-right has-text-centered-mobile">
                            <div className="control">
                                <button
                                    className="button is-primary"
                                    type="submit"
                                    data-tab="tab1"
                                    style={{width: '150px'}}
                                    disabled={false}
                                >
                                    Continuar
                                </button>
                            </div>
                        </div>

                    </form>
                </div>

                <div id="tab2" className="tab coolscroll coolscroll primary" style={{overflowY: 'scroll'}}>
                    <form id="form2" onSubmit={formik2.handleSubmit} className="pr-3 py-3">
                        <FormField label="Id tratamiento" number="1" name="treatmentCode" checked={formik2.values.treatmentCode != ''}>
                            <input className="input cool-input" type="text" name="treatmentCode" onChange={customHandleChange2} value={formik2.values.treatmentCode} />
                            {formik2.touched.treatmentCode && formik2.errors.treatmentCode ? <div className="help is-danger"> {formik2.errors.treatmentCode}</div> : null }
                        </FormField>

                        <FormField2
                            number1="2" number2="2.1"
                            checked1={formik2.values.isClaim} checked2={formik2.values.wasInfoGiven}
                            label1="Reclamo*" label2="¿Se brindo Información?"
                            input1={
                                <div>
                                    <Radio name="isClaim" onChange={customHandleChange2} value={formik2.values.isClaim}/>
                                    {formik2.touched.isClaim && formik2.errors.isClaim ? <div className="help is-danger"> {formik2.errors.isClaim}</div> : null }
                                </div>
                            }
                            input2={
                                <div>
                                    <Radio name="wasInfoGiven" onChange={customHandleChange2} value={formik2.values.wasInfoGiven}/>
                                    {formik2.touched.wasInfoGiven && formik2.errors.wasInfoGiven ? <div className="help is-danger"> {formik2.errors.wasInfoGiven}</div> : null }
                                </div>
                            }
                        />

                        <FormField2
                            number1="3" number2="3.1"
                            checked1={formik2.values.boxesPerUnit} checked2={formik2.values.product}
                            label1="Número cajas / unidades*" label2="Producto*"
                            input1={
                                <div>
                                    <Dropdown name="boxesPerUnit" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary" />
                                    {formik2.touched.boxesPerUnit && formik2.errors.boxesPerUnit ? <div className="help is-danger"> {formik2.errors.boxesPerUnit}</div> : null }
                                </div>
                            }
                            input2={
                                <div>
                                    <Dropdown name="product" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary" />
                                    {formik2.touched.product && formik2.errors.product ? <div className="help is-danger"> {formik2.errors.product}</div> : null }
                                </div>
                            }
                        />

                        <FormField2
                            number1="4" number2="4.1"
                            checked1={formik2.values.dose} checked2={formik2.values.startDose}
                            label1="Dosis" label2="Dosis de inicio"
                            input1={
                                <div>
                                    <Dropdown name="dose" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                    {formik2.touched.dose && formik2.errors.dose ? <div className="help is-danger"> {formik2.errors.dose}</div> : null }
                                </div>
                            }
                            input2={
                                <div>
                                    <Radio name="startDose" onChange={customHandleChange2} value={formik2.values.startDose}/>
                                    {formik2.touched.startDose && formik2.errors.startDose ? <div className="help is-danger"> {formik2.errors.startDose}</div> : null }
                                </div>
                            }
                        />

                        <FormField2
                            number1="5" number2="5.1"
                            checked1={formik2.values.patientStatus} checked2={formik2.values.pathologicalClassification}
                            label1="Estado del paciente" label2="Clasificación patológica*"
                            input1={
                                <div>
                                    <Dropdown name="patientStatus" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                    {formik2.touched.patientStatus && formik2.errors.patientStatus ? <div className="help is-danger"> {formik2.errors.patientStatus}</div> : null }
                                </div>
                            }
                            input2={
                                <div>
                                    <Dropdown name="pathologicalClassification" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                    {formik2.touched.pathologicalClassification && formik2.errors.pathologicalClassification ? <div className="help is-danger"> {formik2.errors.pathologicalClassification}</div> : null }
                                </div>
                            }
                        />

                        <FormField2
                            number1="6" number2="6.1"
                            checked1={formik2.values.previousTreatment} checked2={formik2.values.consent}
                            label1="Tratamiento previo*" label2="Consentimiento*"
                            input1={
                                <div>
                                    <Dropdown name="previousTreatment" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                    {formik2.touched.previousTreatment && formik2.errors.previousTreatment ? <div className="help is-danger"> {formik2.errors.previousTreatment}</div> : null }
                                </div>
                            }
                            input2={
                                <div>
                                    <Dropdown name="consent" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                    {formik2.touched.consent && formik2.errors.consent ? <div className="help is-danger"> {formik2.errors.consent}</div> : null }
                                </div>
                            }
                        />

                        <FormField2
                            number1="7" number2="7.1"
                            checked1={formik2.values.treatmentStartingDate} checked2={formik2.values.regimen}
                            label1="Fecha de inicio terapia*" label2="Regimen*"
                            input1={
                                <div>
                                    <input className="input cool-input" type="date" name="treatmentStartingDate" onChange={customHandleChange2} value={formik2.values.treatmentStartingDate} />
                                    {formik2.touched.treatmentStartingDate && formik2.errors.treatmentStartingDate ? <div className="help is-danger"> {formik2.errors.treatmentStartingDate}</div> : null }
                                </div>
                            }
                            input2={
                                <div>
                                    <Dropdown name="regimen" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                    {formik2.touched.regimen && formik2.errors.regimen ? <div className="help is-danger"> {formik2.errors.regimen}</div> : null }
                                </div>
                            }
                        />

                        <FormField2
                            number1="8" number2="8.1"
                            checked1={formik2.values.insurer} checked2={formik2.values.logisticOperator}
                            label1="Asegurador*" label2="Operador lógistico*"
                            input1={
                                <div>
                                    <Dropdown name="insurer" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                    {formik2.touched.insurer && formik2.errors.insurer ? <div className="help is-danger"> {formik2.errors.insurer}</div> : null }
                                </div>
                            }
                            input2={
                                <div>
                                    <Dropdown name="logisticOperator" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                    {formik2.touched.logisticOperator && formik2.errors.logisticOperator ? <div className="help is-danger"> {formik2.errors.logisticOperator}</div> : null }
                                </div>
                            }
                        />

                        <FormField2
                            number1="9" number2="9.1"
                            checked1={formik2.values.lastClaimDate} checked2={formik2.values.otherOperators}
                            label1="Fecha última reclamación" label2="Otros operadores"
                            input1={
                                <div>
                                    <input className="input cool-input" type="date" name="lastClaimDate" onChange={customHandleChange2} value={formik2.values.lastClaimDate} />
                                    {formik2.touched.lastClaimDate && formik2.errors.lastClaimDate ? <div className="help is-danger"> {formik2.errors.lastClaimDate}</div> : null }
                                </div>
                            }
                            input2={
                                <div>
                                    <Dropdown name="otherOperators" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                    {formik2.touched.otherOperators && formik2.errors.otherOperators ? <div className="help is-danger"> {formik2.errors.otherOperators}</div> : null }
                                </div>
                            }
                        />

                        <FormField2
                            number1="10" number2="10.1"
                            checked1={formik2.values.deliveryPoint} checked2={formik2.values.attendantIPS}
                            label1="Punto de entrega" label2="IPS que atiende*"
                            input1={
                                <div>
                                    <Dropdown name="deliveryPoint" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                    {formik2.touched.deliveryPoint && formik2.errors.deliveryPoint ? <div className="help is-danger"> {formik2.errors.deliveryPoint}</div> : null }
                                </div>
                            }
                            input2={
                                <div>
                                    <input name="attendantIPS" className="input cool-input" type="text"  onChange={customHandleChange2} value={formik2.values.attendantIPS} />
                                    {formik2.touched.attendantIPS && formik2.errors.attendantIPS ? <div className="help is-danger"> {formik2.errors.attendantIPS}</div> : null }
                                </div>
                            }
                        />

                        <FormField2
                            number1="11" number2="11.1"
                            checked1={formik2.values.acquisitionWays} checked2={formik2.values.nextCallDate}
                            label1="Medios de adquisición" label2="Fecha de la próxima llamada*"
                            input1={
                                <div>
                                    <input name="acquisitionWays" className="input cool-input" type="text"  onChange={customHandleChange2} value={formik2.values.acquisitionWays} />
                                    {formik2.touched.acquisitionWays && formik2.errors.acquisitionWays ? <div className="help is-danger"> {formik2.errors.acquisitionWays}</div> : null }
                                </div>
                            }
                            input2={
                                <div>
                                    <input name="nextCallDate" className="input cool-input" type="date"  onChange={customHandleChange2} value={formik2.values.nextCallDate} />
                                    {formik2.touched.nextCallDate && formik2.errors.nextCallDate ? <div className="help is-danger"> {formik2.errors.nextCallDate}</div> : null }
                                </div>
                            }
                        />

                        <FormField2
                            number1="12" number2="12.1"
                            checked1={formik2.values.medic} checked2={formik2.values.specialism}
                            label1="Médico" label2="Especialidad"
                            input1={
                                <div>
                                    <Dropdown name="medic" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                    {formik2.touched.medic && formik2.errors.medic ? <div className="help is-danger"> {formik2.errors.medic}</div> : null }
                                </div>
                            }
                            input2={
                                <div>
                                    <Dropdown name="specialism" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                    {formik2.touched.specialism && formik2.errors.specialism ? <div className="help is-danger"> {formik2.errors.specialism}</div> : null }
                                </div>
                            }
                        />

                        <FormField2
                            number1="13" number2="13.1"
                            checked1={formik2.values.representant} checked2={formik2.values.atentionZone}
                            label1="Paramédico o representante" label2="Zona de atención paramédico o representante"
                            input1={
                                <div>
                                    <Dropdown name="representant" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                    {formik2.touched.representant && formik2.errors.representant ? <div className="help is-danger"> {formik2.errors.representant}</div> : null }
                                </div>
                            }
                            input2={
                                <div>
                                    <Dropdown name="atentionZone" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                    {formik2.touched.atentionZone && formik2.errors.atentionZone ? <div className="help is-danger"> {formik2.errors.atentionZone}</div> : null }
                                </div>
                            }
                        />

                        <FormField2
                            number1="14" number2="14.1"
                            checked1={formik2.values.code} checked2={formik2.values.baseCity}
                            label1="Código" label2="Ciudad base paramédico o representante"
                            input1={
                                <div>
                                    <Dropdown name="code" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                    {formik2.touched.code && formik2.errors.code ? <div className="help is-danger"> {formik2.errors.code}</div> : null }
                                </div>
                            }
                            input2={
                                <div>
                                    <Dropdown name="baseCity" onChange={customHandleChange2} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                    {formik2.touched.baseCity && formik2.errors.baseCity ? <div className="help is-danger"> {formik2.errors.baseCity}</div> : null }
                                </div>
                            }
                        />

                        <FormField2
                            number1="15" number2="15.1"
                            checked1={formik2.values.isPatientPartOfPap} checked2={formik2.values.addInfoApps}
                            label1="Paciente hace parte del PAAP" label2="Agregar Información aplicaciones"
                            input1={
                                <div>
                                    <Radio name="isPatientPartOfPap" onChange={customHandleChange2} value={formik2.values.isPatientPartOfPap}/>
                                    {formik2.touched.isPatientPartOfPap && formik2.errors.isPatientPartOfPap ? <div className="help is-danger"> {formik2.errors.isPatientPartOfPap}</div> : null }
                                </div>
                            }
                            input2={
                                <div>
                                    <Radio name="addInfoApps" onChange={customHandleChange2} value={formik2.values.addInfoApps}/>
                                    {formik2.touched.addInfoApps && formik2.errors.addInfoApps ? <div className="help is-danger"> {formik2.errors.addInfoApps}</div> : null }
                                </div>
                            }
                        />

                        <FormField label="Tipo de envio" number="16" name="shippingType" checked={formik2.values.shippingType} noline>
                            <input className="input cool-input" type="text" name="shippingType" onChange={customHandleChange2} value={formik2.values.shippingType} />
                            <div class="help">
                                <div className="level is-mobile">
                                    <div className="level-right has-text-danger">{formik2.touched.shippingType && formik2.errors.shippingType ? formik2.errors.shippingType : ''}</div>
                                    <div className="level-left"><a><u>Agregar tratamiento</u></a></div>
                                </div>
                            </div>
                        </FormField>



                        <br/>

                        <div className="field has-text-right has-text-centered-mobile">
                            <div className="control">
                                <button
                                    className="button is-primary"
                                        type="submit"
                                        data-tab="tab1"
                                        style={{width: '150px'}}
                                        disabled={false}
                                    >
                                        Registrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </section>
        </div>
    );
}

export default CreatePatient;
