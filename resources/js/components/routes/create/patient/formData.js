import {useFormik} from 'formik';
import * as Yup from 'yup';

export const formPart1 = {
    initialValues: {
        userCode: '',
        patientStatus: '',
        activationDate: '',
        email: '',
        name: '',
        surname: '',
        identification: '',
        phone: '',
        department: '',
        city: '',
        neighborhood: '',
        address: '',
        via: '',
        viaDetails: '',
        numberplus: '',
        mobileNumber: '',
        interior: '',
        interiorDetails: '',
        gender: '',
        birthdate: '',
        age: '',
        guardian: '',
        guardianPhone: '',
        notes: '',
        file: ''
    }
};

export const formPart1Schema = Yup.object().shape({
    userCode: Yup.number().typeError('se debe especificar un valor numerico'),
    patientStatus: Yup.string().required('Campo obligatorio'),
    activationDate: Yup.string().required('Campo obligatorio'),
    email: Yup.string().required('Campo obligatorio').email('email invalido'),
    name: Yup.string().required('Campo obligatorio'),
    surname: Yup.string().required('Campo obligatorio'),
    identification: Yup.number().typeError('se debe especificar un valor numerico').required('Campo obligatorio'),
    phone: Yup.number().typeError('se debe especificar un valor numerico').required('Campo obligatorio'),
    department: Yup.string().required('Campo obligatorio'),
    city: Yup.string().required('Campo obligatorio'),
    neighborhood: Yup.string().required('Campo obligatorio'),
    address: Yup.string().required('Campo obligatorio'),
    via: Yup.string(),
    viaDetails: Yup.string(),
    numberplus: Yup.string(),
    mobileNumber: Yup.number().typeError('se debe especificar un valor numerico'),
    interior: Yup.string(),
    interiorDetails: Yup.string(),
    gender: Yup.string().required('Campo obligatorio'),
    birthdate: Yup.string().required('Campo obligatorio'),
    age: Yup.number().typeError('se debe especificar un valor numerico'),
    guardian: Yup.string(),
    guardianPhone: Yup.number().typeError('se debe especificar un valor numerico'),
    notes: Yup.string(),

});


/* *~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~* */

export const formPart2 =
{
    initialValues: {
        treatmentCode: '',
        isClaim: '',
        wasInfoGiven: '',
        boxesPerUnit: '',
        product: '',
        dose: '',
        startDose: '',
        patientStatus: '',
        pathologicalClassification: '',
        previousTreatment: '',
        consent: '',
        treatmentStartingDate: '',
        regimen: '',
        insurer: '',
        logisticOperator: '',
        lastClaimDate: '',
        otherOperators: '',
        deliveryPoint: '',
        attendantIPS: '',
        acquisitionWays: '',
        nextCallDate: '',
        medic: '',
        specialism: '',
        representant: '',
        atentionZone: '',
        code: '',
        baseCity: '',
        isPatientPartOfPap: '',
        addInfoApps: '',
        shippingType: ''
    }
};

export const formPart2Schema = Yup.object().shape({

    treatmentCode: Yup.number().typeError('se debe especificar un valor numerico'),
    isClaim: Yup.string().required('Campo obligatorio'),
    wasInfoGiven: Yup.string(),
    boxesPerUnit: Yup.string().required('Campo obligatorio'),
    product: Yup.string().required('Campo obligatorio'),
    dose: Yup.string(),
    startDose: Yup.string(),
    patientStatus: Yup.string(),
    pathologicalClassification: Yup.string().required('Campo obligatorio'),
    previousTreatment: Yup.string().required('Campo obligatorio'),
    consent: Yup.string().required('Campo obligatorio'),
    treatmentStartingDate: Yup.string().required('Campo obligatorio'),
    regimen: Yup.string().required('Campo obligatorio'),
    insurer: Yup.string().required('Campo obligatorio'),
    logisticOperator: Yup.string().required('Campo obligatorio'),
    lastClaimDate: Yup.string(),
    otherOperators:  Yup.string(),
    deliveryPoint: Yup.string(),
    attendantIPS: Yup.string().required('Campo obligatorio'),
    acquisitionWays: Yup.string(),
    nextCallDate: Yup.string().required('Campo obligatorio'),
    medic:  Yup.string(),
    specialism:  Yup.string(),
    representant: Yup.string(),
    atentionZone: Yup.string(),
    code: Yup.string(),
    baseCity: Yup.string(),
    isPatientPartOfPap: Yup.string(),
    addInfoApps: Yup.string(),
    shippingType: Yup.string()

});
