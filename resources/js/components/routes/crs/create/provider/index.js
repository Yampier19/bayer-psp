import {useState} from 'react';


import CreateBtn from '../../../../base/buttons/create-btn';
import FormField from '../../../../form/formfield';
import Dropdown from '../../../../form/dropdown';
import Radio from '../../../../form/dropdown';
import FormField2 from '../../../../form/formfield2';
import FormField3 from '../../../../form/formfield3';

import ProgressIcon from '../../../../base/progress-icon';
import ProgressBar from '../../../../base/progress-bar';

import {Link} from 'react-router-dom';

import {tabClicked} from '../../../../commons/tabs/tabClicked';
import {areAllFieldFilled} from '../../../../../utils/forms';



const countProgress = (form, fields) => {

    let count = 0;

    for(let i = 0; i < fields; i++)
        if(form[`check_field${i+1}`].checked)
            count++;

    return count / fields * 100;
}


const CreatePatient = props => {

    const [submitDisabled, setSubmitDisabled] = useState(true);
    const [progress, setProgress] = useState(0);

    const handleSubmit = e => {
        e.preventDefault();
    }

    const onChange = e => {



        const form = document.getElementById('form');

        //get the name of current field
        const name = e.target.name;

        //look for the linked check
        const check = form[`check_${name}`];

        check.checked = e.target.value != '' ? true : false;

        //submit btn
        const allFilled = areAllFieldFilled(form, 15);

        setSubmitDisabled( allFilled ? false : true );

        //progress
        const _progress = countProgress(form, 15);
        setProgress( _progress );
    }


    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>


            {/* header */}
            <section className="section px-0 pt-0 pb-3 ">
                <div className="columns is-mobile">
                    <div className="column is-3-desktop ">
                        <h1 className="subtitle has-text-horange is-3 mb-1"  style={{ whiteSpace: 'nowrap'}}>
                            <strong className="has-text-horange">CREAR</strong>
                            <span className="is-hidden-touch"> - PROVEEDOR</span >
                        </h1>
                        <h1 className="subtitle has-text-horange is-hidden-desktop">Paciente nuevo</h1>
                    </div>

                </div>
            </section>

            <hr className="has-background-dark"/>

            {/* tabs */}
            <section className="section p-0 m-0 px-1">
                <div className="columns is-vcentered">

                    <div className="column">
                        <div className="tabs is-boxed createTab3">
                            <ul>
                                <li className="formTab is-active" onClick={tabClicked} data-tab="tab1">
                                    <a>
                                        <ProgressIcon className="has-background-horange has-text-white" value={progress} icon={<i class="fas fa-user"></i>}/>&nbsp;
                                        <span className="is-hidden-touch">General</span>
                                    </a>
                                </li>
                                <li className="formTab" onClick={tabClicked} data-tab="tab2">
                                    <a>
                                        <ProgressIcon className="has-background-horange has-text-white" value={progress} icon={<i class="fal fa-paperclip"></i>}></ProgressIcon> &nbsp;
                                        <span className="is-hidden-mobile">Archivos adjuntos</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div className="column is-3">

                        <ProgressBar className="has-background-horange" value={progress}></ProgressBar>
                    </div>
                </div>
            </section>

            {/* forms */}
            <section className="section px-0 is-flex-grow-1 py-4" style={{height: '1px', overflow: 'hidden', minHeight: '500px'}}>
                <div id="tab1" className="tab is-active">
                    <form id="form" onSubmit={handleSubmit} className="coolscroll horange pr-3 py-3">
                        <FormField label="Canal" number="1" name="field1">
                            <input className="input cool-input" type="text" name="field1"  onChange={onChange}/>
                        </FormField>

                        <FormField2
                            number1="2" number2="2.1" name1="field2" name2="field2.1"
                            label1="Año*"
                            input1={<Dropdown options={["op1", "op2"]} name="field2" onChange={onChange} arrowColor="has-text-horange" required/>}
                            label2="Mes* "
                            input2={<Dropdown options={["op1", "op2"]} name="field2.1" onChange={onChange} arrowColor="has-text-horange" required/>}
                        />

                        <FormField label="Producto*" number="3" name="field3">
                            <input className="input cool-input" type="text" name="field3" onChange={onChange} required/>
                        </FormField>

                        <FormField label="Diagnostico" number="4" name="field4">
                            <input className="input cool-input" type="text" name="field3" onChange={onChange} required/>
                        </FormField>

                        <FormField label="CMP" number="5" name="field5">
                            <input className="input cool-input" type="text" name="field5" onChange={onChange} required/>
                        </FormField>

                        <FormField2
                            number1="6" number2="6.1" name1="field6" name2="field6.1"
                            label1="Medico"
                            input1={<Dropdown options={["op1", "op2"]} name="field6" onChange={onChange} arrowColor="has-text-horange" required/>}
                            label2="Especialidad"
                            input2={<Dropdown options={["op1", "op2"]} name="field6.1" onChange={onChange} arrowColor="has-text-horange" required/>}
                        />

                        <FormField2
                            number1="7" number2="7.1" name1="field7" name2="field7.1"
                            label1="COP PAC"
                            input1={<Dropdown options={["op1", "op2"]} name="field7" onChange={onChange} arrowColor="has-text-horange" required/>}
                            label2="Tipo de paciente"
                            input2={<Dropdown options={["op1", "op2"]} name="field7.1" onChange={onChange} arrowColor="has-text-horange" required/>}
                        />

                        <FormField2
                            number1="8" number2="8.1" name1="field8" name2="field8.1"
                            label1="Representante"
                            input1={<input className="input cool-input" type="text" name="field8" onChange={onChange} required/>}
                            label2="Aseguradora"
                            input2={<input className="input cool-input" type="text" name="field8.1" onChange={onChange} required/>}
                        />


                        <FormField2
                            number1="9" number2="9.1" name1="field9" name2="field9.1"
                            label1="Institución"
                            input1={<Dropdown options={["op1", "op2"]} name="field9" onChange={onChange} arrowColor="has-text-horange" required/>}
                            label2="Unidades"
                            input2={<input className="input cool-input" type="text" name="field9.1" onChange={onChange} required/>}
                        />


                        <FormField label="Clasificación" number="10" name="field10" noline>
                            <input className="input cool-input" type="text" name="field10" onChange={onChange}/>
                        </FormField>



                        <br/>

                        <div className="field has-text-right has-text-centered-mobile">
                            <div className="control">
                                <button
                                    className="button is-horange"
                                    type="submit"
                                    style={{width: '150px'}}
                                    disabled={submitDisabled}
                                >
                                    SIGUIENTE
                                </button>
                            </div>
                        </div>

                    </form>
                </div>

                <div id="tab2" className="tab">
                    <form id="form2" onSubmit={handleSubmit} className="coolscroll horange pr-3 py-3">
                        <FormField3 label="Carta de solicitud de reposición" number="1" name="field1" col2={<div><u>Cargar una nueva carta</u></div>}>
                            <input className="input cool-input" type="text" name="field1"  onChange={onChange}/>
                        </FormField3>

                        <FormField3 label="Formato evento adverso" number="2" name="field2" noline col2={<ProgressBar className="has-background-hblack" value={50}></ProgressBar>}>
                            <input className="input cool-input" type="text" name="field2"  onChange={onChange}/>
                        </FormField3>



                        <div className="field has-text-right has-text-centered-mobile">
                            <div className="control">
                                <button
                                    className="button is-horange"
                                    type="submit"
                                    style={{width: '150px'}}
                                >
                                    Enviar
                                </button>
                            </div>
                        </div>

                    </form>
                </div>

            </section>
        </div>
    );
}

export default CreatePatient;
