import {useState} from 'react';


import CreateBtn from '../../../../base/buttons/create-btn';
import FormField from '../../../../form/formfield';
import Dropdown from '../../../../form/dropdown';
import Radio from '../../../../form/dropdown';
import FormField2 from '../../../../form/formfield2';
import FormField3 from '../../../../form/formfield3';

import ProgressIcon from '../../../../base/progress-icon';
import ProgressBar from '../../../../base/progress-bar';

import {Link} from 'react-router-dom';

import {tabClicked} from '../../../../commons/tabs/tabClicked';
import {areAllFieldFilled} from '../../../../../utils/forms';



const countProgress = (form, fields) => {

    let count = 0;

    for(let i = 0; i < fields; i++)
        if(form[`check_field${i+1}`].checked)
            count++;

    return count / fields * 100;
}


const CreatePatient = props => {

    const [submitDisabled, setSubmitDisabled] = useState(true);
    const [progress, setProgress] = useState(0);

    const handleSubmit = e => {
        e.preventDefault();
    }

    const onChange = e => {



        const form = document.getElementById('form');

        //get the name of current field
        const name = e.target.name;

        //look for the linked check
        const check = form[`check_${name}`];

        check.checked = e.target.value != '' ? true : false;

        //submit btn
        const allFilled = areAllFieldFilled(form, 15);

        setSubmitDisabled( allFilled ? false : true );

        //progress
        const _progress = countProgress(form, 15);
        setProgress( _progress );
    }


    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>


            {/* header */}
            <section className="section px-0 pt-0 pb-3 ">
                <div className="columns is-mobile">
                    <div className="column is-3-desktop ">
                        <h1 className="subtitle has-text-horange is-3 mb-1"  style={{ whiteSpace: 'nowrap'}}>
                            <strong className="has-text-horange">CREAR</strong>
                            <span className="is-hidden-touch"> - MÉDICO</span >
                        </h1>
                        <h1 className="subtitle has-text-horange is-hidden-desktop">Paciente nuevo</h1>
                    </div>

                </div>
            </section>

            <hr className="has-background-dark"/>

            {/* tabs */}
            <section className="section p-0 m-0 px-1">
                <div className="columns is-vcentered">

                    <div className="column">
                        <div className="tabs is-boxed createTab3">
                            <ul>
                                <li className="formTab is-active" onClick={tabClicked} data-tab="tab1">
                                    <a>
                                        <ProgressIcon className="has-background-horange has-text-white" value={progress} icon={<i class="fas fa-user"></i>}/>&nbsp;
                                        <span className="is-hidden-touch">General</span>
                                    </a>
                                </li>
                                <li className="formTab" onClick={tabClicked} data-tab="tab2">
                                    <a>
                                        <ProgressIcon className="has-background-horange has-text-white" value={progress} icon={<i class="fal fa-paperclip"></i>}></ProgressIcon> &nbsp;
                                        <span className="is-hidden-mobile">Archivos adjuntos</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div className="column is-3">

                        <ProgressBar className="has-background-horange" value={progress}></ProgressBar>
                    </div>
                </div>
            </section>

            {/* forms */}
            <section className="section px-0 is-flex-grow-1 py-4" style={{height: '1px', overflow: 'hidden', minHeight: '500px'}}>
                <div id="tab1" className="tab is-active">
                    <form id="form" onSubmit={handleSubmit} className="coolscroll horange pr-3 py-3">
                        <FormField label="Código de usuario" number="1" name="field1">
                            <input className="input cool-input" type="text" name="field1"  onChange={onChange}/>
                        </FormField>

                        <FormField label="Estado paciente*" number="2" name="field2">
                            <Dropdown options={["op1", "op2"]} name="field2" onChange={onChange} arrowColor="has-text-primary" required/>
                        </FormField>

                        <FormField label="Fecha de activación*" number="3" name="field3">
                            <input className="input cool-input" type="date" name="field3" onChange={onChange} required/>
                        </FormField>

                        <FormField2
                            number1="4" number2="4.1" name1="field4" name2="field4.1"
                            label1="Nombres*"
                            input1={<input className="input cool-input" type="text" name="field4" onChange={onChange} required/>}
                            label2="Apellidos* "
                            input2={<input className="input cool-input" type="text" name="field4.1" onChange={onChange} required/>}
                        />

                        <FormField label="Identificación*" number="5" name="field5">
                            <input className="input cool-input" type="text" name="field5" onChange={onChange} required/>
                        </FormField>

                        <FormField label="Telefono*" number="6" name="field6">
                            <input className="input cool-input" type="text" name="field6" onChange={onChange} required/>
                            <p class="help has-text-right is-size-6"><a><u>Agregar telefono</u></a></p>
                        </FormField>

                        <FormField2
                            number1="7" number2="7.1" name1="field7" name2="field7.1"
                            label1="Departamento*"
                            input1={<Dropdown options={["op1", "op2"]} name="field7" onChange={onChange} arrowColor="has-text-primary" required/>}
                            label2="Ciudad* "
                            input2={<Dropdown options={["op1", "op2"]} name="field7.1" onChange={onChange} arrowColor="has-text-primary" required/>}
                        />

                        <FormField label="Borrar" number="8" name="field8">
                            <input className="input cool-input" type="text" name="field8" onChange={onChange}></input>
                        </FormField>

                        <FormField label="Departamento" number="9" name="field9">
                            <input className="input cool-input" type="text" name="field9" onChange={onChange}/>
                        </FormField>

                        <FormField label="Barrio" number="10" name="field10">
                            <input className="input cool-input" type="text" name="field10" onChange={onChange}/>
                        </FormField>

                        <FormField label="Via" number="11" name="field11">
                            <input className="input cool-input" type="text" name="field11" onChange={onChange}/>
                        </FormField>

                        <FormField label="Interior" number="12" name="field12">
                            <input className="input cool-input" type="text" name="field12" onChange={onChange}/>
                        </FormField>

                        <FormField label="Genero" number="13" name="field13">
                            <input className="input cool-input" type="text" name="field13" onChange={onChange}/>
                        </FormField>

                        <FormField label="Fecha de nacimiento" number="14" name="field14">
                            <input className="input cool-input" type="date" name="field14" onChange={onChange}/>
                        </FormField>

                        <FormField label="Acudiente" number="15" name="field15" noline>
                            <input className="input cool-input" type="date" name="field15" onChange={onChange}/>
                        </FormField>

                        <br/>

                        <div className="field has-text-right has-text-centered-mobile">
                            <div className="control">
                                <button
                                    className="button is-horange"
                                    type="submit"
                                    style={{width: '150px'}}
                                    disabled={submitDisabled}
                                >
                                    SIGUIENTE
                                </button>
                            </div>
                        </div>

                    </form>
                </div>

                <div id="tab2" className="tab">
                    <form id="form2" onSubmit={handleSubmit} className="coolscroll horange pr-3 py-3">
                        <FormField3 label="Carta de solicitud de reposición" number="1" name="field1" col2={<div><u>Cargar una nueva carta</u></div>}>
                            <input className="input cool-input" type="text" name="field1"  onChange={onChange}/>
                        </FormField3>

                        <FormField3 label="Formato evento adverso" number="2" name="field2" col2={<ProgressBar className="has-background-hblack" value={50}></ProgressBar>}>
                            <input className="input cool-input" type="text" name="field2"  onChange={onChange}/>
                        </FormField3>



                        <FormField3 label="Sticker del producto" number="3" name="field3" col2={<div><u>retroalimentación</u></div>}>
                            <input className="input cool-input" type="text" name="field3"  onChange={onChange}/>
                        </FormField3>



                        <FormField3 label="Carta de entrega del producto" number="4" name="field4" noline col2={<div><u>retroalimentación</u></div>}>
                            <input className="input cool-input" type="text" name="field4"  onChange={onChange}/>
                        </FormField3>




                        <div className="field has-text-right has-text-centered-mobile">
                            <div className="control">
                                <button
                                    className="button is-horange"
                                    type="submit"
                                    style={{width: '150px'}}
                                >
                                    Enviar
                                </button>
                            </div>
                        </div>

                    </form>
                </div>

            </section>
        </div>
    );
}

export default CreatePatient;
