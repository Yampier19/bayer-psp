

export const patientFilters = [
    {
        name: 'Listado pacientes',
        visibility: <div>Privada <span className="icon"><i class="fas fa-user"></i></span></div>,
        id: 0
    },
    {
        name: 'Paciente terapia',
        visibility: <div>usuarios concretos  <span className="icon"><i class="fas fa-users"></i></span></div>,
        id: 1
    },
    {
        name: 'Informe educación',
        visibility: <div>Pública <span className="icon"><i class="fas fa-globe-americas"></i></span></div>,
        id: 2
    },
    {
        name: 'Gestión del dia',
        visibility: <div>Pública <span className="icon"><i class="fas fa-globe-americas"></i></span></div>,
        id: 3
    }
];

export const claimFilters = [
    {
        name: 'Informe reclamación',
        visibility: <div>Pública <span className="icon"><i class="fas fa-globe-americas"></i></span></div>,
        id: 0
    },
    {
        name: 'Informe reclamación historico',
        visibility: <div>Pública <span className="icon"><i class="fas fa-globe-americas"></i></span></div>,
        id: 1
    }
];

export const conciliationFilters = [
    {
        name: 'Conciliación out',
        visibility: <div>Pública <span className="icon"><i class="fas fa-globe-americas"></i></span></div>,
        id: 0
    },
    {
        name: 'Conciliación out',
        visibility: <div>Pública <span className="icon"><i class="fas fa-globe-americas"></i></span></div>,
        id: 1
    },
    {
        name: 'Reconciliación',
        visibility: <div>Pública <span className="icon"><i class="fas fa-globe-americas"></i></span></div>,
        id: 2
    }
];

export const therapyFilters = [
    {
        name: 'Apoyo Adempas',
        visibility: <div>Pública <span className="icon"><i class="fas fa-globe-americas"></i></span></div>,
        id: 0
    },
    {
        name: 'Aplicaciones Eyilia',
        visibility: <div>Pública <span className="icon"><i class="fas fa-globe-americas"></i></span></div>,
        id: 1
    }
];

export const wallsFilters = [
    {
        name: 'Reporte contador causal',
        visibility: <div>Pública <span className="icon"><i class="fas fa-globe-americas"></i></span></div>,
        id: 0
    }
];

export const medicFilters = [
    {
        name: 'Conciliación out',
        visibility: <div>Pública <span className="icon"><i class="fas fa-globe-americas"></i></span></div>,
        id: 0
    },
    {
        name: 'Conciliación out',
        visibility: <div>Pública <span className="icon"><i class="fas fa-globe-americas"></i></span></div>,
        id: 1
    },
    {
        name: 'Reconciliación',
        visibility: <div>Pública <span className="icon"><i class="fas fa-globe-americas"></i></span></div>,
        id: 2
    }
];
