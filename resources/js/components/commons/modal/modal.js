const Modal = props => {

    const onCloseClicked = e => {

        const modal = document.getElementById('modal');
        modal.remove();


    }


    return(
        <div id="modal" className="modal is-active">
            <div className="modal-background"></div>
            <div className="modal-content">

                <div className="box py-6">
                    <div className="columns is-vcentered">
                        <div className="column is-4 is-flex  is-justify-content-center">
                            <figure class="image is-128x128" >
                                <img src="https://bulma.io/images/placeholders/128x128.png" />
                            </figure>
                        </div>

                        <div className="column">
                            <h1 className="subtitle is-4">Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</h1>
                        </div>
                    </div>
                </div>

            </div>
            <button class="modal-close is-large" aria-label="close" onClick={onCloseClicked}></button>

        </div>
    );
}

export default Modal;
