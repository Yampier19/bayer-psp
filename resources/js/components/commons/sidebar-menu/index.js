import {Link} from 'react-router-dom';



const SideBarMenu = props => {

    const {color} = props;


    return(
        <div className="has-background-white py-6">

            <div className="level my-3 mx-0">
                <div className="level-left pl-5">
                    <div className="level-item">
                        <Link to="/temp" className="button is-white is-size-5 has-text-left is-rounded" style={{width: '200px'}}>
                            <span className="icon"><i className="fas fa-home"></i></span> &nbsp;&nbsp;&nbsp;
                            Inicio
                        </Link>
                    </div>
                </div>
            </div>

            <div className="level my-3 mx-0">
                <div className="level-left pl-5">
                    <div className="level-item">
                        <Link to="/temp" className="button is-white is-size-5 has-text-left is-rounded" style={{width: '200px'}}>
                            <span className="icon"><i className="fas fa-plus-circle"></i></span> &nbsp;&nbsp;&nbsp;
                            Novedades
                        </Link>
                    </div>
                </div>
            </div>

            <div className="level my-3 mx-0">
                <div className="level-left pl-5">
                    <div className="level-item">
                        <Link to="/temp" className="button is-white is-size-5 has-text-left is-rounded" style={{width: '200px'}}>
                            <span className="icon"><i className="far fa-comment"></i></span> &nbsp;&nbsp;&nbsp;
                            Seguimiento
                        </Link>
                    </div>
                </div>
            </div>

            <div className="level my-3 mx-0">
                <div className="level-left pl-5">
                    <div className="level-item">
                        <Link to="/temp" className="button is-white is-size-5 has-text-left is-rounded" style={{width: '200px'}}>
                            <span className="icon"><i className="fas fa-box"></i></span> &nbsp;&nbsp;&nbsp;
                            Productos
                        </Link>
                    </div>
                </div>
            </div>

            <div className="level my-3 mx-0">
                <div className="level-left pl-5">
                    <div className="level-item">
                        <Link to="/temp" className="button is-white is-size-5 has-text-left is-rounded" style={{width: '200px'}}>
                            <span className="icon"><i className="fas fa-paste"></i></span> &nbsp;&nbsp;&nbsp;
                            Reportes
                        </Link>
                    </div>
                </div>
            </div>

            <div className="level my-3 mx-0">
                <div className="level-left pl-5">
                    <div className="level-item">
                        <Link to="/temp" className="button is-white is-size-5 has-text-left is-rounded" style={{width: '200px'}}>
                        <span className="icon"><i className="fas fa-cog"></i></span>  &nbsp;&nbsp;&nbsp;
                            Configuración
                        </Link>
                    </div>
                </div>
            </div>

            <br/>

            <div className="level my-3 mx-0">
                <div className="level-left pl-5">
                    <div className="level-item">
                        <a className="button is-white level-item has-text-red is-size-5">
                            <span className=""><i className="gg-log-off"></i></span> &nbsp;
                            Cerrar Sesión
                        </a>
                    </div>
                </div>
            </div>

        </div>
    );
}

export default SideBarMenu;
