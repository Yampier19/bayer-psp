import {Link} from 'react-router-dom';
const DotsBtn = props => {
    return(
        <div class="dropdown is-hoverable">
            <div class="dropdown-trigger">
                <button class="button" aria-haspopup="true" aria-controls="dropdown-menu" style={{background: 'rgba(0,0,0,0)', border: 'none'}}>

                    <span class="icon is-small">
                        <i class="fas fa-ellipsis-v has-text-primary"></i>
                    </span>
                </button>
            </div>
            <div class="dropdown-menu" id="dropdown-menu" role="menu">
                <div class="dropdown-content">
                    <Link to="/home" href="#" class="dropdown-item has-text-left">
                        Programa de Seguimiento a pacientes
                    </Link>
                    <Link to="/crs/home" class="dropdown-item has-text-left">
                        Programa de compensacion y reembolso
                    </Link>
                    <Link to="/support/home" href="#" class="dropdown-item has-text-left">
                        Apoyo Diagnostico
                    </Link>
                </div>
            </div>
        </div>
    );
}

export default DotsBtn;
