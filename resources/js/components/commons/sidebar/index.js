import {useState, useEffect, useRef} from 'react';


import ProfileWide from './profile-wide';
import ProfileNarrow from './profile-narrow';
import SidebarBtn from './sidebar-btn';
import SubButton from './sub-btn';


import {open_sidebar, close_sidebar} from '../../../redux/actions/sidebarActions';
import {connect} from 'react-redux';


import './sidebar.scss';

const Sidebar = props => {

    // *~~**~~**~~**~~**~~**~~**~~**~~* type
    const type = props.type || 1;



    // *~~**~~**~~**~~**~~**~~**~~**~~* sidebar state
    const {sidebarData} = props;

    const collapseClicked = e => {

        if(sidebarData.isOpen)
            props.close_sidebar();
        else props.open_sidebar();
    }

    // *~~**~~**~~**~~**~~**~~**~~**~~* fields
    //fields
    const [name, setName] = useState('Nombre del admin');
    const [role, setRole] = useState('Super admin');


    return(
        <nav id="sidebar" className={`menu has-background-light is-flex is-flex-direction-column ${props.sidebarData.isOpen ? 'open' : ''}`} >

            {/* collapse btn */}
            <div className="collapse-button">
                <button className="button is-primary is-rounded" onClick={collapseClicked}>
                    <span className="icon"><i className="fas fa-bars"></i></span>
                </button>

                </div>

            <br/>
            <div className="sidebar-body mb-3">

                {
                    sidebarData.isOpen ?
                    <ProfileWide name={name} role={role}/>
                    :
                    <ProfileNarrow name={name}  />


                }

            </div>

            <br/>

            <div className="sidebar-footer">


                    {
                        type == 1 ?
                        <section className={`section pb-3 pt-4 has-background-white + ${sidebarData.isOpen ? 'px-2' : 'px-0'} `}>

                            <SidebarBtn isActive={props.activeSite == '1'} activeClass="" activeTextClass=""
                                to="/home"
                                name={sidebarData.isOpen ? "Inicio" : null}><i className="fas fa-home"></i></SidebarBtn>

                            <SidebarBtn isActive={props.activeSite == '2'} activeClass="is-hgreen-light" activeTextClass="has-text-hgreen"
                                to="/news/consult"
                                name={sidebarData.isOpen ? "Novedades" : null}><i className="fas fa-plus-circle"></i></SidebarBtn>

                            <SidebarBtn isActive={props.activeSite == '3'} activeClass="is-hblue-light" activeTextClass="has-text-hblue"
                                to="/tracking"
                                name={sidebarData.isOpen ? "Seguimiento" : null}><i className="fas fa-comment"></i></SidebarBtn>

                            <SidebarBtn isActive={props.activeSite == '4'} activeClass="is-horange-light" activeTextClass="has-text-horange"
                                to="/products"
                                name={sidebarData.isOpen ? "Productos" : null}><i className="fas fa-box"></i></SidebarBtn>

                            <SidebarBtn isActive={props.activeSite == '5'} activeClass="is-hred-light" activeTextClass="has-text-hred"
                                to="/reports"
                                name={sidebarData.isOpen ? "Reportes" : null}><i className="fas fa-paste"></i></SidebarBtn>

                                {
                                    props.activeSite == '5' ?
                                        <div>
                                            <SubButton isActive={props.activeSite == '5'} activeTextClass="has-text-hred"
                                                to="/reports/create"
                                                name={sidebarData.isOpen ? "Filtros" : null}><i className="fas fa-paste"></i></SubButton>
                                            <div className="pb-1"/>
                                            <SubButton isActive={props.activeSite == '5'} activeTextClass="has-text-hred"
                                                to="/reports/graphics"
                                                name={sidebarData.isOpen ? "Gráficas" : null}><i className="fas fa-paste"></i></SubButton>
                                        </div>

                                        :
                                        null

                                }





                            <SidebarBtn isActive={props.activeSite == '6'} activeClass="is-light" activeTextClass="has-text-hblack"
                                to="/Configuration/fundem"
                                name={sidebarData.isOpen ? "Configuración" : null}><i className="fas fa-cog"></i></SidebarBtn>

                                {
                                    props.activeSite == '6' ?
                                        <div style={{maxHeight: '100px', overflow: 'auto'}}>
                                            <SubButton isActive={props.activeSite == '5'} activeTextClass="has-text-hblack"
                                                to="/configuration/fundem"
                                                name={sidebarData.isOpen ? "Gestion FUNDEM" : null}><i className="fas fa-paste"></i></SubButton>

                                            <SubButton isActive={props.activeSite == '5'} activeTextClass="has-text-hblack"
                                                to="/configuration/date"
                                                name={sidebarData.isOpen ? "Cambio de fecha" : null}><i className="fas fa-paste"></i></SubButton>

                                            <SubButton isActive={props.activeSite == '5'} activeTextClass="has-text-hblack"
                                                to="/configuration/users"
                                                name={sidebarData.isOpen ? "usuarios" : null}><i className="fas fa-paste"></i></SubButton>

                                            <SubButton isActive={props.activeSite == '5'} activeTextClass="has-text-hblack"
                                                to="/configuration/create"
                                                name={sidebarData.isOpen ? "Creación de rol" : null}><i className="fas fa-paste"></i></SubButton>

                                            <SubButton isActive={props.activeSite == '5'} activeTextClass="has-text-hblack"
                                                to="/configuration/templates"
                                                name={sidebarData.isOpen ? "Historial plantillas" : null}><i className="fas fa-paste"></i></SubButton>

                                            <SubButton isActive={props.activeSite == '5'} activeTextClass="has-text-hblack"
                                                to="/configuration/users"
                                                name={sidebarData.isOpen ? "Historial asignación usuarios" : null}><i className="fas fa-paste"></i></SubButton>

                                            <SubButton isActive={props.activeSite == '5'} activeTextClass="has-text-hblack"
                                                to="/configuration/users"
                                                name={sidebarData.isOpen ? "Correo electrónico" : null}><i className="fas fa-paste"></i></SubButton>
                                        </div>

                                        :
                                        null

                                }

                            <br/>



                            <SidebarBtn className="has-text-danger" name={sidebarData.isOpen ? "Cerrar Sesión" : null}><i className="gg-log-off"></i></SidebarBtn>
                        </section>
                        :
                        <section className={`section pb-3 pt-4 pb-5 ${sidebarData.isOpen ? 'px-2' : 'px-0'} `}>


                            <br/>
                            <SidebarBtn isActive activeClass="has-background-light" name={sidebarData.isOpen ? "Configuración" : null}><i className="fas fa-cog"></i></SidebarBtn>
                            <SidebarBtn isActive activeClass="has-text-danger has-background-light" name={sidebarData.isOpen ? "Cerrar Sesión" : null}><i className="gg-log-off"></i></SidebarBtn>

                            <br/>

                            <h1 className={`subtitle is-6 px-4 has-text-light2 ${sidebarData.isOpen ? '' : 'is-hidden'}`} >By People Marketing</h1>
                        </section>
                    }



            </div>

        </nav>
    );
}

const mapStateToProps = state => ({
    sidebarData: state.sidebarReducer
});

export default connect(
    mapStateToProps,
    {
        close_sidebar,
        open_sidebar
    })
(Sidebar);
