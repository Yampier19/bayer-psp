import './sub-btn.scss';

import {Link} from 'react-router-dom';

const SubButton = props => {
    return(
        <Link
            className={`button has-text-left is-fullwidth sub-btn is-rounded ${props.isActive ? props.activeTextClass : 'is-white'}`}
            to={props.to}
            >
            <span className="icon is-size-7 has-text-centered" style={{zIndex: '1'}}>
                <i class="fas fa-circle"></i>
            </span>
            {
                props.name != null ?
                <span>&nbsp;{props.name}</span>
                :null
            }

        </Link>
    );
}

export default SubButton;
