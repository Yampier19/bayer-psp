const FormField3 = props => {

    return(
        <div className="field3 mb-6">
            <div className="columns " style={{minHeight: '100px'}}>



                <div className="column">
                    <div className="columns  is-mobile">

                        <div className="column is-1" style={{width: '50px'}}>
                            <div className="control">
                                <input className="def_checkbox" type="checkbox" name={`check_${props.name}`} checked={props.checked} readOnly/>
                                <div className={`newcheck xl ${props.noline ? 'no-line' : ''}`}>
                                    <strong className="num">{props.number}</strong>
                                </div>
                            </div>
                        </div>

                        <div className="column">
                            <div className="columns ">
                                <div className="column is-4">
                                     <label className="label">{props.label}</label>
                                </div>
                                <div className="column">
                                    <div className="control">
                                        {props.children}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="column mt-2 is-2">
                    <div className="columns is-vcentered-desktop is-mobile">
                        <div className="column is-1 is-hidden-tablet" style={{width: '50px'}}>

                        </div>

                        <div className="column">
                            <div className="has-text-right-desktop">
                                {props.col2}
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    );
}

export default FormField3;
