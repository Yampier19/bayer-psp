const Radio = props => {
    return(
        <div class="control">
            <label class="radio">
                <input type="radio" name={props.name} onChange={props.onChange} value={props.option1 || 'Si'}/>&nbsp;
                {props.option1 || 'Si'}
            </label> &nbsp;&nbsp;&nbsp;
            <label class="radio">
                <input type="radio" name={props.name} onChange={props.onChange} value={props.option2 || 'No'}/>&nbsp;
                {props.option2 || 'No'}
             </label>
        </div>
    );
}

export default Radio;
