import './dropdown.scss';

const Dropdown = props => {

    return(
        <div>
            <span className={`icon down-arrow ${props.arrowColor}`}>
                <i class="fas fa-chevron-down"></i>
            </span>
            <select id={props.id} className="input cool-input dropdown-base" name={props.name} onChange={props.onChange}>
                <option disabled selected className="">  </option>
                {
                    props.options.map( (op,i) =>
                        <option key={i} value={op}>
                            {op}
                        </option>

                    )
                }

            </select>
        </div>
    );
}

export default Dropdown;
