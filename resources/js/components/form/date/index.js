import {baseURL, calendar} from '../../../images';

import './date.scss';

const Date = props => {

    const onClicked = e => {
        const target = e.currentTarget;
        target.classList.remove('placeholderclass');
    }

    return(
        <input
            type="date"
            placeholder={props.placeholder}
            onClick={onClicked}
            className="input cool-input dateclass placeholderclass"
            name={props.name}
            onChange={props.onChange}
            data-iconURL={baseURL + calendar}
            data-date-format='dd-mm-yy'
            />
    );
}

export default Date;
