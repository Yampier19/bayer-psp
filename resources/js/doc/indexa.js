const Documentation = props => {
    return(
        <div className="block m-6">
            <div className="container">
                <div className="content">

                    <h1>Documentación</h1>
                    <hr/>
                    <section className="section">
                        <h3 className="subtitle">Tecnologia</h3>

                        <p>
                            Esta app esta siendo desarrollada con ReactJS, bulma.io y sass, adicionalmente se utilizan las siguientes librerias
                        </p>

                        <ul>
                            <li><strong>react-router-dom:</strong> permite dividir la app en rutas.</li>
                            <li><strong>react-redux:</strong> manejar el estado de la app.</li>
                            <li><strong>node-sass:</strong> permite utilizar sass como procesador de hojas de estilo.</li>
                        </ul>
                    </section>
                    <br/>
                    <section  className="section">
                        <h3>Estructura</h3>

                        <p>
                            la estructura de los folders es la siguiente

                            <br/>

                            src/ <br/>
                            ├─ componentes/ <br/>
                            │  ├─ base/ <br/>
                            │  ├─ cards/ <br/>
                            │  ├─ commons/ <br/>
                            │  ├─ form/ <br/>
                            │  ├─ routes/ <br/>
                            │ <br/>
                            ├─ utils/ <br/>

                            <br/>
                            todos los componentes se almacenan dentro de la carpeta componentes


                        </p>

                        <hr/>

                        <ol>
                            <li className="mb-6">
                                <h4>Base</h4>
                                <p>
                                    Contiene componentes elementales que son especificos al diseño de esta app

                                    <ol>
                                        <li><strong> progress icon: </strong> este componente renderiza un icono dentro de un circulo, y renderiza un progreso al rededor de este que va desde 0 a 100</li>

                                    </ol>

                                </p>
                            </li>
                            <li className="mb-6">
                                <h4>Cards</h4>
                                <p>
                                    Contiene todos los componente de tipo card

                                </p>
                            </li>

                            <li className="mb-6">
                                <h4>Commons</h4>
                                <p>
                                    Contiene componentes que son comunes a todas las rutas, o que se utilizan
                                    demasiadas veces en la app (pero no son especificos al diseño de la app)
                                    <ol>
                                        <li>sidebar</li>
                                        <li>modal</li>
                                        <li>tabs</li>
                                    </ol>
                                </p>
                            </li>

                            <li className="mb-6">
                                <h4>Form</h4>
                                <p>
                                    Contiene componentes que ayudan a realizar formularios (que contienen el diseño de la app) de forma mas rapida
                                </p>
                            </li>

                            <li className="mb-6">
                                <h4>Routes</h4>
                                <p>
                                    Contiene las rutas de la app, no se pueden considerar como un componente sino mas bien como un wrapper que
                                    donde se ensamblan todos los componentes

                                    <br/>

                                    app/ <br/>
                                    ├─ home/ <br/>
                                    │ <br/>
                                    ├─ create/ <br/>
                                    │  ├─ new/ <br/>
                                    │  ├─ patient/ <br/>
                                    │  ├─ eps-opl/ <br/>
                                    │ <br/>
                                    ├─ create/ <br/>


                                </p>
                            </li>
                        </ol>



                    </section>


                </div>
            </div>
        </div>
    );
}

export default Documentation;
