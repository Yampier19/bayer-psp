import * as boostrap from "bootstrap/dist/js/bootstrap.bundle.js";

const LeftMenu = props => {

    const onClicked = e => {

        // console.log(boostrap);

       const sidebar = document.getElementById('sidebar');
       const bsSidebar =  new boostrap.Offcanvas(sidebar);
       bsSidebar.toggle();
   }

   return(
       <div className="app">
           <div className="offcanvas offcanvas-start show bg-dark" id="sidebar">

               <div className="offcanvas-header text-black">
                   <h4 className="offcanvas-title" id="offcanvasLabel">
                       Documentation
                   </h4>
                   <button type="button " className="btn-close text-reset btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
               </div>
               <hr className="bg-white"/>
               <div className="offcanvas-body text-white">
                   <ul class="list-unstyled">
                       <li class="mb-1">
                            <button className="btn d-inline-flex align-items-centered rounded collapsed text-white" data-bs-toggle="collapse" data-bs-target="#getting-started-collapse" aria-expanded="false">Getting Started</button>

                            <div className="collapse" id="getting-started-collapse">
                                <ul className="list-unstyled fw-normal pb-1 small">
                                    <li><a className="d-inline-flex align-items-center rounded">test</a></li>
                                    <li>test</li>
                                    <li>test</li>
                                </ul>
                            </div>

                       </li>
                       <li class="mb-1">
                           <a class="nav-link active" aria-current="page" href="#">Active</a>
                       </li>

                   </ul>
               </div>
           </div>

           <div className="container text-center">
               <button class="btn btn-dark" onClick={onClicked}>toggle</button>
           </div>

       </div>
   );
}

export default LeftMenu;
