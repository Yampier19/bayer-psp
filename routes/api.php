<?php

use App\Http\Controllers\CauseNoClaimController;
use App\Http\Controllers\ClaimHistoryController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\EducationReasonController;
use App\Http\Controllers\EducationThemeController;
use App\Http\Controllers\EpsController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\InsuranceController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\LogisticOperatorController;
use App\Http\Controllers\NoveltyController;
use App\Http\Controllers\NoveltyStatuController;
use App\Http\Controllers\PathologicalClassificationController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\PatientStatuController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProviderController;
use App\Http\Controllers\ReplacementCommentController;
use App\Http\Controllers\ReplacementController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TreatmentController;
use App\Http\Controllers\TreatmentPreviousController;
use App\Http\Controllers\DiagnosticSupportPsPController;
use App\Http\Controllers\SupplieIncomeController;
use App\Http\Controllers\SupplieExpenseController;
use App\Http\Controllers\SupplieProviderController;
use App\Http\Controllers\TreatmentStatuController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserMedicalController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login',                            [UserController::class, 'login']);

Route::prefix('password')->group(function () {
    Route::post('email',                        [ForgotPasswordController::class, 'forgot']);
    Route::post('code',                         [ForgotPasswordController::class, 'checkCode']);
    Route::post('reset',                        [ForgotPasswordController::class, 'reset']);
    Route::post('changed',                      [UserController::class, 'ChangePassword']);
});

 // rutas de recursos
Route::apiResources([
    'role'                  =>                  RoleController::class,
]);


    /*RUTAS PROTEGIDAS CON JWT*/
Route::group(['middleware' => 'jwt'], function () {
    Route::post('logout',                       [UserController::class, 'logout']);
    Route::post('register',                     [UserController::class, 'register']);
    Route::get('users',                         [UserController::class, 'users']);
    Route::get('auth',                          [UserController::class, 'auth']);
    Route::prefix('user')->group(function () {
        Route::post('/update/{user}',           [UserController::class, 'update']);
        Route::post('/delete/{user}',           [UserController::class, 'destroy']);
        Route::get('/show/{user}',              [UserController::class, 'show']);
    });

    // rutas de recursos
    Route::apiResources([
        'novelty'                       =>      NoveltyController::class,
        'statu/novelty'                 =>      NoveltyStatuController::class,
        'product'                       =>      ProductController::class,
        'patient'                       =>      PatientController::class,
        'statu/patient'                 =>      PatientStatuController::class,
        'claim/history'                 =>      ClaimHistoryController::class,
        'claim/cause'                   =>      CauseNoClaimController::class,
        'treatment'                     =>      TreatmentController::class,
        'statu/treatment'               =>      TreatmentStatuController::class,
        'classification/pathological'   =>      PathologicalClassificationController::class,
        'eps'                           =>      EpsController::class,
        'operator'                      =>      LogisticOperatorController::class,
        'replacement'                   =>      ReplacementController::class,
        'replacement/comment'           =>      ReplacementCommentController::class,
        'medical/user'                  =>      UserMedicalController::class,
        'education/theme'               =>      EducationThemeController::class,
        'education/reason'              =>      EducationReasonController::class,
        'doctor'                        =>      DoctorController::class,
        'provider'                      =>      ProviderController::class,
        'diagnostic/support'            =>      DiagnosticSupportPsPController::class,
        'supplie/incomes'               =>      SupplieIncomeController::class,
        'supplie/expenses'              =>      SupplieExpenseController::class,
        'supplie/providers'             =>      SupplieProviderController::class,
    ]);

    //ENDPOINT INDIVIDUALES LOS TRATAMIENTOS
    Route::prefix('diagnostic/support')->group(function () {
        Route::get('/treatment/{tratment_id}', [DiagnosticSupportPsPController::class, 'treatmentShow']);
    });

    //ENDPOINT INDIVIDUALES LOS PRODUCTOS
    Route::prefix('product')->group(function () {
        Route::get('/name/{id}', [ProductController::class, 'nameShow']);
    });

    // dosis por producto
    Route::get('doses/product/{id}', [ProductController::class, 'indexDoses']);

     //ENDPOINT INDIVIDUALES PARA LOS INGTRESOS
    Route::prefix('supplie/incomes')->group(function () {
        Route::get('referral/{referral_number}', [SupplieIncomeController::class, 'referralNumberShow']);
        Route::get('product/{product_id}', [SupplieIncomeController::class, 'productShow']);
        Route::get('medicine/without_commercial_value', [SupplieIncomeController::class, 'medicineWithoutCommercialValueShow']);
    });

    Route::prefix('supplie/expenses')->group(function () {
        Route::get('medicine/without_commercial_value', [SupplieExpenseController::class, 'medicineWithoutCommercialValueShow']);
        Route::get('orders', [SupplieExpenseController::class, 'orderShow']);
    });


    // obtener paciente por cedula
    Route::get('patient/document/{document}',   [PatientController::class, 'handleDocument']);

    // tratamiento previo
    Route::get('previou/treatment',             [TreatmentPreviousController::class, 'index']);

    // Egresos por paciente
    Route::get('supplie/expenses/patient/{patient_id}',             [SupplieExpenseController::class, 'indexPatient']);

    // Historial de reclamacion por id de tratamiento
    Route::get('claim/history/treatment/{treatment_id}',             [ClaimHistoryController::class, 'showTreatment']);

    //novedades
    Route::get('history/novelty/{id}',          [NoveltyController::class, 'noveltyHistoryShow']);
    Route::prefix('comment/novelty/')->group(function () {
        Route::get('{id}',                      [NoveltyController::class, 'noveltyCommentShow']);
        Route::post('{novelty}',                [NoveltyController::class, 'noveltyCommentStore'])->where('novelty','[0-9]+');
        Route::post('response/{novelty}',       [NoveltyController::class, 'commentResponse']);
    });

    // tratamiento
    Route::prefix('follows/treatment/')->group(function () {
        Route::post('{treatment}',              [TreatmentController::class, 'treatmentFollows']);
        Route::post('comunication/{treatment}', [TreatmentController::class, 'treatmentFollowCommunication']);
    });

    // localidades
    Route::prefix('location')->group(function () {
        Route::get('/country',                  [LocationController::class, 'indexCountry']);
        Route::get('/department',               [LocationController::class, 'indexDepartment']);
        Route::get('/department/{id}',          [LocationController::class, 'indexDepartmentAsCountry']);
        Route::get('/city',                     [LocationController::class, 'indexCity']);
        Route::get('/city/{id}',                [LocationController::class, 'indexCityAsDepartment']);
        Route::get('/city/zone/{id}',           [LocationController::class, 'indexCityAsZone']);
        Route::get('/zone/{id}',                [LocationController::class, 'indexZone']);
    });

    // EPS
    Route::get('handle/eps/{id}',               [EpsController::class, 'handleIpsInsurance']);

    // aseguradora
    Route::get('insurance/{id}',                [InsuranceController::class, 'index']);
    Route::post('insurance',                    [InsuranceController::class, 'store']);
    Route::get('operator/insurance/{id}',       [LogisticOperatorController::class, 'handleIpsInsurance']);
});
