<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\TreatmentStatu;
use Illuminate\Database\Seeder;

class TreatmentStatuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <count(TreatmentStatu::NAME); $i++) {
            TreatmentStatu::create([
                'name'          => TreatmentStatu::NAME[$i],
                'product_id'    => 2,
            ]);
        }

        TreatmentStatu::create([
            'name'          => 'Mantenimiento',
            'product_id'    => 1,
        ]);
        TreatmentStatu::create([
            'name'          => 'Titulacion',
            'product_id'    => 1,
        ]);
        TreatmentStatu::create([
            'name'          => 'Aplicacion programada',
            'product_id'    => 3,
        ]);
        TreatmentStatu::create([
            'name'          => 'Aplicacion no programada',
            'product_id'    => 3,
        ]);
    }
}
