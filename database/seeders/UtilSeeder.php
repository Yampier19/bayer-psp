<?php

namespace Database\Seeders;

use App\Models\ApplicationSystem;
use App\Models\EducationReason;
use App\Models\TreatmentPrevious;
use Illuminate\Database\Seeder;

class UtilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // tratamiento previo
        for ($i=0; $i <count(TreatmentPrevious::NAME); $i++) {
            TreatmentPrevious::create([
                'name'          => TreatmentPrevious::NAME[$i],
            ]);
        }

        // sistemas
        for ($i=0; $i <count(ApplicationSystem::NAME); $i++) {
            ApplicationSystem::create([
                'name'          => ApplicationSystem::NAME[$i],
                'description'   => ApplicationSystem::DESCRIPTION[$i],
            ]);
        }

        // motivo educacion
        for ($i=0; $i <count(EducationReason::NAME); $i++) {
            EducationReason::create([
                'name' => EducationReason::NAME[$i],
            ]);
        }
    }
}
