<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PermissionPermission;
use App\Models\ThemePermission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //creación de roles
        $roleSuperAdmin             = Role::create(['name' => 'Super Admin']);
        $coordinadorPM              = Role::create(['name' => 'Coordinador PM']);
        $coordinadorProveedores     = Role::create(['name' => 'Coordinador Otros Proveedores']);
        $educadorPeople             = Role::create(['name' => 'Educador People']);
        $educadorProveedores        = Role::create(['name' => 'Educador Otros Proveedores']);
        $analistaPeople             = Role::create(['name' => 'Analista de inf People']);
        $analistProveedores         = Role::create(['name' => 'Analista de inf Otos Proveedores']);
        $calidad                    = Role::create(['name' => 'Calidad']);
        $ASEI                       = Role::create(['name' => 'ASEI']);
        $cliente                    = Role::create(['name' => 'Cliente']);

        // creación de tema para para permisos

        $novedad        = ThemePermission::create(['name' => 'NOVEDADES']);
        $pacienteNuevo  = ThemePermission::create(['name' => 'PACIENTE NUEVO']);
        $seguimiento    = ThemePermission::create(['name' => 'SEGUIMIENTO']);
        $productos      = ThemePermission::create(['name' => 'PRODUCTOS']);
        $reportes       = ThemePermission::create(['name' => 'REPORTES']);
        $configuracion  = ThemePermission::create(['name' => 'CONFIGURACIÓN']);
        $solicitudes    = ThemePermission::create(['name' => 'SOLICTUDES ADICIONALES']);

        $arrayPivot1 = [
            'view' => 'SI',
            'edit' => 'SI',
        ];
        $arrayPivot2 = [
            'view' => 'SI',
            'edit' => 'NO',
        ];

        $arrayPivot3 = [
            'view' => 'NO',
            'edit' => 'SI',
        ];
        $arrayPivot4 = [
            'view' => 'NO',
            'edit' => 'NO',
        ];

        // creación de permisos
        Permission::create(['name' => 'todos los permisos'])->syncRoles([$roleSuperAdmin]);

        $permission = Permission::create(['name' => 'Registro de novedad', 'theme_permission' => $novedad->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot1);
        $permission->roles()->attach($calidad->id, $arrayPivot2);
        $permission->roles()->attach($cliente->id, $arrayPivot2);

        $permission->roles()->attach($educadorPeople->id, $arrayPivot4);
        $permission->roles()->attach($educadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot4);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot4);
        $permission->roles()->attach($ASEI->id, $arrayPivot4);

        $permission = Permission::create(['name' => 'Consulta de novedad', 'theme_permission' => $novedad->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot1);
        $permission->roles()->attach($calidad->id, $arrayPivot2);
        $permission->roles()->attach($cliente->id, $arrayPivot2);

        $permission->roles()->attach($educadorPeople->id, $arrayPivot4);
        $permission->roles()->attach($educadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot4);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot4);
        $permission->roles()->attach($ASEI->id, $arrayPivot4);

        $permission = Permission::create(['name' => 'Comunicación Novedad', 'theme_permission' => $novedad->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot1);
        $permission->roles()->attach($calidad->id, $arrayPivot2);
        $permission->roles()->attach($cliente->id, $arrayPivot2);

        $permission->roles()->attach($educadorPeople->id, $arrayPivot4);
        $permission->roles()->attach($educadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot4);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot4);
        $permission->roles()->attach($ASEI->id, $arrayPivot4);

        $permission = Permission::create(['name' => 'Creación', 'theme_permission' => $pacienteNuevo->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($educadorPeople->id, $arrayPivot1);

        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($educadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot4);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot4);
        $permission->roles()->attach($calidad->id, $arrayPivot4);
        $permission->roles()->attach($ASEI->id, $arrayPivot4);
        $permission->roles()->attach($cliente->id, $arrayPivot4);

        $permission = Permission::create(['name' => 'Listado paciente nuevo', 'theme_permission' => $pacienteNuevo->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($educadorPeople->id, $arrayPivot1);
        $permission->roles()->attach($educadorProveedores->id, $arrayPivot2);

        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot4);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot4);
        $permission->roles()->attach($calidad->id, $arrayPivot4);
        $permission->roles()->attach($ASEI->id, $arrayPivot4);
        $permission->roles()->attach($cliente->id, $arrayPivot4);

        $permission = Permission::create(['name' => 'Listado seguimiento', 'theme_permission' => $seguimiento->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot2);
        $permission->roles()->attach($educadorPeople->id, $arrayPivot1);
        $permission->roles()->attach($educadorProveedores->id, $arrayPivot1);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot2);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot2);
        $permission->roles()->attach($calidad->id, $arrayPivot1);
        $permission->roles()->attach($ASEI->id, $arrayPivot2);
        $permission->roles()->attach($cliente->id, $arrayPivot2);

        $permission = Permission::create(['name' => 'Buscar', 'theme_permission' => $seguimiento->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot2);
        $permission->roles()->attach($educadorPeople->id, $arrayPivot2);
        $permission->roles()->attach($educadorProveedores->id, $arrayPivot2);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot2);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot2);
        $permission->roles()->attach($calidad->id, $arrayPivot1);
        $permission->roles()->attach($ASEI->id, $arrayPivot2);
        $permission->roles()->attach($cliente->id, $arrayPivot2);


        $permission = Permission::create(['name' => 'Gestionar', 'theme_permission' => $seguimiento->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot2);
        $permission->roles()->attach($educadorPeople->id, $arrayPivot1);
        $permission->roles()->attach($educadorProveedores->id, $arrayPivot1);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot2);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot2);
        $permission->roles()->attach($calidad->id, $arrayPivot1);

        $permission->roles()->attach($ASEI->id, $arrayPivot4);
        $permission->roles()->attach($cliente->id, $arrayPivot4);

        $permission = Permission::create(['name' => 'Registro material', 'theme_permission' => $productos->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot1);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot1);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot2);

        $permission->roles()->attach($educadorPeople->id, $arrayPivot4);
        $permission->roles()->attach($educadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($calidad->id, $arrayPivot4);
        $permission->roles()->attach($ASEI->id, $arrayPivot4);
        $permission->roles()->attach($cliente->id, $arrayPivot4);


        $permission = Permission::create(['name' => 'Inventario', 'theme_permission' => $productos->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot1);
        $permission->roles()->attach($educadorPeople->id, $arrayPivot2);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot1);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot2);
        $permission->roles()->attach($calidad->id, $arrayPivot2);
        $permission->roles()->attach($cliente->id, $arrayPivot2);

        $permission->roles()->attach($educadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($ASEI->id, $arrayPivot4);

        $permission = Permission::create(['name' => 'Listado reportes', 'theme_permission' => $reportes->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot2);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot1);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot2);
        $permission->roles()->attach($calidad->id, $arrayPivot2);
        $permission->roles()->attach($cliente->id, $arrayPivot2);

        $permission->roles()->attach($educadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($ASEI->id, $arrayPivot4);
        $permission->roles()->attach($educadorPeople->id, $arrayPivot4);

        $permission = Permission::create(['name' => 'Creación EPS/OPL', 'theme_permission' => $configuracion->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot1);

        $permission->roles()->attach($educadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($ASEI->id, $arrayPivot4);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($educadorPeople->id, $arrayPivot4);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot4);
        $permission->roles()->attach($calidad->id, $arrayPivot4);
        $permission->roles()->attach($cliente->id, $arrayPivot4);

        $permission = Permission::create(['name' => 'Gestiones COORDINADOR', 'theme_permission' => $configuracion->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot1);

        $permission->roles()->attach($educadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($ASEI->id, $arrayPivot4);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($educadorPeople->id, $arrayPivot4);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot4);
        $permission->roles()->attach($calidad->id, $arrayPivot4);
        $permission->roles()->attach($cliente->id, $arrayPivot4);

        $permission = Permission::create(['name' => 'Cambio de fecha', 'theme_permission' => $configuracion->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot1);
        $permission->roles()->attach($calidad->id, $arrayPivot1);

        $permission->roles()->attach($educadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($ASEI->id, $arrayPivot4);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($educadorPeople->id, $arrayPivot4);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot4);
        $permission->roles()->attach($cliente->id, $arrayPivot4);

        $permission = Permission::create(['name' => 'Asignación', 'theme_permission' => $configuracion->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot2);
        $permission->roles()->attach($calidad->id, $arrayPivot1);

        $permission->roles()->attach($educadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($ASEI->id, $arrayPivot4);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot4);
        $permission->roles()->attach($educadorPeople->id, $arrayPivot4);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot4);
        $permission->roles()->attach($cliente->id, $arrayPivot4);

        $permission = Permission::create(['name' => 'Usuarios', 'theme_permission' => $configuracion->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot1);

        $permission->roles()->attach($educadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($ASEI->id, $arrayPivot4);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot4);
        $permission->roles()->attach($educadorPeople->id, $arrayPivot4);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot4);
        $permission->roles()->attach($calidad->id, $arrayPivot4);
        $permission->roles()->attach($cliente->id, $arrayPivot4);

        $permission = Permission::create(['name' => 'Mi cuenta', 'theme_permission' => $configuracion->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot1);
        $permission->roles()->attach($educadorPeople->id, $arrayPivot2);
        $permission->roles()->attach($educadorProveedores->id, $arrayPivot2);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot1);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot2);
        $permission->roles()->attach($calidad->id, $arrayPivot1);
        $permission->roles()->attach($ASEI->id, $arrayPivot2);
        $permission->roles()->attach($cliente->id, $arrayPivot2);

        $permission = Permission::create(['name' => 'Asignar novedades', 'theme_permission' => $solicitudes->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot1);
        $permission->roles()->attach($cliente->id, $arrayPivot1);

        $permission->roles()->attach($educadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($ASEI->id, $arrayPivot4);
        $permission->roles()->attach($educadorPeople->id, $arrayPivot4);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot4);
        $permission->roles()->attach($calidad->id, $arrayPivot4);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot4);

        $permission = Permission::create(['name' => 'Tipificador', 'theme_permission' => $solicitudes->name]);
        $permission->roles()->attach($coordinadorPM->id, $arrayPivot1);
        $permission->roles()->attach($educadorPeople->id, $arrayPivot1);
        $permission->roles()->attach($calidad->id, $arrayPivot3);

        $permission->roles()->attach($educadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($ASEI->id, $arrayPivot4);
        $permission->roles()->attach($analistaPeople->id, $arrayPivot4);
        $permission->roles()->attach($analistProveedores->id, $arrayPivot4);
        $permission->roles()->attach($coordinadorProveedores->id, $arrayPivot4);
        $permission->roles()->attach($cliente->id, $arrayPivot4);





        // creación de roles
        // $roleSuperAdmin     = Role::create(['name' => 'Super Admin']);
        // $rolePSP            = Role::create(['name' => 'Aplicativo PSP']);
        // $roleCRS            = Role::create(['name' => 'Programa de compensación y reembolso']);
        // $roleAD             = Role::create(['name' => 'Apoyo diagnóstico']);

        // // creación de permisos
        // Permission::create(['name' => 'todos los permisos'])->syncRoles([$roleSuperAdmin]);
        // Permission::create(['name' => 'crear'])->syncRoles([$rolePSP]);
        // Permission::create(['name' => 'novedades'])->syncRoles([$rolePSP]);

        // $permission = Permission::create(['name' => 'seguimiento'])->syncRoles([$rolePSP, $roleAD, $roleCRS]);
        // // creación de los accesos que tiene el permiso seguimiento
        // PermissionPermission::create(['name' => 'paciente', 'permission_id' => $permission->id, 'role_id' => $rolePSP->id ]);
        // PermissionPermission::create(['name' => 'tratamiento', 'permission_id' => $permission->id , 'role_id' => $rolePSP->id]);
        // PermissionPermission::create(['name' => 'comunicaciones', 'permission_id' => $permission->id , 'role_id' => $rolePSP->id]);

        // PermissionPermission::create(['name' => 'paciente', 'permission_id' => $permission->id, 'role_id' => $roleCRS->id ]);
        // PermissionPermission::create(['name' => 'reposicion', 'permission_id' => $permission->id , 'role_id' => $roleCRS->id]);
        // PermissionPermission::create(['name' => 'comunicaciones', 'permission_id' => $permission->id , 'role_id' => $roleCRS->id]);

        // PermissionPermission::create(['name' => 'paciente', 'permission_id' => $permission->id, 'role_id' => $roleAD->id ]);
        // PermissionPermission::create(['name' => 'tratamiento', 'permission_id' => $permission->id , 'role_id' => $roleAD->id]);
        // PermissionPermission::create(['name' => 'comunicaciones', 'permission_id' => $permission->id , 'role_id' => $roleAD->id]);

        // $permission = Permission::create(['name' => 'productos'])->syncRoles([$rolePSP]);
        // // creación de los accesos que tiene el permiso productos
        // PermissionPermission::create(['name' => 'registrar material', 'permission_id' => $permission->id, 'role_id' => $rolePSP->id ]);
        // PermissionPermission::create(['name' => 'inventario', 'permission_id' => $permission->id, 'role_id' => $rolePSP->id ]);
        // PermissionPermission::create(['name' => 'movimiento inventario', 'permission_id' => $permission->id, 'role_id' => $rolePSP->id ]);
        // PermissionPermission::create(['name' => 'detalles inventario', 'permission_id' => $permission->id, 'role_id' => $rolePSP->id ]);
        // PermissionPermission::create(['name' => 'inventarios', 'permission_id' => $permission->id, 'role_id' => $rolePSP->id ]);
        // PermissionPermission::create(['name' => 'descarga inventario', 'permission_id' => $permission->id, 'role_id' => $rolePSP->id ]);

        // $permission = Permission::create(['name' => 'reportes'])->syncRoles([$rolePSP, $roleAD]);
        // // creación de los accesos que tiene el permiso reportes
        // PermissionPermission::create(['name' => 'pacientes', 'permission_id' => $permission->id, 'role_id' => $rolePSP->id ]);
        // PermissionPermission::create(['name' => 'reclamacion', 'permission_id' => $permission->id, 'role_id' => $rolePSP->id ]);
        // PermissionPermission::create(['name' => 'conciliacion', 'permission_id' => $permission->id, 'role_id' => $rolePSP->id ]);
        // PermissionPermission::create(['name' => 'terapias', 'permission_id' => $permission->id, 'role_id' => $rolePSP->id ]);

        // $permission = Permission::create(['name' => 'usuario nuevo'])->syncRoles([$rolePSP]);
        // // creación de los accesos que tiene el permiso reportes
        // PermissionPermission::create(['name' => 'medico', 'permission_id' => $permission->id, 'role_id' => $roleAD->id ]);
        // PermissionPermission::create(['name' => 'proveedor', 'permission_id' => $permission->id, 'role_id' => $roleAD->id ]);

        // Permission::create(['name' => 'configuración'])->syncRoles([$roleCRS, $roleAD]);
        // Permission::create(['name' => 'paciente nuevo'])->syncRoles([$roleAD]);
    }
}
