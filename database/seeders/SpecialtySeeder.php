<?php

namespace Database\Seeders;

use App\Models\Specialty;
use Illuminate\Database\Seeder;

class SpecialtySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <count(Specialty::NAME); $i++) {
            Specialty::create([
                'name'      => Specialty::NAME[$i],
            ]);
        }
    }
}
