<?php

namespace Database\Seeders;

use App\Models\NoveltyStatu;
use Illuminate\Database\Seeder;

class NoveltyStatuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <count(NoveltyStatu::NAME); $i++) {
            NoveltyStatu::create([
                'name'      => NoveltyStatu::NAME[$i],
                'colour'    => NoveltyStatu::COLOUR[$i],
            ]);
        }
    }
}
