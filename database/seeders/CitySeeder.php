<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Department;
use App\Models\Zone;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::create([
            'nombre'            => 'Santa Marta',
            'departamento_id'   => Department::where('nombre', 'Magdalena')->first()['id'],
            'zone_id'           => Zone::all()->random()->id
        ]);

        City::create([
            'nombre'            => 'Bogota',
            'departamento_id'   => Department::where('nombre', 'Cundinamarca')->first()['id'],
            'zone_id'           => Zone::all()->random()->id
        ]);

        City::create([
            'nombre'            => 'Tunja',
            'departamento_id'   => Department::where('nombre', 'Boyaca')->first()['id'],
            'zone_id'           => Zone::all()->random()->id

            
        ]);

        City::create([
            'nombre'            => 'Fundación',
            'departamento_id'   => Department::where('nombre', 'Magdalena')->first()['id'],
            'zone_id'           => Zone::all()->random()->id

        ]);

        City::create([
            'nombre'            => 'Medellin',
            'departamento_id'   => Department::where('nombre', 'Antioquia')->first()['id'],
            'zone_id'           => Zone::all()->random()->id

        ]);

        City::create([
            'nombre'            => 'Barranquilla',
            'departamento_id'   => Department::where('nombre', 'Atlantico')->first()['id'],
            'zone_id'           => Zone::all()->random()->id

        ]);

        City::create([
            'nombre'            => 'Valledeupar',
            'departamento_id'   => Department::where('nombre', 'Cesar')->first()['id'],
            'zone_id'           => Zone::all()->random()->id

        ]);
    }
}
