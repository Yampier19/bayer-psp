<?php

namespace Database\Seeders;

use App\Models\Insurance;
use App\Models\LogisticOperator;
use Illuminate\Database\Seeder;

class LogisticOperatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <count(LogisticOperator::NAME); $i++) {
            LogisticOperator::create([
                'name'          => LogisticOperator::NAME[$i],
                'insurance_id'  => Insurance::all()->random()->id,
            ]);
        }
    }
}
