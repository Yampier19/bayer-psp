<?php

namespace Database\Seeders;

use App\Models\Insurance;
use App\Models\Ips;
use Illuminate\Database\Seeder;

class IpsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <count(Ips::NAME); $i++) {
            Ips::create([
                'name'          => Ips::NAME[$i],
                'insurance_id'  => Insurance::all()->random()->id,
            ]);
        }
    }
}
