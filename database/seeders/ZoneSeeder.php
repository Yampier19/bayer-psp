<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Zone;
use Illuminate\Database\Seeder;

class ZoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i=0; $i <count(Zone::NAME); $i++) {
            Zone::create([
                'name'                  => Zone::NAME[$i],
                'country_id'            => Country::where('name', 'COLOMBIA')->first()['id']
            ]);
        }
    }
}
