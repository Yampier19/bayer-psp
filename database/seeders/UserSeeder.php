<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Country;
use App\Models\Provider;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'                  => 'Admin User',
            'user_name'             => 'Admin',
            'password'              => bcrypt('$adminPsp*@'),
            'type'                  => collect(['Educador','Edugestor'])->random(),
            'country_id'            => Country::all()->random()->id,
            'provider_id'           => Provider::all()->random()->id,
            'phone'                 => 3006408288,
            'password_change_date'  => Carbon::now(),
        ])->assignRole('Super Admin')
        ->applicationSystems()->attach([1,2,3]);

        User::create([
            'name'                  => 'User',
            'user_name'             => 'User',
            'password'              => bcrypt('$adminPsp*@'),
            'type'                  => collect(['Educador','Edugestor'])->random(),
            'country_id'            => Country::all()->random()->id,
            'provider_id'           => Provider::all()->random()->id,
            'phone'                 => 111212212,
            'password_change_date'  => Carbon::now(),

        ])->assignRole('Coordinador PM')
        ->applicationSystems()->attach([1,2]);

        User::create([
            'name'                  => 'Educador',
            'user_name'             => 'Educador',
            'password'              => bcrypt('$adminPsp*@'),
            'country_id'            => Country::all()->random()->id,
            'type'                  => collect(['Educador','Edugestor'])->random(),
            'provider_id'           => Provider::all()->random()->id,
            'phone'                 => 1111111,
            'password_change_date'  => Carbon::now(),

        ])->assignRole('Educador People')
        ->applicationSystems()->attach([1]);

        User::create([
            'name'                  => 'Educador 2',
            'user_name'             => 'Educador 2',
            'password'              => bcrypt('$adminPsp*@'),
            'country_id'            => Country::all()->random()->id,
            'type'                  => collect(['Educador','Edugestor'])->random(),
            'provider_id'           => Provider::all()->random()->id,
            'phone'                 => 12121323132,
            'password_change_date'  => Carbon::now(),

        ])->assignRole('Educador People')
        ->applicationSystems()->attach([1]);
    }
}
