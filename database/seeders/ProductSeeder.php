<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Product;
use App\Models\ProductDose;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <=count(Country::VALUE) ; $i++) {
            $product = Product::create([
                'name'              => Product::ADEMPAS,
                'country_id'        => $i,
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '1.0 mg',
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '1.5 mg',
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '2.0 mg',
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '2.5 mg',
            ]);
            $product = Product::create([
                'name'              => Product::BETAFERON,
                'country_id'        => $i,
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '0.25 ml',
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '0.50 ml',
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '0.75 ml',
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '1.0 ml',
            ]);
            $product = Product::create([
                'name'              => Product::EYLIA,
                'country_id'        => $i,
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '2 mg',
            ]);
            $product = Product::create([
                'name'              => Product::NEXAVAR,
                'country_id'        => $i,
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '400 mg',
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '600 mg',
            ]);
            $product = Product::create([
                'name'              => Product::VENTAVIS,
                'country_id'        => $i,
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '2.5 microgramos',
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '5.0 microgramos',
            ]);
            $product = Product::create([
                'name'              => Product::XARELTO,
                'country_id'        => $i,
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '15 mg',
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '20 mg',
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '10 mg',
            ]);
            $product = Product::create([
                'name'              => Product::NEXAVAR,
                'country_id'        => $i,
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '600 mg',
            ]);
            $product = Product::create([
                'name'              => Product::NUBEQA,
                'country_id'        => $i,
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '300 mg',
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '600 mg',
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '1200 mg',
            ]);
            $product = Product::create([
                'name'              => Product::XOFIGO,
                'country_id'        => $i,
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '300 mg',
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '600 mg',
            ]);
            ProductDose::create([
                'product_id'        =>  $product->id,
                'dose'              => '1200 mg',
            ]);
        }
    }
}
