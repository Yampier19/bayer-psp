<?php

namespace Database\Seeders;

use App\Models\Doctor;
use App\Models\Specialty;
use App\Models\User;
use Illuminate\Database\Seeder;

class DoctorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <count(Doctor::NAME); $i++) {
            Doctor::create([
                'name'              => Doctor::NAME[$i],
                'user_id'           => User::all()->random()->id,
            ])->specialtys()->attach([Specialty::all()->random()->id, Specialty::all()->random()->id]);
        }
    }
}
