<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <count(Department::NAME); $i++) {
            Department::create([
                'nombre'        => Department::NAME[$i],
                'country_id'    => Country::all()->first()['id'],
            ]);
        }
    }
}
