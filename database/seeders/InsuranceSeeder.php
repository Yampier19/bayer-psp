<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\Insurance;
use App\Models\LogisticOperator;
use Illuminate\Database\Seeder;

class InsuranceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <count(Insurance::NAME); $i++) {
            Insurance::create([
                'name'                  => Insurance::NAME[$i],
                'department_id'         => Department::all()->random()->id,
            ]);
        }
    }
}
