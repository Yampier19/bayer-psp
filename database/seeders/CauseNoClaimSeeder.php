<?php

namespace Database\Seeders;

use App\Models\CauseNoClaim;
use Illuminate\Database\Seeder;

class CauseNoClaimSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <count(CauseNoClaim::NAME); $i++) {
            CauseNoClaim::create([
                'name'  => CauseNoClaim::NAME[$i],
            ]);
        }
    }
}
