<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <count(Country::NAME); $i++) {
            Country::create([
                'name'  => Country::NAME[$i],
            ]);
        }
    }
}
