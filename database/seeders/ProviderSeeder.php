<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Provider;
use Illuminate\Database\Seeder;

class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <=3; $i++) {
            Provider::create([
                'name'          => 'Proveedor '.$i,
                'country_id'    => Country::all()->random()->id,
            ]);
        }
    }
}
