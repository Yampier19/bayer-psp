<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\PathologicalClassification;
use App\Models\Product;
use Illuminate\Database\Seeder;

class PathologicalClassificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <count(PathologicalClassification::NAME); $i++) {
            PathologicalClassification::create([
                'name'          => PathologicalClassification::NAME[$i],
                'product_id'    => Product::all()->random()->id,
            ]);
        }
    }
}
