<?php

namespace Database\Seeders;

use App\Models\EducationTheme;
use Illuminate\Database\Seeder;

class EducationThemeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <count(EducationTheme::NAME); $i++) {
            EducationTheme::create([
                'name' => EducationTheme::NAME[$i],
            ]);
        }
    }
}
