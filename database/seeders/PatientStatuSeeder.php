<?php

namespace Database\Seeders;

use App\Models\PatientStatu;
use Illuminate\Database\Seeder;

class PatientStatuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <count(PatientStatu::NAME); $i++) {
            PatientStatu::create([
                'name'  => PatientStatu::NAME[$i],
            ]);
        }
    }
}
