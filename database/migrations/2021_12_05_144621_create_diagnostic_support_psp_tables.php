<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiagnosticSupportPspTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diagnostic_support_psp', function (Blueprint $table) {
            $table->id();
            $table->string('PAP');

            //product
            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products');

            //tratamiento
            $table->unsignedBigInteger('treatment_id');
            $table->foreign('treatment_id')->references('id')->on('treatments');

            $table->integer('quantity_exams');
            $table->string('voucher_number');
            $table->string('diagnostic_medical_center');
            $table->string('file')->nullable();
            $table->string('management');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('exams_diagnostic_support_psp', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->double('price');
            $table->string('order');
            $table->unsignedBigInteger('diagnostic_support_id');
            $table->foreign('diagnostic_support_id')->references('id')->on('diagnostic_support_psp');
            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diagnostic_support_psp_tables');
    }
}
