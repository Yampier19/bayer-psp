<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('patients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('last_name');
            $table->dateTime('date_active')->comment('fecha de activación');
            $table->string('email')->nullable();
            $table->enum('document_type',['CC','DNI','TI'])->comment('tipo de documento');
            $table->bigInteger('document_number');

            $table->enum('change_state_patient',['SI','NO'])->nullable()->comment('solicitar cambio de estado de paciente');
            $table->date('retirement_date')->nullable()->comment('fecha de retiro');
            $table->string('retirement_reason')->nullable()->comment('motivo de retirto');
            $table->longText('retirement_reason_observations')->nullable()->comment('observaciones de retiro');

            $table->unsignedBigInteger('city_id');
            $table->foreign('city_id')->references('id')->on('cities');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('patient_statu_id');
            $table->foreign('patient_statu_id')->references('id')->on('patient_status');

            $table->bigInteger('consecutive_application_system')->comment('id consecutivo por sistema de la aplicacion');
            $table->unsignedBigInteger('application_system_id')->comment('sistema a que pertenece');
            $table->foreign('application_system_id')->references('id')->on('application_system');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->string('address',500);
            $table->string('neighborhood',400)->nullable()->comment('barrio');
            $table->enum('gender',['MASCULINO','FEMENINO','OTROS'])->comment('genero');
            $table->string('date_birth')->comment('fecha de nacimiento');
            $table->bigInteger('age')->nullable()->comment('edad');
            $table->string('guardian')->nullable()->comment('acudiente');
            $table->bigInteger('guardian_phone')->nullable()->comment('telefono acudiente');
            $table->longText('note')->nullable()->comment('notas');
            $table->string('image')->nullable()->comment('imagen de archivo');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('patient_phones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('number');
            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');

            $table->unsignedBigInteger('patient_id');
            $table->foreign('patient_id')->references('id')->on('patients');

            $table->timestamps();
            $table->softDeletes();
        });



        Schema::create('attached_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('claim_request_letter')->nullable()->comment('carta de solicitud de reclamación');
            $table->string('adverse_event_format')->nullable()->comment('formato de evento adverso');
            $table->string('product_sticker')->nullable()->comment('stiker del producto');
            $table->string('product_delivery_letter')->nullable()->comment('carta de entrega del producto');

            $table->unsignedBigInteger('patient_id');
            $table->foreign('patient_id')->references('id')->on('patients');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_status');
        Schema::dropIfExists('patients');
        Schema::dropIfExists('patient_phones');
        // Schema::dropIfExists('claims_history');
    }
}
