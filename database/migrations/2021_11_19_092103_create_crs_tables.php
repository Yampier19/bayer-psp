<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_medical', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('activation_date')->comment('fecha de activación');
            $table->string('full_name')->comment('nombre completo');
            $table->enum('document_type',['CC','DNI','TI'])->nullable()->comment('tipo de documento');
            $table->bigInteger('document_number');
            $table->string('address')->nullable();
            $table->string('email')->nullable();
            $table->enum('gender',['MASCULINO','FEMENINO','OTROS'])->comment('genero')->nullable();
            $table->date('date_birth')->nullable()->comment('fecha de nacimiento');

            $table->unsignedBigInteger('city_id');
            $table->foreign('city_id')->references('id')->on('cities');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('replacements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('doctor_code')->comment('codigo de medico');
            $table->bigInteger('crs_code')->comment('codigo de CRS');
            $table->string('full_name')->comment('nombre completo');
            $table->string('address')->nullable();
            $table->date('application_date')->nullable()->comment('fecha de solicitud');
            $table->string('lote')->nullable();
            $table->bigInteger('argus_code')->nullable()->comment('codigo argus');
            $table->string('qualification')->nullable()->comment('calificacion');
            $table->date('replacement_date')->nullable()->comment('fecha de reposición');
            $table->enum('repositioning_done',['SI','NO']);
            $table->string('repositioning_done_date')->nullable()->comment('fecha de reposicion realizada');
            $table->string('email')->nullable();
            $table->date('adverse_event_date')->nullable()->comment('fecha de evento adverso');
            $table->enum('done',['SI','NO'])->comment('cumple?');

            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products');

            $table->unsignedBigInteger('city_id');
            $table->foreign('city_id')->references('id')->on('cities');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('replacements_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('comment');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('replacement_id');
            $table->foreign('replacement_id')->references('id')->on('replacements');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_medical');
        Schema::dropIfExists('replacements');
        Schema::dropIfExists('replacements_comments');
    }
}
