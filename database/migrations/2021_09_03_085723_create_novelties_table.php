<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoveltiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('novelty_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 300);
            $table->string('colour');
            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('novelties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('affair',500)->comment('asunto');
            $table->string('novelty')->comment('novedad');
            $table->string('report_date')->comment('fecha de reporte');
            $table->string('answer_date')->comment('fecha de pregunta');
            $table->string('observation', 400)->nullable();

            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products');

            $table->unsignedBigInteger('user_id')->coment('usuario que creo la novedad');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('manager_id')->coment('usuario a quien se le asigna');
            $table->foreign('manager_id')->references('id')->on('users');

            $table->unsignedBigInteger('novelty_statu_id');
            $table->foreign('novelty_statu_id')->references('id')->on('novelty_status');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('novelty_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('description');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('novelty_id');
            $table->foreign('novelty_id')->references('id')->on('novelties');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('novelty_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('description');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('novelty_id');
            $table->foreign('novelty_id')->references('id')->on('novelties');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('novelty_comment_photos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('file');

            $table->unsignedBigInteger('novelty_comment_id');
            $table->foreign('novelty_comment_id')->references('id')->on('novelty_comments');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('novelty_comment_responses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('description');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('novelty_comment_id');
            $table->foreign('novelty_comment_id')->references('id')->on('novelty_comments');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('novelty_status');
        Schema::dropIfExists('novelties');
        Schema::dropIfExists('novelty_history');
        Schema::dropIfExists('novelty_comments');
        Schema::dropIfExists('novelty_comment_photos');
        Schema::dropIfExists('novelty_comment_responses');
    }
}
