<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplieTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference_number')->comment('numero de referencia');
            $table->enum('medicine_without_commercial_value',['SI','NO'])->default('NO');
            //id del producto
            $table->unsignedBigInteger('product_id')->nullable();
            $table->foreign('product_id')->references('id')->on('products');

            //nombre producto
            $table->string("product_name")->nullable()->comment('Nombre del producto');

            // id de la dosis del producto
            $table->unsignedBigInteger('product_doses_id')->nullable();
            $table->foreign('product_doses_id')->references('id')->on('product_doses');
            $table->dateTime('expiration_date')->nullable()->comment('Fecha de vencimiento');

            $table->unsignedBigInteger('city_id')->comment('Ciudad de la bodega')->nullable();
            $table->foreign('city_id')->references('id')->on('cities');
            $table->string('address')->nullable()->comment('Ubicación de la bodega');

            $table->integer('quantity_available')->comment('Cantidad total disponible');
            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('supplie_incomes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('referral_number')->nullable()->comment('Número de remisión');

            $table->unsignedBigInteger('supplie_id')->nullable()->comment('Número de referencia');
            $table->foreign('supplie_id')->references('id')->on('supplies')->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('quantity')->nullable()->comment('cantidad');

            // id del responsable
            $table->unsignedBigInteger('user_id')->nullable()->comment('Responsable');
            $table->foreign('user_id')->references('id')->on('users');
            $table->dateTime('supplie_incomes_date')->nullable()->comment('Fecha de ingreso');
            $table->string('supplier')->nullable()->comment('Proveedor');
            $table->string('product_image')->nullable()->comment('Foto del producto');
            $table->text('observations')->nullable()->comment('Observaciones');
            
            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('supplie_expenses', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('supplie_income_id')->nullable()->comment('Número de referencia');
            $table->foreign('supplie_income_id')->references('id')->on('supplie_incomes')->onDelete('cascade')->onUpdate('cascade');

            //codigo del usuario
            $table->unsignedBigInteger('patient_id')->nullable()->comment('id del paciente');
            $table->foreign('patient_id')->references('id')->on('patients');

            $table->bigInteger('quantity')->nullable()->comment('Cantidad egresada');

            // id del responsable
            $table->unsignedBigInteger('user_id')->nullable()->comment('Responsable');
            $table->foreign('user_id')->references('id')->on('users');
            $table->dateTime('supplie_expenses_date')->nullable()->comment('Fecha de egreso');
            $table->enum('third_party_for_the_distribution_of_the_product',['SI','NO'])->default('NO');
            $table->text('observations')->nullable()->comment('Observaciones');
            $table->string('supplier_name')->nullable()->comment('Nombre del proveedor');
            $table->string('product_image')->nullable()->comment('Foto del producto');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->enum('from_follow',['SI','NO'])->default('NO')->comment('Viene de seguimiento?');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('supplie_providers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('supplie_income_id')->nullable()->comment('Número de referencia');
            $table->foreign('supplie_income_id')->references('id')->on('supplie_incomes')->onDelete('cascade')->onUpdate('cascade');

            //codigo del usuario
            $table->unsignedBigInteger('patient_id')->nullable()->comment('id del paciente');
            $table->foreign('patient_id')->references('id')->on('patients');

            $table->bigInteger('quantity')->nullable()->comment('Cantidad devuelta');

            // id del responsable
            $table->unsignedBigInteger('user_id')->nullable()->comment('Responsable');
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->dateTime('supplie_provider_date')->nullable()->comment('Fecha de devolucion');
            $table->string('who_return')->nullable()->comment('Quien devuelve');

            $table->text('reason_for_return')->nullable()->comment('Razón de devolución');
            $table->text('observations')->nullable()->comment('Observaciones');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->string('product_image_provider')->nullable()->comment('Foto del producto cuando se devuelve');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('supplie_states', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        /*Schema::create('supplie_providers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('store')->comment('bodega');
            $table->string('address');
            $table->bigInteger('phone');
            $table->string('conveyor')->comment('transportador');
            $table->string('observation')->nullable();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('city_id');
            $table->foreign('city_id')->references('id')->on('cities');

            $table->unsignedBigInteger('supplie_id');
            $table->foreign('supplie_id')->references('id')->on('supplies');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplie_tables');
        Schema::dropIfExists('supplie_expenses');
        Schema::dropIfExists('supplie_providers');
        Schema::dropIfExists('supplie_states');
    }
}
