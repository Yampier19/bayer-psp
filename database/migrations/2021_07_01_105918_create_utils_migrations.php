<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUtilsMigrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cause_no_claims', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 500);
            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('pathological_classifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name');

            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products');

            // $table->unsignedBigInteger('country_id');
            // $table->foreign('country_id')->references('id')->on('countries');


            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cause_no_claims');
        Schema::dropIfExists('pathological_classifications');
    }
}
