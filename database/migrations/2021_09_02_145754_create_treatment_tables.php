<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTreatmentTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treatment_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 200);

            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('education_themes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 300);
            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('education_reason', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 300);
            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('treatment_previous', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 300);
            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('treatments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('claim',['SI','NO'])->comment('reclamo');
            $table->string('claim_date')->nullable()->comment('fecha de reclamación');
            $table->string('reason')->nullable()->comment('motivo de no reclamacion');
            $table->bigInteger('number_box')->nullable()->comment('numero de cajas');
            $table->enum('units',['Ampolla(s)','Aplicacion','Caja(s)'])->nullable()->comment('unidades de numero de cajas');
            $table->string('dose')->nullable()->comment('dosis');
            $table->enum('dose_start',['SI','NO'])->comment('inicio de dosis');
            $table->enum('education',['SI','NO'])->comment('se brindo educación');
            $table->string('education_date')->nullable()->comment('fecha de educación');
            $table->string('consent')->nullable()->comment('consentimiento');
            $table->string('therapy_start_date')->nullable()->comment('fecha de inicio de terapia');
            $table->string('regime')->nullable()->comment('regimen');
            $table->string('other_operator')->nullable()->comment('Otros operadores');
            $table->string('delivery_point')->nullable()->comment('punto de entrega');
            $table->string('means_acquisition')->nullable()->comment('Medios de adquisición');
            $table->string('file')->nullable()->comment('Adjunto del archivo');

            $table->string('paramedic')->nullable()->comment('Paramédico o representante');
            $table->string('cause_no_visit')->nullable()->comment('Causa no visita');
            $table->string('zone')->nullable()->comment('Zona de atención paramédico o representante');
            $table->string('code')->nullable()->comment('codigo');
            $table->enum('patient_part_PAAP',['SI','NO'])->nullable()->comment('Paciente hace parte del PAAP');
            $table->bigInteger('number_tablet')->nullable()->comment('numero de tabletas');
            $table->bigInteger('number_lots')->nullable()->comment('numero de lotes');
            $table->enum('add_application_information',['SI','NO'])->nullable()->comment('Agregar Información aplicaciones');
            $table->string('shipping_type')->comment('Tipo de envio');
            $table->bigInteger('consecutive_product_id')->comment('id del consecutivo por productos');
            $table->string('ips',300)->nullable();

            // nuevos atributos
            $table->string('formulation_date')->nullable()->comment('fecha de formulación');
            $table->enum('active_for_change',['SI','NO'])->nullable()->comment('activo para cambio');
            $table->string('initial_visit_schedule')->nullable()->comment('programación de visita inicial');
            $table->enum('effective_initial_visit',['SI','NO'])->nullable()->comment('visita inicial efectiva');
            $table->string('medication_date')->nullable()->comment('medicamento hasta');
            $table->enum('comunication',['SI','NO'])->nullable()->comment('se logró comunicación');
            $table->string('reason_communication')->nullable()->comment('Motivo de la comunicación');
            $table->string('reason_not_communication')->nullable()->comment('motivo de no comunicación');
            $table->bigInteger('number_attemps')->nullable()->comment('numero de intentos');
            $table->string('contact_type')->nullable()->comment('tipo de contacto');
            $table->string('contact_medium')->nullable()->comment('medio de contacto');
            $table->string('call_type')->nullable()->comment('tipo de llamada');
            $table->bigInteger('authorization_number')->nullable()->comment('numero de autorización');
            $table->enum('difficulty_access',['SI','NO'])->nullable()->comment('dificultad en el acceso');
            $table->string('type_difficulty')->nullable()->comment('tipo de dificultad');
            $table->enum('generate_request',['SI','NO'])->nullable()->comment('genera requerimeinto');
            $table->enum('adverse_event',['SI','NO'])->nullable()->comment('evento adverso');
            $table->string('type_adverse_event')->nullable()->comment('tipo de evento adverso');
            $table->string('date_next_call')->comment('Fecha de la próxima llamada');
            $table->string('reason_next_call')->nullable()->comment('motivo de la proxima llamada');
            $table->text('observation_next_call')->nullable()->comment('observaciones de la proxima llamada');
            $table->string('consecutive')->nullable()->comment('consecutivo');
            $table->string('generate_requirement')->nullable()->comment('genera requerimiento');
            $table->enum('Shipping_request',['SI','NO'])->nullable()->comment('solicitud de envio');
            $table->text('communication_description')->nullable()->comment('descripcion de comunicacion');
            $table->text('note')->nullable()->comment('notas');
            $table->string('require_support')->nullable()->comment('requiere apoyo');
            $table->enum('support_is_provided',['SI','NO'])->nullable()->comment('Se brindo apoyo');
            $table->string('transfer_type')->nullable()->comment('tipo de transferencia');
            $table->string('code_xofigo')->nullable()->comment('codigo xofigo');
            $table->string('doctor_formulator')->nullable();
            /*$table->unsignedBigInteger('doctor_formulator')->nullable();
            $table->foreign('doctor_formulator')->references('id')->on('doctors');*/

            $table->unsignedBigInteger('product_dose_id')->nullable();
            $table->foreign('product_dose_id')->references('id')->on('product_doses');

            // **************
            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products');

            $table->unsignedBigInteger('treatment_previou_id')->comment('tratamiento previo');
            $table->foreign('treatment_previou_id')->references('id')->on('treatment_previous');

            $table->unsignedBigInteger('education_theme_id')->nullable()->comment('tema educación');
            $table->foreign('education_theme_id')->references('id')->on('education_themes');

            $table->unsignedBigInteger('education_reason_id')->nullable()->comment('motivo educación');
            $table->foreign('education_reason_id')->references('id')->on('education_reason');

            $table->unsignedBigInteger('patient_statu_id');
            $table->foreign('patient_statu_id')->references('id')->on('patient_status');

            $table->unsignedBigInteger('pathological_classification_id');
            $table->foreign('pathological_classification_id')->references('id')->on('pathological_classifications');



            $table->unsignedBigInteger('insurance_id');
            $table->foreign('insurance_id')->references('id')->on('insurances');

            $table->unsignedBigInteger('logistic_operator_id');
            $table->foreign('logistic_operator_id')->references('id')->on('logistic_operators');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('patient_id');
            $table->foreign('patient_id')->references('id')->on('patients');

            $table->unsignedBigInteger('doctor_id');
            $table->foreign('doctor_id')->references('id')->on('doctors');

            $table->unsignedBigInteger('specialty_id');
            $table->foreign('specialty_id')->references('id')->on('specialtys');

            $table->unsignedBigInteger('cause_no_claim_id')->nullable()->comment('causo no reclamación');
            $table->foreign('cause_no_claim_id')->references('id')->on('cause_no_claims');

            $table->unsignedBigInteger('treatment_statu_id')->nullable()->comment('estado del paciente según el producto');
            $table->foreign('treatment_statu_id')->references('id')->on('treatment_status');

            $table->unsignedBigInteger('city_id')->nullable();
            $table->foreign('city_id')->references('id')->on('cities');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('claims_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('claim',['SI','NO'])->comment('reclamo');
            $table->date('claim_date')->nullable()->comment('Fecha reclamación');
            $table->string('reason')->nullable()->comment('motivo de no reclamacion');

            $table->unsignedBigInteger('cause_no_claim_id')->nullable()->comment('causo no reclamación');
            $table->foreign('cause_no_claim_id')->references('id')->on('cause_no_claims');

            $table->unsignedBigInteger('treatment_id');
            $table->foreign('treatment_id')->references('id')->on('treatments');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('treatment_follows', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('initial_visit_schedule')->comment('programación de visita inicial');
            $table->enum('effective_initial_visit',['SI','NO'])->nullable()->comment('visita inicial efectiva');
            $table->string('claim')->nullable()->comment('reclamo');
            $table->string('medication_date')->nullable()->comment('medicamento hasta');
            $table->enum('comunication',['SI','NO'])->nullable()->comment('se logró comunicación');
            $table->string('reason_communication')->nullable()->comment('Motivo de la comunicación');
            $table->string('contact_medium')->nullable()->comment('medio de contacto');
            $table->string('call_type')->nullable()->comment('tipo de llamada');
            $table->string('reason_not_communication')->nullable()->comment('motivo de no comunicación');
            $table->bigInteger('number_attemps')->nullable()->comment('numero de intentos');

            $table->unsignedBigInteger('ips_id');
            $table->foreign('ips_id')->references('id')->on('ips');

            $table->unsignedBigInteger('treatment_id');
            $table->foreign('treatment_id')->references('id')->on('treatments');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('treatment_follow_communications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description');
            $table->string('next_contact')->comment('proximo contacto');
            $table->string('last_collection')->comment('fecha de última recolección');
            $table->string('start_paap')->comment('fecha de inicio de paap');
            $table->string('code_argus')->comment('codigo de argus');
            $table->string('next_collection')->nullable()->comment('fecha de la siguiente recollección');
            $table->string('end_paap')->nullable()->comment('fecha de fin paap');
            $table->string('image')->nullable();

            $table->unsignedBigInteger('treatment_id');
            $table->foreign('treatment_id')->references('id')->on('treatments');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('information_applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("where_eye")->nullable()->comment("Cual ojo?");
            $table->enum('have_application',['SI','NO'])->default('SI')->comment("Cuenta con aplicacion?");
            $table->dateTime('date_application')->nullable()->comment('Fecha de la aplicacion');
            $table->text("reason_for_non_application")->nullable()->comment("Razón de la no aplicacion");
            $table->unsignedBigInteger('treatment_id');
            $table->foreign('treatment_id')->references('id')->on('treatments');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('treatment_status');
        Schema::dropIfExists('education_themes');
        Schema::dropIfExists('education_reason');
        Schema::dropIfExists('treatment_previous');
        Schema::dropIfExists('treatments');
        Schema::dropIfExists('treatment_follows');
        Schema::dropIfExists('treatment_follow_communications');
    }
}
