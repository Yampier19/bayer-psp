<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('user_name', 50);
            $table->string('password');
            $table->string('image')->nullable();
            $table->string('type');
            $table->string('reason_inactivity')->nullable()->comment('rason por inactividad');
            $table->string('code')->nullable()->comment('codigo de verificación para cambio de contraseña');
            $table->bigInteger('phone');
            $table->bigInteger('password_attempt')->default(0)->comment('numero de intentos, de contraseñna incorrecta');
            $table->dateTime('password_change_date')->comment('fecha de cambio de contraseña');

            $table->unsignedBigInteger('provider_id');
            $table->foreign('provider_id')->references('id')->on('providers');

            $table->unsignedBigInteger('country_id');
            $table->foreign('country_id')->references('id')->on('countries');

            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('users_actions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('action');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create('application_system', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->enum('status',['ACTIVO','DESACTIVADO'])->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('user_has_application_system', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('application_system_id');
            $table->foreign('application_system_id')->references('id')->on('application_system');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('application_system');
        Schema::dropIfExists('user_has_application_system');
    }
}
