<?php

namespace App\Http\Requests;

use App\Traits\ApiResponser;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class PatientRequest extends FormRequest
{
    use ApiResponser;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            // paciente
            'patient.name'                              => 'bail|required|string|min:3|max:50',
            'patient.last_name'                         => 'bail|required|string|min:3|max:100',
            'patient.document_number'                   => 'bail|required|numeric|digits_between:7,10',
            'patient.document_type'                     => 'bail|required|string',
            'patient.date_active'                       => 'bail|required|date',
            'patient.email'                             => 'bail|nullable|email|string|min:5',
            'patient.patient_statu_id'                  => 'bail|required|exists:patient_status,id',
            'patient.address'                           => 'bail|required|string|min:5',
            'patient.gender'                            => 'bail|required|string',
            'patient.date_birth'                        => 'bail|required|date',
            'patient.age'                               => 'bail|required|numeric|min:1|max:150',
            'patient.guardian'                          => 'bail|nullable|string|min:3',
            'patient.guardian_phone'                    => 'bail|nullable|numeric',
            'patient.image'                             => 'bail|nullable|image',
            'patient.phones'                            => 'bail|required|array',
            'patient.application_system_id'             => 'bail|required|exists:application_system,id',


            //tratamiento
            'treatment.claim'                          => 'bail|required|string',
            'treatment.number_box'                     => 'bail|required|numeric|min:1',
            'treatment.dose'                           => 'bail|nullable|string|min:3',
            'treatment.dose_start'                     => 'bail|required|string',
            'treatment.pathological_classification_id' => 'bail|required|exists:pathological_classifications,id',
            'treatment.consent'                        => 'bail|required|string|min:3',
            'treatment.therapy_start_date'             => 'bail|required|date',
            'treatment.regime'                         => 'bail|nullable|string|min:3',
            'treatment.product_id'                     => 'bail|required|exists:products,id',
            'treatment.product_dose_id'                => 'bail|nullable|exists:product_doses,id',
            'treatment.insurance_id'                   => 'bail|required|exists:insurances,id',
            'treatment.logistic_operator_id'           => 'bail|required|exists:logistic_operators,id',
            'treatment.doctor_id'                      => 'bail|required|exists:doctors,id',
            'treatment.specialty_id'                   => 'bail|required|exists:specialtys,id',
            'treatment.other_operator'                 => 'bail|nullable|string|min:3',
            'treatment.delivery_point'                 => 'bail|nullable|string|min:3',
            'treatment.means_acquisition'              => 'bail|nullable|string|min:3',
            'treatment.date_next_call'                 => 'bail|nullable|date',
            'treatment.patient_part_PAAP'              => 'bail|string|max:2',
            'treatment.add_application_information'    => 'bail|nullable|string|max:2',
            'treatment.ips'                            => 'bail|required|string|max:280',
            'treatment.cause_no_claim_id'              => 'bail|nullable|exists:cause_no_claims,id',
            'treatment.treatment_statu_id'             => 'bail|nullable|exists:treatment_status,id',
            'treatment.patient_statu_id'               => 'bail|required|exists:patient_status,id',
        ];
    }

    public function failedValidation(ValidationValidator $validator) {
        $message = $validator->errors()->first();
        throw new HttpResponseException($this->showMessage($message, 500, false));
    }
}
