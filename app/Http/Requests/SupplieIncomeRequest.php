<?php

namespace App\Http\Requests;

use App\Traits\ApiResponser;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class SupplieIncomeRequest extends FormRequest
{
    use ApiResponser;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "referral_number"       => "string|min:3|unique:supplie_incomes,referral_number",
            "reference_number"      => "string|min:3",
            "product_id"            => "numeric",
            "product_name"          => "string",
            "product_doses_id"      => "numeric|nullable",
            "quantity"              => "numeric|nullable",
            "user_id"               => "numeric|nullable",
            "supplie_incomes_date"  => "date|nullable",
            "product_image"         => "mimes:jpeg,jpg|nullable"
        ];
    }

    public function failedValidation(ValidationValidator $validator) {
        $message = $validator->errors()->first();
        throw new HttpResponseException($this->showMessage($message, 500, false));
    }
}
