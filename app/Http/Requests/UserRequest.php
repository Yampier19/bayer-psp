<?php

namespace App\Http\Requests;

use App\Traits\ApiResponser;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserRequest extends FormRequest
{
    use ApiResponser;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user.name'                 => 'bail|required|string|min:3|max:60',
            'user.password'             => 'bail|required|min:8',
            'user.user_name'            => 'bail|required|min:8|unique:users,user_name',
            'user.type'                 => 'bail|required|string',
            'user.provider'             => 'bail|required|string',
            'user.country_id'           => 'bail|required|numeric|exists:countries,id',
            'user.role'                 => 'bail|required|array',
            'user.phone'                => 'bail|required|digits_between:7,10|unique:users,phone',
            'user.system'               => 'bail|required|exists:application_system,id'
        ];
    }

    public function messages(){
        return  [
            'user.email.unique'                 => 'El email ya se encuentra registrado',
            'user.user.unique'                  => 'El nombre de usuario ya se encuentra registrado',
            'user.phone.unique'                 => 'Este numero de telefono ya se encuentra registrado',
            'user.role.required'                => 'Debe escoger un rol para el usuario',
            'user.system.required'              => 'Debe escoger por lo menos un sistema para el usuario',
        ];
    }

    public function failedValidation(ValidationValidator $validator) {
        $message = $validator->errors()->first();
        throw new HttpResponseException($this->showMessage($message, 500, false));
    }
}
