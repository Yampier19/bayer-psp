<?php

namespace App\Http\Requests;

use App\Traits\ApiResponser;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class DiagnosticSupportPsPRequest extends FormRequest
{
    use ApiResponser;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // diagnostic_support
            'PAP'                               => 'bail|required|string|min:3|max:50',
            'product_id'                        => 'bail|required|exists:products,id',
            'treatment_id'                      => 'bail|required|exists:treatments,id',
            'quantity_exams'                    => 'bail|required|numeric|max:100',
            'voucher_number'                    => 'bail|required|string|min:3|max:100',
            'diagnostic_medical_center'         => 'bail|required|string|min:3|max:100',
            'management'                        => 'bail|required|string',
        ];
    }

    public function failedValidation(ValidationValidator $validator) {
        $message = $validator->errors()->first();
        throw new HttpResponseException($this->showMessage($message, 500, false));
    }
}
