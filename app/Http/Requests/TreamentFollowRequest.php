<?php

namespace App\Http\Requests;

use App\Traits\ApiResponser;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class TreamentFollowRequest extends FormRequest
{
    use ApiResponser;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'treatmentFollow.initial_visit_schedule'           => 'bail|required|string|min:3',
            'treatmentFollow.effective_initial_visit'          => 'bail|required|string|min:1',
            'treatmentFollow.claim'                            => 'bail|required|string|min:3',
            'treatmentFollow.medication_date'                  => 'bail|required|date',
            'treatmentFollow.comunication'                     => 'bail|nullable|string',
            'treatmentFollow.reason_communication'             => 'bail|nullable|string|min:3',
            'treatmentFollow.contact_medium'                   => 'bail|nullable|string|min:3',
            'treatmentFollow.call_type'                        => 'bail|nullable|string',
            'treatmentFollow.reason_not_communication'         => 'bail|nullable|string|min:3',
            'treatmentFollow.number_attemps'                   => 'bail|nullable|numeric',
            'treatmentFollow.ips_id'                           => 'bail|required|exists:ips,id',
        ];
    }


    public function failedValidation(ValidationValidator $validator) {
        $message = $validator->errors()->first();
        throw new HttpResponseException($this->showMessage($message, 500, false));
    }
}
