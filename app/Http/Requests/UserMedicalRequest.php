<?php

namespace App\Http\Requests;

use App\Traits\ApiResponser;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserMedicalRequest extends FormRequest
{
    use ApiResponser;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user.activation_date'      => 'bail|required|date',
            'user.full_name'            => 'bail|required|string|min:5|max:300',
            'user.document_type'        => 'bail|required|string|min:2',
            'user.document_number'      => 'bail|required|numeric|digits_between:7,12|unique:user_medical,document_number',
            'user.address'              => 'bail|nullable|string',
            'user.email'                => 'bail|nullable|email|unique:user_medical,email',
            'user.gender'               => 'bail|nullable|string|min:3',
            'user.date_birth'           => 'bail|required|date',
            'user.city_id'              => 'bail|required|exists:cities,id',
        ];
    }

    public function failedValidation(ValidationValidator $validator) {
        $message = $validator->errors()->first();
        throw new HttpResponseException($this->showMessage($message, 500, false));
    }
}
