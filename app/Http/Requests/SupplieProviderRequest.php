<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupplieProviderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "referral_number"       => "string|min:3",
            "reference_number"      => "string|min:3",
            "product_id"            => "numeric|nullable",
            "product_doses_id"      => "numeric|nullable",
            "patient_id"            => "numeric|nullable",
            "quantity"              => "numeric|nullable",
            "user_id"               => "numeric|nullable",
            "supplie_provider_date" => "date|nullable",
            'product_image'         => 'mimes:jpeg,jpg|nullable',
        ];
    }
}
