<?php

namespace App\Http\Requests;

use App\Traits\ApiResponser;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;


class ChangedPassworrequest extends FormRequest
{
    use ApiResponser;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'current_password'      => 'required|string|password',
            'user_name'             => 'required',
            'password'              => 'required|min:8|string|confirmed',
            // 'password_confirmation' => 'required|string'
        ];
    }

    public function messages(){
        return  [
            // 'current_password'      => 'La contraseña es incorrecta',
            'password.min'          => 'Minimo 8 caracteres',
            'password.confirmed'    => 'Las contraseñas no coinciden',
        ];
    }

    public function failedValidation(ValidationValidator $validator) {
        $message = $validator->errors()->first();
        throw new HttpResponseException($this->showMessage($message, 500, false));
    }
}
