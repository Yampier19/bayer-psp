<?php

namespace App\Http\Requests;

use App\Traits\ApiResponser;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class TreamentFollowComunicationRequest extends FormRequest
{
    use ApiResponser;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'           => 'bail|required|string|min:3',
            'next_contact'          => 'bail|required|string|min:1',
            'last_collection'       => 'bail|required|date',
            'start_paap'            => 'bail|required|date',
            'code_argus'            => 'bail|required|string',
            'next_collection'       => 'bail|required|date',
            'end_paap'              => 'bail|required|date',
            'image'                 => 'bail|nullable|image',
        ];
    }


    public function failedValidation(ValidationValidator $validator) {
        $message = $validator->errors()->first();
        throw new HttpResponseException($this->showMessage($message, 500, false));
    }
}
