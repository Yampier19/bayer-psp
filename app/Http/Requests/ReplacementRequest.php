<?php

namespace App\Http\Requests;

use App\Traits\ApiResponser;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ReplacementRequest extends FormRequest
{
    use ApiResponser;

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'replacement.doctor_code'               => 'bail|required|numeric|min:3',
            'replacement.crs_code'                  => 'bail|required|numeric',
            'replacement.full_name'                 => 'bail|required|min:5',
            'replacement.address'                   => 'bail|nullable|string',
            'replacement.application_date'          => 'bail|required|date',
            'replacement.argus_code'                => 'bail|required|string',
            'replacement.qualification'             => 'bail|nullable|string',
            'replacement.replacement_date'          => 'bail|nullable|date',
            'replacement.repositioning_done'        => 'bail|required|string|min:2',
            'replacement.repositioning_done_date'   => 'bail|nullable|string',
            'replacement.email'                     => 'bail|nullable|email',
            'replacement.adverse_event_date'        => 'bail|nullable|nullable',
            'replacement.done'                      => 'bail|required|min:2|string',
            'replacement.product_id'                => 'bail|required|exists:products,id',
            'replacement.city_id'                   => 'bail|required|exists:cities,id',
            'replacement.user_id'                   => 'bail|required|exists:users,id',
        ];
    }


    public function failedValidation(ValidationValidator $validator) {
        $message = $validator->errors()->first();
        throw new HttpResponseException($this->showMessage($message, 500, false));
    }
}
