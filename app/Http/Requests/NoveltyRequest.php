<?php

namespace App\Http\Requests;

use App\Traits\ApiResponser;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;


class NoveltyRequest extends FormRequest
{
    use ApiResponser;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'novelty.affair'                => 'bail|required|string|min:5',
            'novelty.novelty'               => 'bail|required|string|min:5',
            'novelty.report_date'           => 'bail|required|date',
            'novelty.answer_date'           => 'bail|required|date',
            'novelty.observation'           => 'bail|nullable|string|min:5',
            'novelty.product_id'            => 'bail|required|exists:products,id',
            'novelty.manager_id'            => 'bail|required|exists:users,id',
            'novelty.novelty_statu_id'      => 'bail|required|exists:novelty_status,id',
        ];
    }

    public function messages(){
        return  [
            'novelty.affair.required'           => 'El asunto es requerido',
            'novelty.novelty.required'          => 'La novedad es requerida',
            'novelty.affair.required'           => 'El asunto es requerido',
            'novelty.report_date.required'      => 'La fecha de reporte es requerida',
            'novelty.answer_date.required'      => 'La fecha de respuesta es requerida',
            'novelty.report_date.date'          => 'La fecha de reporte debe ser de tipo fecha',
            'novelty.answer_date.date'          => 'La fecha de respuesta debe ser de tipo fecha',
        ];
    }

    public function failedValidation(ValidationValidator $validator) {
        $message = $validator->errors()->first();
        throw new HttpResponseException($this->showMessage($message, 500, false));
    }
}
