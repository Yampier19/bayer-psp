<?php

namespace App\Http\Controllers;

use App\Http\Resources\UtilCollection;
use App\Models\CauseNoClaim;
use Illuminate\Http\Request;

class CauseNoClaimController extends ApiController
{
    protected $cause;

    public function __construct(CauseNoClaim $cause)
    {
        $this->cause = $cause;
    }

    public function index()
    {
        return $this->successResponse(new UtilCollection($this->cause::active()->get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
