<?php

namespace App\Http\Controllers;

use App\Http\Resources\PathologicalClassificationCollection;
use App\Http\Resources\UtilCollection;
use App\Models\PathologicalClassification;
use Illuminate\Http\Request;

class PathologicalClassificationController extends ApiController
{
    protected $pathological;

    public function __construct(PathologicalClassification $pathological)
    {
        $this->pathological = $pathological;
    }

    public function index(Request $request)
    {
        $product =  $request->product;
        return $this->successResponse(new UtilCollection($this->pathological::active()->handleProduct($product)->get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($product)
    {
        // $country = request()->header('country');
        return $this->successResponse(new PathologicalClassificationCollection($this->pathological::active()->handleProduct($product)->get()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
