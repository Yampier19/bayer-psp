<?php

namespace App\Http\Controllers;

use App\Http\Resources\UtilCollection;
use App\Models\EducationTheme;
use Illuminate\Http\Request;

class EducationThemeController extends ApiController
{
    protected $EducationTheme;

    public function __construct(EducationTheme $EducationTheme)
    {
        $this->EducationTheme   = $EducationTheme;
    }

    public function index()
    {
        return $this->successResponse(new UtilCollection($this->EducationTheme::active()->get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
