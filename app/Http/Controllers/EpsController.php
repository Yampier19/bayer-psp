<?php

namespace App\Http\Controllers;

use App\Http\Resources\IpsCollection;
use App\Models\Ips;
use Illuminate\Http\Request;

class EpsController extends ApiController
{
    protected $ips;

    public function __construct(Ips $ips)
    {
        $this->ips   = $ips;
    }

    public function index()
    {
        return $this->successResponse(new IpsCollection($this->ips::active()->get()));
    }

    public function handleIpsInsurance($id)
    {
        return $this->successResponse(new IpsCollection($this->ips::active()->insurance($id)->get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
