<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DiagnosticSupportPsp;
use App\Models\ExamDiagnosticSupportPsP;
use App\Models\Patient;
use App\Http\Resources\DiagnosticSupportPsP as ResourcesDiagnosticSupportPsP;
use App\Http\Resources\DiagnosticSupportPsPCollection;
use App\Http\Requests\DiagnosticSupportPsPRequest;
use Illuminate\Support\Facades\DB;
use App\Exceptions\NonExistentException;
use League\Flysystem\Util;

class DiagnosticSupportPsPController extends ApiController
{
    protected $diagnostic_support;
    protected $exam_diagnostic_support;

    public function __construct(DiagnosticSupportPsp $diagnostic_support, ExamDiagnosticSupportPsP $exam_diagnostic_support)
    {
        //$this->middleware('permission:Registro de novedad')->only(["store", "update"]);
        //$this->middleware('permission:Consulta de novedad')->only(["index", "show"]);
        $this->diagnostic_support = $diagnostic_support;
        $this->exam_diagnostic_support = $exam_diagnostic_support;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->successResponse(new DiagnosticSupportPsPCollection($this->diagnostic_support::active()->get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DiagnosticSupportPsPRequest $request)
    {
        try{
            DB::beginTransaction();

            $data = $request->all();
            if(!Patient::documentNumber($request->PAP)->first()) throw new NonExistentException('No existe el paciente con PAP '.$request->PAP);

            if($request->hasFile('file')){
                $file = $request->file('file');
                $name = time().$file->getClientOriginalName();
                $path = $file->move(public_path().'/files/diagnosticSupport/', $name);
                $data['file'] = Util::normalizePath($path);;
            };

            $diagnostic_support = $this->diagnostic_support::create($data);
            $this->examStore($request->exams, $diagnostic_support);
            DB::commit();
            return $this->showMessage('Apoyo diagnóstico creado correctamente');

        }catch (NonExistentException $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    public function examStore($exams, $diagnostic_support){
        try{
            foreach($exams as $exam){
                $exam = json_decode($exam);
                $exam->diagnostic_support_id = $diagnostic_support->id;

                $objExam = array(
                    "name" => $exam->name,
                    "price" => $exam->price,
                    "order" => $exam->order,
                    "diagnostic_support_id" => $exam->diagnostic_support_id,
                );

                $this->exam_diagnostic_support::create($objExam);
            }
        }catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $diagnostic_support = $this->diagnostic_support->findOrFail($id);
            return $this->successResponse(new ResourcesDiagnosticSupportPsP($diagnostic_support));
        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return $this->errorResponse('Apoyo diagnóstico no encontrado', 404);
        }
    }

    public function treatmentShow($treatment_id){
        try{
            $diagnostic_supports = $this->diagnostic_support::GetTreatment($treatment_id)->get();
            if(count($diagnostic_supports) == 0) throw new NonExistentException("No se encontraron apoyos diagnósticos perteneciente a ese tratamiento");

            return $this->successResponse(new DiagnosticSupportPsPCollection($diagnostic_supports));
        }catch (NonExistentException $e) {
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            DB::beginTransaction();

            $data = $request->all();

            if($request->hasFile('file')){
                $diagnostic_support = $this->diagnostic_support::findOrFail($id);
                $image_path = $diagnostic_support->file;

                if(file_exists($image_path)) {
                    unlink($image_path);
                }

                $file = $request->file('file');
                $name = time().$file->getClientOriginalName();
                $path = $file->move(public_path().'/files/diagnosticSupport/', $name);
                $data['file'] = Util::normalizePath($path);;
            };

            $diagnostic_support = $this->diagnostic_support->findOrFail($id);
            $diagnostic_support->update($data);
            $this->updateExamStore($request->exams, $diagnostic_support);

            DB::commit();
            return $this->showMessage('Apoyo diagnóstico actualizado correctamente');

        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            DB::rollBack();
            return $this->errorResponse('Apoyo diagnóstico no encontrado', 409);
        }
    }

    public function updateExamStore($exams, $diagnostic_support){
        try{
            $exams_actually = $diagnostic_support->exams;
            $exams_collection = collect($exams);

            foreach($exams_actually as $exam_actually){
                if(!$exams_collection->where('id', $exam_actually->id)->first()) $exam_actually->delete();
            }

            foreach ($exams_collection as $exam) {
                $this->exam_diagnostic_support::updateOrCreate([
                    'id'  => isset($exam['id']) ? $exam['id'] : null,
                ],[
                    'name'        => $exam['name'],
                    'price'    => $exam['price'],
                    'diagnostic_support_id'    => $diagnostic_support->id,
                ]);
            }
        }catch (\ErrorException $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $diagnostic_support = $this->diagnostic_support->findOrFail($id);
            $diagnostic_support->delete();
            return $this->showMessage('Apoyo diagnóstico eliminado correctamente');
        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            DB::rollBack();
            return $this->errorResponse('Apoyo diagnóstico no encontrado', 409);
        }

    }
}
