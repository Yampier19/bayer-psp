<?php

namespace App\Http\Controllers;

use App\Http\Helpers\UtilHelper;
use App\Http\Requests\UserMedicalRequest;
use App\Http\Resources\UserMedical as ResourcesUserMedical;
use App\Http\Resources\UserMedicalCollection;
use App\Models\UserMedical;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserMedicalController extends ApiController
{
    protected $userMedical;

    public function __construct(UserMedical $userMedical)
    {
        $this->userMedical = $userMedical;
    }

    public function index()
    {
        return $this->successResponse(new UserMedicalCollection($this->userMedical::all()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserMedicalRequest $request)
    {
        $data               = $request->user;
        $data['user_id']    = \request()->user()->id;
        try {
            DB::beginTransaction();

            $userMedical = $this->userMedical::create($data);

            UtilHelper::UserAction(\request()->user()->id, 'Ha creado un nuevo medico CRS con el nombre de '.$userMedical->full_name);

            DB::commit();
            return $this->showMessage('Nuevo medico creado correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userMedical = $this->userMedical::find($id);
        return $this->successResponse(new ResourcesUserMedical($userMedical));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data           = $request->user;
        $userMedical    = $this->userMedical::find($id);

        $rules = [
            'user.document_number'   => 'nullable|numeric|unique:user_medical,document_number,'.$userMedical->id,//Exeptuar de la validación mismo usuario
            'user.email'             => 'email|nullable|unique:user_medical,email,'.$userMedical->id,
        ];
        $this->validate($request, $rules);
        try {
            DB::beginTransaction();

            $userMedical->update($data);

            UtilHelper::UserAction(\request()->user()->id, 'Ha actualizado un nuevo medico CRS con el nombre de '.$userMedical->full_name);

            DB::commit();
            return $this->showMessage('Nuevo medico actualizado correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userMedical = $this->userMedical::find($id);
        $userMedical->delete();
        UtilHelper::UserAction(\request()->user()->id, 'Ha eliminado un nuevo medico CRS con el nombre de '.$userMedical->full_name);
        return $this->showMessage('Nuevo medico eliminado correctamente');
    }
}
