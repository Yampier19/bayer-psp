<?php

namespace App\Http\Controllers;

use App\Http\Helpers\UtilHelper;
use Illuminate\Http\Request;
use App\Models\SupplieIncome;
use App\Models\Supplie;
use App\Http\Resources\SupplieIncome as ResourcesSupplieIncome;
use App\Http\Resources\SupplieIncomeCollection;
use App\Http\Requests\SupplieIncomeRequest;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Util;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\NonExistentException;

class SupplieIncomeController extends ApiController
{
    protected $supplie_income;
    protected $supplie;

    public function __construct(SupplieIncome $supplie_income, Supplie $supplie)
    {
        $this->supplie_income = $supplie_income;
        $this->supplie = $supplie;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->successResponse(new SupplieIncomeCollection($this->supplie_income->get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SupplieIncomeRequest $request)
    {
        try{
            DB::beginTransaction();
            $data = $request->all();
            $supplie_income = null;
            if($supplie = $this->supplie->existSupplie($request->reference_number, $request->city_id)->first()){
                $available = $supplie->quantity_available;
                $supplie->update([
                    "quantity_available" => $available + $request->quantity
                ]);

                $data["supplie_id"] = $supplie->id;
                $supplie_income = $this->supplie_income::create($data);
            }else{
                if($request->hasFile('product_image')){
                    $file = $request->file('product_image');
                    $name = time().$file->getClientOriginalName();
                    $path = $file->move(public_path().'/images/products/', $name);
                    $data['product_image'] = Util::normalizePath($path);;
                };
                $supplie = $this->supplie::create([
                    "reference_number" => $request->reference_number,
                    "city_id" => $request->city_id,
                    "address" => $request->address,
                    "medicine_without_commercial_value" => $request->medicine_without_commercial_value,
                    "product_id" => $request->product_id,
                    "product_name" => $request->product_name,
                    "product_doses_id" => $request->product_doses_id,
                    "doses_id" => $request->product_doses_id,
                    "expiration_date" => $request->expiration_date,
                    "quantity_available" => $request->quantity
                ]);
                $data["supplie_id"] = $supplie->id;
                $supplie_income = $this->supplie_income::create($data);
            }

            UtilHelper::UserAction(\request()->user()->id, 'Creó un ingreso con número de remisión '.$supplie_income->referral_number);

            DB::commit();
            return $this->showMessage('Ingreso creado correctamente');

        }catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $supplie_income = $this->supplie_income->findOrFail($id);
            return $this->successResponse(new ResourcesSupplieIncome($supplie_income));
        }catch (ModelNotFoundException $e) {
            return $this->errorResponse('Ingreso no encontrado', 404);
        }
    }

    public function referralNumberShow($referral_number){
        try{
            $supplie_income = $this->supplie_income::referral($referral_number)->first();

            if(!$supplie_income) throw new NonExistentException("El producto con número de remisión ".$referral_number." no ha sido ingresado aún");
            $supplie = $supplie_income->supplie;
            $supplie->product_image = $supplie_income->product_image;
            return $this->successResponse($supplie);

        }catch (NonExistentException $e) {
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    public function productShow($product_id){
        try{
            $supplie = $this->supplie::productAvailable($product_id)->first();
            if(!$supplie) throw new NonExistentException("No se encontraron ingresos de ese producto");
            return $this->successResponse(new SupplieIncomeCollection($supplie->supplieIncomes));
        }catch (NonExistentException $e) {
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    public function medicineWithoutCommercialValueShow(){
        try{
            $supplies = $this->supplie::medicineWithoutCommercialValue()->get();
            if(count($supplies) == 0) throw new NonExistentException("No se encontraron ingresos");
            $data = collect();

            foreach($supplies as $supplie){
                $objData = array(
                    "product_id" => $supplie->product_id,
                    "product_name" => $supplie->product_name
                );
                $data->push($objData);
            }

            return $this->successResponse($data);
        }catch (NonExistentException $e) {
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            DB::beginTransaction();

            $data = $request->all();
            $supplie_income = $this->supplie_income->findOrFail($id);
            $supplie_income->update($data);
            UtilHelper::UserAction(\request()->user()->id, 'Actualizó el ingreso con id '.$supplie_income->id);

            DB::commit();
            return $this->showMessage('Ingreso actualizado correctamente');
        }catch (ModelNotFoundException $e) {
            DB::rollBack();
            return $this->errorResponse('Ingreso no encontrado', 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::beginTransaction();
            $supplie_income = $this->supplie_income->findOrFail($id);

            $supplie_income->delete();
            UtilHelper::UserAction(\request()->user()->id, 'Eliminó el ingreso con id '.$supplie_income->id);

            DB::commit();
            return $this->showMessage('Ingreso eliminado correctamente');
        }catch (ModelNotFoundException $e) {
            DB::rollBack();
            return $this->errorResponse('Ingreso no encontrado', 404);
        }
    }
}
