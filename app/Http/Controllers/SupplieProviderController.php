<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SupplieProvider;
use App\Models\Supplie;
use App\Http\Resources\SupplieProvider as ResourcesSupplieProvider;
use App\Http\Resources\SupplieProviderCollection;
use App\Http\Requests\SupplieProviderRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Helpers\UtilHelper;
use League\Flysystem\Util;
use Illuminate\Support\Facades\DB;
use App\Models\SupplieExpense;
use App\Models\SupplieIncome;
use App\Exceptions\NonExistentException;

class SupplieProviderController extends ApiController
{
    protected $supplie_provider;
    protected $supplie;

    public function __construct(SupplieProvider $supplie_provider, Supplie $supplie)
    {
        $this->supplie_provider = $supplie_provider;
        $this->supplie = $supplie;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->successResponse(new SupplieProviderCollection($this->supplie_provider->get()));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $data = $request->all();
            $supplie_income = SupplieIncome::referral($request['referral_number'])->first();

            if(!$supplie_income) throw new NonExistentException("El producto con número de remisión ".$request['referral_number']." no ha sido ingresado aún");

            $supplie = Supplie::findOrFail($supplie_income->supplie_id);
            $available = $supplie->quantity_available;

            $supplie->update([
                "quantity_available" => $available + $request['quantity']
            ]);

            $data["supplie_income_id"] = $supplie_income->id;

            if($request->hasFile('product_image_provider')){
                $file = $request->file('product_image_provider');
                $name = time().$file->getClientOriginalName();
                $path = $file->move(public_path().'/images/products_provider/', $name);
                $data['product_image_provider'] = Util::normalizePath($path);;
            };

            $supplie_provider = SupplieProvider::create($data);

            UtilHelper::UserAction(\request()->user()->id, 'Creó una devolución con número de remisión '.$supplie_provider->referral_number);

            DB::commit();
            return $this->showMessage('Devolución creada correctamente');

        }catch(NonExistentException $e){
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $supplie_provider = $this->supplie_provider->findOrFail($id);
            return $this->successResponse(new ResourcesSupplieProvider($supplie_provider));
        }catch (ModelNotFoundException $e) {
            return $this->errorResponse('Devolución no encontrada', 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            DB::beginTransaction();

            $data = $request->all();
            $supplie_provider = $this->supplie_provider->findOrFail($id);
            $supplie_provider->update($data);
            UtilHelper::UserAction(\request()->user()->id, 'Actualizó la devolución con id '.$supplie_provider->id);

            DB::commit();
            return $this->showMessage('Devolución actualizada correctamente');
        }catch (ModelNotFoundException $e) {
            DB::rollBack();
            return $this->errorResponse('Devolución no encontrada', 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::beginTransaction();
            $supplie_provider = $this->supplie_provider->findOrFail($id);
            $supplie_provider->delete();
            UtilHelper::UserAction(\request()->user()->id, 'Eliminó la devolución con id '.$supplie_provider->id);

            DB::commit();
            return $this->showMessage('Devolución eliminada correctamente');
        }catch (ModelNotFoundException $e) {
            DB::rollBack();
            return $this->errorResponse('Devolución no encontrada', 404);
        }
    }
}
