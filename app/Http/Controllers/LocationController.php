<?php

namespace App\Http\Controllers;

use App\Http\Resources\LocationCollection;
use App\Models\City;
use App\Models\Country;
use App\Models\Department;
use App\Models\Zone;
use Illuminate\Http\Request;

class LocationController extends ApiController
{
    protected $country;
    protected $department;
    protected $city;
    protected $zone;

    public function __construct(Country $country, Department $department, City $city, Zone $zone)
    {
        $this->country          = $country;
        $this->department       = $department;
        $this->city             = $city;
        $this->zone             = $zone;
    }

    public function indexCountry(){
        return $this->successResponse(new LocationCollection($this->country::active()->get()));
    }

    public function indexDepartment(){
        return $this->successResponse(new LocationCollection($this->department::active()->get()));
    }
    public function indexDepartmentAsCountry($id){
        return $this->successResponse(new LocationCollection($this->department::active()->country($id)->get()));
    }

    public function indexCity(){
        return $this->successResponse(new LocationCollection($this->city::active()->get()));
    }

    public function indexCityAsDepartment($id){
        return $this->successResponse(new LocationCollection($this->city::active()->department($id)->get()));
    }

    public function indexCityAsZone($id){
        return $this->successResponse(new LocationCollection($this->city::active()->byZone($id)->get()));
    }

    public function indexZone($id){
        return $this->successResponse($this->zone::active()->byCountry($id)->get());
    }
}
