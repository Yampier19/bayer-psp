<?php

namespace App\Http\Controllers;

use App\Http\Helpers\UtilHelper;
use App\Http\Requests\PatientRequest;
use App\Http\Resources\Patient as ResourcesPatient;
use App\Http\Resources\PatientCollection;
use App\Models\Patient;
use App\Models\PatientPhone;
use App\Models\Product;
use App\Models\Treatment;
use App\Models\InformationApplication;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Util;

class PatientController extends ApiController
{
    protected $patient;
    protected $treatment;
    protected $patienPhone;
    protected $informationApplication;

    public function __construct(Patient $patient, Treatment $treatment, PatientPhone $patienPhone, InformationApplication $informationApplication)
    {
        $this->patient          = $patient;
        $this->treatment        = $treatment;
        $this->patienPhone      = $patienPhone;
        $this->informationApplication = $informationApplication;
    }

    public function index()
    {
        return $this->successResponse(new PatientCollection($this->patient::active()->get()));

        // $country            = request()->header('country');
        // $application_system = request()->header('application_system');
        // return $this->successResponse(new PatientCollection($this->patient::active()->handleCity($country)->getSystemApp($application_system)->get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data                                       = $request->patient;
        $data['user_id']                            = \request()->user()->id;
        $data['consecutive_application_system']     = UtilHelper::ConsecutiveCodePatientSystemAplication($data['application_system_id']);
        $handle                                     = UtilHelper::handleDataPersonal($data['email'], $data['document_number'], $data['application_system_id'], $data['phones']);

        try {
            DB::beginTransaction();

            if ($handle['sw']) {//validar si el paciente existe
                /*if($data['file']){
                    $file = $data['file'];
                    $name = time().$file->getClientOriginalName();
                    $path = $file->move(public_path().'/files/patients/', $name);
                    $data['image'] = Util::normalizePath($path);
                };*/

                $patient = $this->patient::create($data);
                if (isset($data['phones'])) {
                    $this->phonePatient($data['phones'], $patient->id);
                }
                UtilHelper::UserAction(\request()->user()->id, 'Ha creado al paciente '.$patient->name.' '.$patient->last_name);

                $treatment = $request->treatment;

                if($treatment['file']){
                    $file = $treatment['file'];
                    $name = time().$file->getClientOriginalName();
                    $path = $file->move(public_path().'/files/treatments/', $name);
                    $treatment['file'] = Util::normalizePath($path);
                };
                $this->treatmentStore($treatment, $patient);

                DB::commit();
                return $this->showMessage('Paciente creado correctamente');
            }else{
                $message = $handle['message'];
                return $this->showMessage($message, 500, false);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    // crear tratamiento
    function treatmentStore($data, $patient){
        $data['patient_id']                 = $patient->id;
        $data['user_id']                    = \request()->user()->id;
        $data['consecutive_product_id']     = UtilHelper::ConsecutiveCodeTreatment($data['product_id']);
        $product                            = Product::find($data['product_id']);

        if ($product->name == 'Xofigo 1x6 ml CO') {
            $data['code_xofigo']            = UtilHelper::handleMedicineConsecutive('Xofigo 1x6 ml CO');
        }
        $information_applications = isset($data["information_applications"]) ? $data["information_applications"] : [];
        $treatment = $this->treatment::create($data);

        if($treatment){
            $this->applicationInformationStore($treatment->id, $information_applications);
        }

        $data['treatment_id']               = $treatment->id;
        $claimHistory                       = UtilHelper::ClaimHistoryStore($data);

        UtilHelper::UserAction(\request()->user()->id, 'Ha creado el tratamiento '.$treatment->product->name.' para el paciente '.$patient->name.' '.$patient->last_name);
    }

    public function applicationInformationStore($treatment_id, $information_applications){
        foreach($information_applications as $information_application){
            $this->informationApplication::create($information_application);
        }
    }

    // crear telefono para pacientes
    function phonePatient($data, $patient_id, $id =  null){
        foreach ($data as $value) {
            $this->patienPhone::updateOrCreate([
                'id'  => $id,
            ],[
                'number'        => $value,
                'patient_id'    => $patient_id,
            ]);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        return $this->successResponse(new ResourcesPatient($patient));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    private function handleData($data, string $string){
        if (isset($data[$string])) {
            return $data[$string];
        }
        return null;
    }

    public function update(Request $request, Patient $patient)
    {
        $data   = $request->patient;
        $handle = UtilHelper::handleDataPersonalEdit($this->handleData($data, 'email'), $this->handleData($data, 'document_number'), $data['application_system_id'], $this->handleData($data, 'phones'), $patient->id);
        try {
            DB::beginTransaction();

            if ($handle['sw']) {//validar si el paciente existe
                $patient->update($data);

                if (isset($request->patient['phones'])) {
                    $this->phonePatient($request->patient['phones'], $patient->id);
                }
                UtilHelper::UserAction(\request()->user()->id, 'Ha actualizado al paciente '.$patient->name);
                DB::commit();
                return $this->showMessage('Paciente actualizado correctamente');
            }else{
                $message = $handle['message'];
                return $this->showMessage($message, 500, false);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    // buscador de paciente por cedula
    public function handleDocument($document)
    {
        $country        = request()->header('country');
        $arrayPatients  =  [];
        $patients       = $this->patient::active()->handleDocument($document)->get();
        foreach ($patients as $patient) {
            $countryPatient = $patient->city->department->country_id;
            if ($country == $countryPatient ) {
                array_push($arrayPatients, new ResourcesPatient($patient));
            }
        }
        if (count($arrayPatients) > 0) {
            return $this->successResponse($arrayPatients);
        }
        return $this->successResponse([], 'no existe ningún paciente con ese documento');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
