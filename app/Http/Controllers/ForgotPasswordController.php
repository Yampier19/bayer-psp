<?php

namespace App\Http\Controllers;

use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\ForgotPasswordResetRequest;
use App\Mail\PasswordReset;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends ApiController
{
    public function forgot(ForgotPasswordRequest $request) {
        $code = rand(10000,500000);
        $user = User::EmmailUser($request->email)->first();
        $user->update(['code' => $code]);
        Mail::to($request->email)->send(new PasswordReset($code, $user));
        return $this->showMessage('Se envió un codigo a su correo para resetear la contraseña');
    }

    //verificar codigo
    public function checkCode(ForgotPasswordRequest $request){
        $user = User::EmmailUser($request->email)->first();
        if ($user->code == $request->code) {
            return $this->showMessage('Codigo correcto');
        }
        return $this->errorResponse('Codigo proporcionado no válido', 400);
    }

    // cambiar contraseña
    public function reset(ForgotPasswordResetRequest $request) {
        $user = User::EmmailUser($request->email)->first();
        if ($user->code == $request->code) {//validamos nuevamente el codigo
            $user->update(['password' => Hash::make($request->password)]);
            return $this->showMessage('La contraseña ha sido cambiada correctamente');
        }
        return $this->errorResponse('Codigo proporcionado no válido', 400);
    }
}
