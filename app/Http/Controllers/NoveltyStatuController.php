<?php

namespace App\Http\Controllers;

use App\Http\Resources\NoveltyStatuCollection;
use App\Models\NoveltyStatu;
use Illuminate\Http\Request;

class NoveltyStatuController extends ApiController
{
    protected $noveltyStatu;

    public function __construct(NoveltyStatu $noveltyStatu)
    {
        $this->noveltyStatu = $noveltyStatu;
    }

    public function index()
    {
        return $this->successResponse(new NoveltyStatuCollection($this->noveltyStatu::active()->get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
