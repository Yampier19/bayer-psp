<?php

namespace App\Http\Controllers;

use App\Http\Resources\LogisticOperatorCollection;
use App\Models\LogisticOperator;
use Illuminate\Http\Request;

class LogisticOperatorController extends ApiController
{
    protected $operator;

    public function __construct(LogisticOperator $operator)
    {
        $this->operator = $operator;
    }

    public function index()
    {
        return $this->successResponse(new LogisticOperatorCollection($this->operator::active()->get()));
    }


    public function handleIpsInsurance($id)
    {
        return $this->successResponse(new LogisticOperatorCollection($this->operator::active()->HandleInsurance($id)->get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
