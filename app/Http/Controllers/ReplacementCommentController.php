<?php

namespace App\Http\Controllers;

use App\Http\Helpers\UtilHelper;
use App\Http\Requests\ReplacementCommentRequest;
use App\Http\Resources\ReplacementComment as ResourcesReplacementComment;
use App\Http\Resources\ReplacementCommentCollection;
use App\Models\Replacement;
use App\Models\ReplacementComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReplacementCommentController extends ApiController
{
    protected $replacementComment;

    public function __construct(ReplacementComment $replacementComment)
    {
        $this->replacementComment = $replacementComment;
    }

    public function index()
    {
        return $this->successResponse(new ReplacementCommentCollection($this->replacementComment::all()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReplacementCommentRequest $request)
    {
        $data           = $request->all();
        $replacement    = Replacement::find($data['replacement_id']);
        try {
            DB::beginTransaction();

            $replacementComment = $this->replacementComment::create($data);

            UtilHelper::UserAction(\request()->user()->id, 'Ha agregado un comentario a la reposición con codigo CRS '.$replacement->crs_code.' y producto '.$replacement->product->name);

            DB::commit();
            return $this->showMessage('Comentario agregado correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($replacement)
    {
        // return $replacement;
        return $this->successResponse(new ReplacementCommentCollection($this->replacementComment::handleReplacement($replacement)->get()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $replacementComment = $this->replacementComment::find($id);
        $data               = $request->all();
        $replacement        = Replacement::find($data['replacement_id']);
        try {
            DB::beginTransaction();

            $replacementComment->update($data);

            UtilHelper::UserAction(\request()->user()->id, 'Ha actualizado un comentario a la reposición con codigo CRS '.$replacement->crs_code.' y producto '.$replacement->product->name);

            DB::commit();
            return $this->showMessage('Comentario actualizado correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $replacementComment =  $this->replacementComment::find($id);
        $replacementComment->delete();
        UtilHelper::UserAction(\request()->user()->id, 'Ha eliminado un comentario a la reposición con codigo CRS '.$replacementComment->replacement->crs_code.' y producto '.$replacementComment->replacement->product->name);
        return $this->showMessage('Comentario eliminado correctamente');

    }
}
