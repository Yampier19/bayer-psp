<?php

namespace App\Http\Controllers;

use App\Http\Helpers\UtilHelper;
use App\Http\Requests\NoveltyRequest;
use App\Http\Resources\Novelty as ResourceNovelty;
use App\Http\Resources\NoveltyCollection;
use App\Http\Resources\NoveltyCommentCollection;
use App\Http\Resources\NoveltyHistoryCollection;
use App\Models\Noveltie;
use App\Models\NoveltyComment;
use App\Models\NoveltyCommentPhoto;
use App\Models\NoveltyCommentResponse;
use App\Models\NoveltyHistory;
use App\Models\NoveltyStatu;
use App\Models\User;
use League\Flysystem\Util;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NoveltyController extends ApiController
{
    protected $novelty;
    protected $noveltyHisroty;
    protected $noveltyStatu;
    protected $noveltyComment;
    protected $noveltyCommentResponse;
    protected $user;

    public function __construct(Noveltie $novelty, NoveltyHistory $noveltyHisroty, NoveltyStatu $noveltyStatu, NoveltyComment $noveltyComment, NoveltyCommentResponse $noveltyCommentResponse, User $user)
    {
        $this->novelty                  = $novelty;
        $this->noveltyHisroty           = $noveltyHisroty;
        $this->noveltyStatu             = $noveltyStatu;
        $this->noveltyComment           = $noveltyComment;
        $this->noveltyCommentResponse   = $noveltyCommentResponse;
        $this->user                     = $user;
    }

    public function index()
    {
        return $this->successResponse(new NoveltyCollection($this->novelty::active()->get()));
    }


    public function store(NoveltyRequest $request)
    {
        // return $request;
        $data               = $request->novelty;
        $data['user_id']    = \request()->user()->id;
        $novelty            = $this->novelty::create($data);
        // guardar historial de novedad
        $this->historyNovelty('Ha creado la novedad', $novelty->id);
        // guardar historial de usuario
        UtilHelper::UserAction(\request()->user()->id, 'Ha creado la novedad '.$novelty->novelty.' con asunto '.$novelty->affair);
        return $this->showMessage('Novedad creada correctamente');
    }

    function historyNovelty($description, $id){
        $this->noveltyHisroty::create([
            'description'   => $description,
            'user_id'       => \request()->user()->id,
            'novelty_id'    => $id,
        ]);
    }

    public function show(Noveltie $novelty)
    {
        return $this->successResponse(new ResourceNovelty($novelty));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Noveltie $novelty)
    {
        $data       = [];
        $statu      = $this->noveltyStatu::find($request->status_id);
        $manager    = $this->user::find($request->manager_id);
        if ($request->has('status_id')) {
            $data['novelty_statu_id']  = $statu->id;
            // guardar historial de novedad
            $this->historyNovelty('Le ha cambiado el estado a '.$statu->name, $novelty->id);
        }
        if ($request->has('manager_id')) {
            $data['manager_id'] = $manager->id;
            $this->historyNovelty('Le ha reasignado la novedad a '.$manager->name, $novelty->id);
        }
        if ($request->has('answer_date')) {
            $data['answer_date'] = $request->answer_date;
            $this->historyNovelty('Le ha cambiado la fecha de respuestaa a la novedad', $novelty->id);
        }
        if ($request->has('observation')) {
            $data['observation'] = $request->observation;
            $this->historyNovelty('Le ha cambiado la observación a la novedad', $novelty->id);
        }
        if (!empty($data) ) {
            $novelty->update($data);
            // guardar historial de usuario
            UtilHelper::UserAction(\request()->user()->id, 'Ha actulizado la novedad '.$novelty->novelty);
            return $this->showMessage('La novedad se ha actulizado correctamente');
        }else{
            return $this->showMessage('No hay nada que actulizar');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // historial de novedades
    public function noveltyHistoryShow($id){
        return $this->successResponse(new NoveltyHistoryCollection($this->noveltyHisroty::hisroty($id)->get()));
    }

    // comentarios de novedad
    public function noveltyCommentShow($id){
        return $this->successResponse(new NoveltyCommentCollection($this->noveltyComment::noveltyShow($id)->get()));
    }

    // guardar comentarios de una novedad
    public function noveltyCommentStore(Request $request, Noveltie $novelty){
        $comment = $this->noveltyComment::create([
            'description'   => $request->comment,
            'user_id'       => \request()->user()->id,
            'novelty_id'    => $novelty->id,
        ]);
        // guardar los archivos
        $i = 1;
        if (isset($request->files)) {
            $files = $request->files;
            foreach ($files as $value) {
                $name = time() . '_' . $value->getClientOriginalName();
                $path = $value->move(public_path().'/imganes/novelty/comments', $name);
                NoveltyCommentPhoto::create([
                    'file'                  => Util::normalizePath($path),
                    'novelty_comment_id'    => $comment->id
                ]);
            }
        }
        $this->historyNovelty('Ha añadido un comentario a la novedad', $novelty->id);
        UtilHelper::UserAction(\request()->user()->id, 'Ha añadido un comentario a la novedad '.$novelty->novelty);
        return $this->showMessage('El comentario de la novedad ha sido creado correctamente');
    }

    public function commentResponse(Request $request, Noveltie $novelty){
        $data               = $request->all();
        $data['user_id']    = \request()->user()->id;

        try {
            DB::beginTransaction();

            $commentResponse = $this->noveltyCommentResponse::create($data);

            $this->historyNovelty('Ha respondido un commentario', $novelty->id);
            UtilHelper::UserAction(\request()->user()->id, 'Ha respondido un comentario a la novedad '.$novelty->novelty);

            DB::commit();
            return $this->showMessage('Respuesta creada correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }
}
