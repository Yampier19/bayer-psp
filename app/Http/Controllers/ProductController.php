<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductCollection;
use App\Http\Resources\Product as ResourcesProduct;
use App\Models\Product;
use App\Models\ProductDose;
use Illuminate\Http\Request;
use App\Exceptions\NonExistentException;

class ProductController extends ApiController
{
    protected $product;
    protected $productDose;

    public function __construct(Product $product, ProductDose $productDose)
    {
        $this->product      = $product;
        $this->productDose  = $productDose;
    }

    public function index()
    {
        $country = request()->header('country');
        return $this->successResponse(new ProductCollection($this->product::active()->handleCountry($country)->get()));
    }

    public function indexDoses($id)
    {
        return $this->successResponse($this->productDose::active()->handleProduct($id)->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $product = $this->product::where('id', $id)->first();
            if(!$product) throw new NonExistentException('Producto no encontrado');

            return $this->successResponse(new ResourcesProduct($product));
        }catch(NonExistentException $e){
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    public function nameShow($id)
    {
        try{
            $product = $this->product::handleName($id)->first();
            if(!$product) throw new NonExistentException('Producto no encontrado');

            return $this->successResponse($product);
        }catch(NonExistentException $e){
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
