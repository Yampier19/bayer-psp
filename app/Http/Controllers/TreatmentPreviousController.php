<?php

namespace App\Http\Controllers;

use App\Http\Resources\UtilCollection;
use App\Models\TreatmentPrevious;
use Illuminate\Http\Request;

class TreatmentPreviousController extends ApiController
{
    protected $treatmentPreviou;

    public function __construct(TreatmentPrevious $treatmentPreviou)
    {
        $this->treatmentPreviou = $treatmentPreviou;
    }

    public function index()
    {
        return $this->successResponse(new UtilCollection($this->treatmentPreviou::active()->get()));
    }
}
