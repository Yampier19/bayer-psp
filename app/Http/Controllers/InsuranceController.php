<?php

namespace App\Http\Controllers;

use App\Http\Resources\InsuranceCollection;
use App\Models\Insurance;
use App\Models\LogisticOperator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InsuranceController extends ApiController
{
    protected $insurance;

    public function __construct(Insurance $insurance)
    {
        $this->insurance   = $insurance;
    }

    public function index($department)
    {
        return $this->successResponse(new InsuranceCollection($this->insurance::active()->handleDepartment($department)->get()));
    }

    public function store(Request $request)
    {
        $data = [];
        try {
            DB::beginTransaction();
            $data['name']           = $request->insurance;
            $data['department_id']  = $request->department_id;
            $insurance              = $this->insurance::create($data);
            $operator               = LogisticOperator::create(['name' => $request->operator, 'insurance_id' => $insurance->id]);

            DB::commit();
            return $this->showMessage('Eps creada correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }
}
