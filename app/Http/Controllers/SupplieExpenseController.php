<?php

namespace App\Http\Controllers;

use App\Http\Helpers\UtilHelper;
use App\Models\SupplieExpense;
use App\Models\SupplieIncome;
use App\Models\Supplie;
use App\Http\Resources\SupplieExpense as ResourcesSupplieExpense;
use App\Http\Resources\SupplieExpenseCollection;
use Illuminate\Http\Request;
use App\Http\Requests\SupplieExpenseRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\NonExistentException;


class SupplieExpenseController extends ApiController
{
    protected $supplie_expense;
    protected $supplie_income;
    protected $supplie;

    public function __construct(SupplieExpense $supplie_expense, SupplieIncome $supplie_income, Supplie $supplie)
    {
        $this->supplie_expense = $supplie_expense;
        $this->supplie_income = $supplie_income;
        $this->supplie = $supplie;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->successResponse(new SupplieExpenseCollection($this->supplie_expense->get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SupplieExpenseRequest $request)
    {
        try{
            $data = $request->all();
            $result = UtilHelper::EgressStore($data);
            return $this->showMessage($result);
        }catch (NonExistentException $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $supplie_expense = $this->supplie_expense->findOrFail($id);
            return $this->successResponse(new ResourcesSupplieExpense($supplie_expense));
        }catch (ModelNotFoundException $e) {
            return $this->errorResponse('Egreso no encontrado', 404);
        }
    }

    public function medicineWithoutCommercialValueShow(){
        try{
            $supplies = $this->supplie::medicineWithoutCommercialValue()->get();
            if(count($supplies) == 0) throw new NonExistentException("No se encontraron egresos con medicina sin valor comercial");

            $data = collect();

            foreach($supplies as $supplie){
                foreach($supplie->supplieIncomes as $supplie_income){
                    $data = $data->merge($supplie_income->supplieExpenses->where('patient_id', \request()->user()->id));
                }
            }

            return $this->successResponse(new SupplieExpenseCollection($data));
        }catch (NonExistentException $e) {
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    public function orderShow(){

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            DB::beginTransaction();

            $data = $request->all();
            $supplie_expense = $this->supplie_expense->findOrFail($id);
            $supplie_expense->update($data);
            UtilHelper::UserAction(\request()->user()->id, 'Actualizó el egreso con id '.$supplie_expense->id);

            DB::commit();
            return $this->showMessage('Egreso actualizado correctamente');
        }catch (ModelNotFoundException $e) {
            DB::rollBack();
            return $this->errorResponse('Egreso no encontrado', 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::beginTransaction();
            $supplie_expense = $this->supplie_expense->findOrFail($id);
            $supplie_expense->delete();
            UtilHelper::UserAction(\request()->user()->id, 'Eliminó el egreso con id '.$supplie_expense->id);

            DB::commit();
            return $this->showMessage('Egreso eliminado correctamente');
        }catch (ModelNotFoundException $e) {
            DB::rollBack();
            return $this->errorResponse('Egreso no encontrado', 404);
        }
    }
}
