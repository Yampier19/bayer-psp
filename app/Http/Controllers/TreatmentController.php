<?php

namespace App\Http\Controllers;

use App\Http\Helpers\UtilHelper;
use App\Http\Requests\TreamentFollowComunicationRequest;
use App\Http\Requests\TreamentFollowRequest;
use App\Http\Requests\TreamentRequest;
use App\Http\Resources\Treatment As ResourceTreatment;
use App\Http\Resources\TreatmentCollection;
use App\Models\Patient;
use App\Models\Treatment;
use App\Models\TreatmentFollow;
use App\Models\TreatmentFollowCommunication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Util;
class TreatmentController extends ApiController
{
    protected $treatment;
    protected $treatmentFollow;
    protected $treatmentFollowCommunication;
    protected $patient;

    public function __construct(Treatment $treatment, Patient $patient, TreatmentFollow $treatmentFollow, TreatmentFollowCommunication $treatmentFollowCommunication)
    {
        $this->treatment                       = $treatment;
        $this->treatmentFollow                 = $treatmentFollow;
        $this->treatmentFollowCommunication     = $treatmentFollowCommunication;
        $this->patient                          = $patient;
    }

    public function index()
    {
        return $this->successResponse(new TreatmentCollection($this->treatment::active()->get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TreamentRequest $request)
    {
        $data                               = $request->treatment;
        $data['user_id']                    = \request()->user()->id;
        $data['consecutive_product_id']     = UtilHelper::ConsecutiveCodeTreatment($data['product_id']);
        $patient                            = $this->patient::find($data['patient_id']);
        $egress                             = [];
        try {
            DB::beginTransaction();

            if(!UtilHelper::StatuPatientTreatment($data['patient_id'], $data['product_id'])){
                
                if($request->hasFile('file')){
                    $file = $request->file('file');
                    $url = "/files/treatment/";                
                    $data['file'] = UtilHelper::uploadFile($file, $url);
                };

                $treatment              = $this->treatment::create($data);
                $data['treatment_id']   = $treatment->id;
                $claimHistory           = UtilHelper::ClaimHistoryStore($data);

                if (isset($data['product_send'])) {
                    foreach ($data['product_send'] as $value) {
                        $egress['referral_number']                      = $value['referral_number'];
                        $egress['patient_id']                           = $data['patient_id'];
                        $egress['quantity']                             = 1;
                        $egress['supplie_expenses_date']                = $value['supplie_expenses_date'];
                        $egress['from_follow']                          = "SI";
                        UtilHelper::EgressStore($egress);
                    }
                }

                UtilHelper::UserAction(\request()->user()->id, 'Ha creado el tratamiento '.$treatment->product->name.' para el paciente '.$patient->name.' '.$patient->last_name);
                DB::commit();
                return $this->showMessage('Tratamiento creado correctamente');
            }
            return  $this->showMessage('El usuario tiene un tratamiento en curso con este producto', 200, false);

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    // crear siguimeinto de tratamiento
    public function treatmentFollows(TreamentFollowRequest $request, Treatment $treatment){

        $data                    = $request->treatmentFollow;
        $data['treatment_id']   = $treatment->id;

        try {
            DB::beginTransaction();

            $treatmentFollow = $this->treatmentFollow::create($data);
            UtilHelper::UserAction(\request()->user()->id, 'Ha creado un seguimiento para el tratamiento '.$treatment->product->name);

            DB::commit();
            return $this->showMessage('Siguimiento de tratamiento creado correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    // crear siguimeinto de tratamiento de comunicaciones
    public function treatmentFollowCommunication(TreamentFollowComunicationRequest $request, Treatment $treatment){

        $data                    = $request->all();
        $data['treatment_id']   = $treatment->id;

        try {
            DB::beginTransaction();

            $treatmentFollowCommunication = $this->treatmentFollowCommunication::create($data);

            if($request->hasFile("image")){
                $file = $request->file("image");
                $name = time().$file->getClientOriginalName();
                $path = $file->move(public_path().'/images/treatment/', $name);
                $treatmentFollowCommunication->image = Util::normalizePath($path);
                $treatmentFollowCommunication->save();
            }

            UtilHelper::UserAction(\request()->user()->id, 'Ha creado una comunicación para el tratamiento '.$treatment->product->name);

            DB::commit();
            return $this->showMessage('Comunicación de tratamiento creada correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Treatment $treatment)
    {
        return $this->successResponse(new ResourceTreatment($treatment));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Treatment $treatment)
    {
        $data       =  $request->treatment;
        $patient    = $this->patient::find($data['patient_id']);
        try {
            DB::beginTransaction();

            if($request->hasFile('file')){

                $path = public_path($treatment->file);
                if(file_exists($path)){
                    unlink($path);
                }

                $file = $request->file('file');
                $url = "/files/treatment/";                
                $data['file'] = UtilHelper::uploadFile($file, $url);
            };

            $treatment->update($data);
            if (isset($data['claim'])) {
                $data['treatment_id']   = $treatment->id;
                $claimHistory           = UtilHelper::ClaimHistoryStore($data);
            }
            UtilHelper::UserAction(\request()->user()->id, 'Ha actualizado el tratamiento '.$treatment->product->name.' para el paciente '.$patient->name.' '.$patient->last_name);

            DB::commit();
            return $this->showMessage('Tratamiento actualizado correctamente');

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
