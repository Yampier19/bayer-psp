<?php

namespace App\Http\Controllers;

use App\Http\Helpers\UtilHelper;
use App\Http\Requests\ClaimHistoryRequest;
use App\Http\Resources\ClaimHistory as ClaimHistoryResource;
use App\Http\Resources\ClaimHistoryCollection;
use App\Models\ClaimsHistory;
use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \App\Exceptions\NonExistentException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ClaimHistoryController extends ApiController
{
    protected $claim;
    protected $patient;

    public function __construct(ClaimsHistory $claim, Patient $patient)
    {
        $this->claim          = $claim;
        $this->patient        = $patient;
    }

    public function index()
    {
        return $this->successResponse(new ClaimHistoryCollection($this->claim::active()->get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($request)
    {
        $data       = $request;
        $patient    = $this->patient::find($data['patient_id']);

        try {
            DB::beginTransaction();

            $claim = $this->claim::create($data);

            UtilHelper::UserAction(\request()->user()->id, 'Agregó un historial de reclamación al paciente '.$patient->name.' '.$patient->last_name);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $this->successResponse(new ClaimHistoryResource($this->claim::find($id)));
        } catch (\Exception $e) {
            return $this->errorResponse('No existe reclamacion con este id', 409);
        }
    }

    public function showTreatment($id)
    {
        try {
            return $this->successResponse(new ClaimHistoryCollection($this->claim::HandleTreatement($id)->get()));
        } catch (\Exception $e) {
            return $this->errorResponse('No existe tratamiento con este id', 409);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
