<?php

namespace App\Http\Controllers;

use App\Http\Helpers\UtilHelper;
use App\Http\Requests\ChangedPassworrequest;
use App\Http\Requests\UserRequest;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserCollection;
use App\Models\Provider;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

 /**
    * @OA\Info(title="API user", version="1.0")
    *
    * @OA\Server(url="http://127.0.0.1:8000/")
    */
class UserController extends ApiController
{
    /**
    * @OA\Get(
    *     path="/api/login",
    *     summary="logearse",
    *     @OA\Response(
    *         response=200,
    *         description="data de usuario logeado"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function login(Request $request)
    {
        $credentials    = request(['user_name', 'password']);
        $user           = User::byUserName($credentials['user_name'])->first();
        if($user){
            $diffInMonths   = $this->handlePasswordDate($user);

            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    $attempUser = $this->handlePassword($user);
                    if ($attempUser > 3) {
                        $this->handleStatus($user);
                        return $this->errorResponse('Su cuenta de usuario se encuentra inactiva, ha superado los 3 intentos. Comuniquese con su administrador', 400);
                    }
                    if ($user->hasRole('Super Admin')) {
                        return $this->errorResponse('Usuario Super Admin. Credenciales Invalidas', 400);
                    }
                    return $this->errorResponse('Credenciales Invalidas, lleva '. $attempUser.' de 3 intentos', 400);
                }
            } catch (JWTException $e) {
                return $this->errorResponse('No encontró ningún token', 500);
            }
            if ($diffInMonths >= 3 && !$user->hasRole('Super Admin') ) {
                return $this->showMessage('Su contraseñna ha caducado, por favor cambiela para poder iniciar sesión, o Comuniquese con su administrador', 400);
            }
            if(\request()->user()->status == 'ACTIVO'){
                return $this->respondWithToken($token);
            }
            return $this->showMessage('Su cuenta de usuario se encuentra inactiva, comuniquese con su administrador', 400);
        }else{
            return $this->showMessage('Credenciales inválidas', 400);
        }
    }

    // sumar el numero de intentos de inicio de sision
    private function handlePassword($user){
        if (!$user->hasRole('Super Admin')) {
            $user->password_attempt = $user->password_attempt + 1;
            $user->save();
        }
        return $user->password_attempt;
    }

    // cambiar de estado
    private function handleStatus($user){
        $user->status = 'DESACTIVADO';
        $user->save();
    }

    // averiguar la diferencia de meses, con respecto al cambio de contraseña
    private function handlePasswordDate($user){
        return Carbon::now()->diffInMonths($user->password_change_date);
    }

    public function logout()
    {
        auth()->logout();
        return $this->showMessage('Sesión cerrada con exito!', 200);
    }

    public function auth()
    {
        if (\request()->user()) {
            return $this->token('Token active');
        }
    }

    public function register(UserRequest $request)
    {
        $role                           = UtilHelper::handleRol($request->user['role']);
        $data                           = $request->user;
        $data['provider_id']            = UtilHelper::handleProvider($data['provider'], $data['country_id']);
        $data['password']               = Hash::make($data['password']);
        $data['password_change_date']   = Carbon::now();
        $user                           = User::create($data)->assignRole($role);//asignar roles
        $user->applicationSystems()->attach($data['system']);//asignar aplicativos
        UtilHelper::UserAction(\request()->user()->id, 'Ha registrado al usuario '.$user->name);
        return $this->successResponse(new UserResource($user), 'Usuario creado con exito!');
    }

    public function update(Request $request, User $user)
    {
        $rules = [
            'name'  => 'min:3|nullable',
            'email' => 'email|unique:users,email,'.$user->id,//Exeptuar de la validación mismo usuario
            'phone' => 'min:5|unique:users,phone,'.$user->id,
        ];
        $this->validate($request, $rules);
        try {
            DB::beginTransaction();
            if ($request->has('name')){
                $user->name = $request->name;
            }
            if ($request->has('type')){
                $user->type = $request->type;
            }
            if ($request->has('reason_inactivity')){
                $user->reason_inactivity = $request->reason_inactivity;
            }
            if ($request->has('phone')){
                $user->phone  = $request->phone ;
            }
            if ($request->has('status')){
                $user->status = $request->status;
            }

            if ($request->has('role')){
                if ($user->roles[0]->id == $request->role['id']) {
                    UtilHelper::handleRolEdit($request->role);
                }else{
                    $user->removeRole($user->getRoleNames()[0]);
                    $role = UtilHelper::handleRol($request->role);
                    $user->assignRole($role);
                }
            }

            if (!$user->isDirty() && !isset($request->role)){
                return $this->errorResponse('Se debe especificar al menos un valor diferente para poder actualizar', 422);
            }

            $user->save();
            UtilHelper::UserAction(\request()->user()->id, 'Ha Actulizado al usuario '.$user->name);

            DB::commit();
            return $this->successResponse(new UserResource($user), 'Usuario actualizado con exito!');

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }

    }

    public function ChangePassword(ChangedPassworrequest $request)
    {
        $user = User::byUserName($request->user_name)->first();
        try {
            if($user->status == 'ACTIVO'){
                if (!\Hash::check($request->current_password, $user->password)) {
                    return $this->showMessage('La contraseñna es incorrecta', 500, false);
                }
                $user->update([
                    'password'              => Hash::make($request->password),
                    'password_change_date'  => Carbon::now(),
                    'password_attempt'      => 0
                ]);

                UtilHelper::UserAction($user->id, 'Ha cambiado su contraseñna');
                return $this->showMessage('Se ha cambiado la contraseña');
            }
            return $this->showMessage('Su cuenta de usuario se encuentra inactiva, comuniquese con su administrador', 400);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    //devuelve todos los usuarios
    public function users()
    {
        // $provider = request()->header('provider');
        // return $this->successResponse(new UserCollection(User::admin()->handleProvider($provider)->get()));
        return $this->successResponse(new UserCollection(User::admin()->get()));
    }

    //devuelve un usuario
    public function show(User $user)
    {
        if (!$user->hasRole('Super Admin')) {
            return $this->successResponse(new UserResource($user));
        }
        return $this->showMessage('No se puede obtener un usuario con este rol', 200);
    }

    protected function respondWithToken($token)
    {
        $expired = auth()->factory()->getTTL() / 60;
        return response()->json([
            'access_token'  => $token,
            'token_type'    => 'Bearer',
            'expires_in'    => $expired. ' hour',
            'user'          => new UserResource(\request()->user())
        ]);
    }

    public function destroy(User $user)
    {
        if ($user->id !=  1) {
            $user->delete();
            UtilHelper::UserAction(\request()->user()->id, 'Ha eliminado al usurio '.$user->name);
            return $this->showMessage('Usuario eliminado con exito!', 200);
        }
        return $this->showMessage('No se puede eliminar este usuario', 200);
    }
}
