<?php

namespace App\Http\Controllers;

use App\Http\Helpers\UtilHelper;
use App\Http\Requests\ReplacementRequest;
use App\Http\Resources\Replacement as ResourcesReplacement;
use App\Http\Resources\ReplacementCollection;
use App\Models\Product;
use App\Models\Replacement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReplacementController extends ApiController
{
    protected $replacement;

    public function __construct(Replacement $replacement)
    {
        $this->replacement = $replacement;
    }

    public function index()
    {
        return $this->successResponse(new ReplacementCollection($this->replacement::all()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReplacementRequest $request)
    {
        // return $country = request()->header('country_id');//pais

        $data       = $request->replacement;
        $product    = Product::find($data['product_id']);
        try {
            DB::beginTransaction();

            $replacement = $this->replacement::create($data);

            UtilHelper::UserAction(\request()->user()->id, 'Ha creado una reposición con codigo CRS '.$replacement->crs_code.', para el producto '.$product->name);

            DB::commit();
            return $this->showMessage('Reposición creada correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Replacement $replacement)
    {
        return $this->successResponse(new ResourcesReplacement($replacement));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Replacement $replacement)
    {
        $data       = $request->replacement;
        $product    = Product::find($data['product_id']);
        try {
            DB::beginTransaction();

            $replacement->update($data);

            UtilHelper::UserAction(\request()->user()->id, 'Ha actualizado una reposición con codigo CRS '.$replacement->crs_code.', para el producto '.$product->name);

            DB::commit();
            return $this->showMessage('Reposición actualizada correctamente');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
