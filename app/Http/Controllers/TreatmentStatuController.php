<?php

namespace App\Http\Controllers;

use App\Http\Resources\UtilCollection;
use App\Models\TreatmentStatu;
use Illuminate\Http\Request;

class TreatmentStatuController extends ApiController
{
    protected $treatmentStatu;

    public function __construct(TreatmentStatu $treatmentStatu)
    {
        $this->treatmentStatu = $treatmentStatu;
    }

    public function index()
    {
        return $this->successResponse(new UtilCollection($this->treatmentStatu::active()->get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->successResponse(new UtilCollection($this->treatmentStatu::active()->handleProduct($id)->get()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
