<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NoveltyComment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'description'           => $this->description,
            'novelty'               => new Novelty($this->novelty),
            'user'                  => $this->user,
            'status'                => $this->status,
            'created_at'            => date_format($this->created_at, 'Y-m-d, h:m'),
            'updated_at'            => date_format($this->updated_at, 'Y-m-d, h:m'),
            'files'                 => $this->__Files($this->noveltyCommentPhotos),
            'responses'             => new NoveltyCommentResponseCollection($this->noveltyCommentResponses),
        ];
    }

    function __Files($data){
        $array = [];
        foreach ($data as $value) {
            array_push($array, [
                'id'        => $value->id,
                'file'      => $value->file,
                'created_at'=> date_format($value->created_at, 'Y-m-d, h:m'),
            ]);
        }
        return $array;
    }
}
