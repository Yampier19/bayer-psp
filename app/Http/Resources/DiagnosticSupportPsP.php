<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DiagnosticSupportPsP extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                                => $this->id,
            'PAP'                               => $this->PAP,
            'product'                           => $this->product,
            'treatment'                         => $this->treatment,
            'quantity_exams'                    => $this->quantity_exams,
            'voucher_number'                    => $this->voucher_number,
            'diagnostic_medical_center'         => $this->diagnostic_medical_center,
            'file'                              => $this->file,
            'management'                        => $this->management,
            'status'                            => $this->status,
            'exams'                             => $this->exams
        ];
    }
}
