<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Replacement extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                        => $this->id,
            'doctor_code'               => $this->doctor_code,
            'crs_code'                  => $this->crs_code,
            'full_name'                 => $this->full_name,
            'address'                   => $this->address,
            'reference_number'          => $this->reference_number,
            'application_date'          => $this->application_date,
            'argus_code'                => $this->argus_code,
            'qualification'             => $this->qualification,
            'replacement_date'          => $this->replacement_date,
            'repositioning_done'        => $this->repositioning_done,
            'repositioning_done_date'   => $this->repositioning_done_date,
            'email'                     => $this->email,
            'adverse_event_date'        => $this->adverse_event_date,
            'done'                      => $this->done,
            'product'                   => new Product($this->product),
            'city'                      => new Location($this->city),
            'user'                      => new User($this->user),
            'status'                    => $this->status,
            'created_at'                => date_format($this->created_at, 'Y-m-d, h:m'),
            'updated_at'                => date_format($this->updated_at, 'Y-m-d, h:m'),
            'comments'                  => new ReplacementCommentCollection($this->replacementComments),
        ];
    }
}
