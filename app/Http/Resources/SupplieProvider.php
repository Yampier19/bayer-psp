<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SupplieProvider extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                                    => $this->id,
            'referral_number'                       => $this->supplie_income ? $this->supplie_income->referral_number : null,
            'reference_number'                      => $this->supplie_income->supplie->reference_number,
            'medicine_without_commercial_value'     => $this->supplie_income->supplie->medicine_without_commercial_value,
            'product'                               => $this->supplie_income->supplie->product,
            'product_doses'                         => $this->supplie_income->supplie->product_doses,
            'patient'                               => $this->patient,
            'quantity'                              => $this->quantity,
            'user'                                  => $this->user,
            'who_return'                            => $this->who_return,
            'city'                                  => $this->supplie_income->supplie->city ? $this->supplie_income->supplie->city->nombre: null,
            'supplie_provider_date'                 => $this->supplie_provider_date,
            'product_image_provider'                => $this->product_image_provider,
            'observaciones'                         => $this->observaciones,
            'status'                                => $this->status,
        ];
    }
}
