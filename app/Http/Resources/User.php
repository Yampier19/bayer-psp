<?php

namespace App\Http\Resources;

use App\Http\Helpers\UtilHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'name'                  => $this->name,
            'user_name'             => $this->user_name,
            'type'                  => $this->type,
            'phone'                 => $this->phone ,
            'provider'              => $this->provider,
            'reason_inactivity'     => $this->reason_inactivity,
            'country'               => $this->country,
            'status'                => $this->status,
            'created_at'            => date_format($this->created_at, 'Y-m-d, h:m'),
            'updated_at'            => date_format($this->updated_at, 'Y-m-d, h:m'),
            'role'                  => $this->__roleName($this->getRoleNames(), $this->roles[0]->id),
            'permissions'           => UtilHelper::__permissions($this->getAllPermissions()),
            'systems'               => $this->__systems($this->applicationSystems),
        ];
    }

    function __systems($data){
        $array = [];
        foreach ($data as $value) {
            array_push($array,[
                'name'          => $value->name,
                'description'   => $value->description,
            ]);
        }
        return $array;
    }

    function __roleName($role, $id){
        $letters = preg_replace('/[^a-zA-Z]/', ' ', $role);
        return [
            "id"    => $id,
            "name"  => $letters,
        ];
    }
}
