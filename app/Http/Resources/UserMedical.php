<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class UserMedical extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'activation_date'       => $this->activation_date,
            'full_name'             => $this->full_name,
            'document_type'         => $this->document_type,
            'document_number '      => $this->document_number ,
            'address'               => $this->address,
            'email'                 => $this->email,
            'gender'                => $this->gender,
            'date_birth'            => $this->date_birth,
            'age'                   => Carbon::parse($this->date_birth)->age,
            'city'                  => new Location($this->city),
            'user_id'               => $this->user_id,
            'user'                  => $this->user->name,
            'status'                => $this->status,
            'created_at'            => date_format($this->created_at, 'Y-m-d, h:m'),
            'updated_at'            => date_format($this->updated_at, 'Y-m-d, h:m'),
        ];
    }
}
