<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TreatmentFollowComunication extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'description'           => $this->description,
            'next_contact'          => $this->next_contact,
            'last_collection'       => $this->last_collection,
            'start_paap'            => $this->start_paap,
            'code_argus'            => $this->code_argus,
            'next_collection'       => $this->next_collection,
            'end_paap'              => $this->end_paap,
            'image'                 => $this->image,
            'treatment'             => $this->treatment->product->name,
            'status'                => $this->status,
            'created_at'            => date_format($this->created_at, 'Y-m-d, h:m'),
            'updated_at'            => date_format($this->created_at, 'Y-m-d, h:m'),
        ];
    }
}
