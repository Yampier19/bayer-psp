<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SupplieIncome extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                                    => $this->id,
            'referral_number'                       => $this->referral_number,
            'reference_number'                      => $this->supplie->reference_number,
            'medicine_without_commercial_value'     => $this->supplie->medicine_without_commercial_value,
            'product'                               => $this->supplie->product,
            'product_image'                         => $this->product_image,
            'product_doses'                         => $this->supplie->product_doses,
            'quantity'                              => $this->quantity,
            'user'                                  => $this->user,
            'city'                                  => $this->supplie->city,
            'address'                               => $this->supplie->address,
            'observations'                          => $this->observations,
            'supplie_incomes_date'                  => $this->supplie_incomes_date,
            'supplier'                              => $this->supplier,
            'status'                                => $this->status,
        ];
    }
}
