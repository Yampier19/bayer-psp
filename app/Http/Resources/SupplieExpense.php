<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SupplieExpense extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                                    => $this->id,
            'referral_number'                       => $this->supplie_income ? $this->supplie_income->referral_number : null,
            'reference_number'                      => $this->supplie_income->supplie->reference_number,
            'medicine_without_commercial_value'     => $this->supplie_income->supplie->medicine_without_commercial_value,
            'product'                               => $this->supplie_income->supplie->product,
            'product_doses'                         => $this->supplie_income->supplie->product_doses,
            'patient'                               => $this->patient,
            'quantity'                              => $this->quantity,
            'user'                                  => $this->user,
            'third_party_for_the_distribution_of_the_product' => $this->third_party_for_the_distribution_of_the_product,
            'supplier_name'                         => $this->supplier_name,
            'city'                                  => $this->supplie_income->supplie->city ? $this->supplie_income->supplie->city->nombre: null,
            'supplie_expenses_date'                 => $this->supplie_expenses_date,
            'product_image'                         => $this->product_image,
            'observaciones'                         => $this->observaciones,
            'status'                                => $this->status,
        ];
    }
}
