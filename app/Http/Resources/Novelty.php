<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Novelty extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'affair'                => $this->affair,
            'novelty'               => $this->novelty,
            'report_date'           => $this->report_date,
            'answer_date'           => $this->answer_date,
            'observation'           => $this->observation,
            'product'               => new Product($this->product),
            'user'                  => $this->user,
            'manager'               => $this->manager ,
            'novelty_statu'         => new NoveltyStatu($this->noveltyStatu),
            'status'                => $this->status,
            'created_at'            => date_format($this->created_at, 'Y-m-d, h:m'),
            'updated_at'            => date_format($this->updated_at, 'Y-m-d, h:m'),
        ];
    }
}
