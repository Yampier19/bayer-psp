<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Doctor extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                   => $this->id,
            'name'                 => $this->name,
            'specialty'            => $this->__specialtys($this->specialtys),
            'user'                 => $this->user->name  ,
            'user_id'              => $this->user_id ,
            'status'               => $this->status,
            'created_at'           => date_format($this->created_at, 'Y-m-d, h:m'),
            'updated_at'           => date_format($this->updated_at, 'Y-m-d, h:m'),
        ];
    }

    function __specialtys($data){
        $array = [];
        foreach ($data as $value) {
            array_push($array,[
                'id'            => $value->id,
                'name'          => $value->name,
                'status'        => $value->status,
                'created_at'    => date_format($value->created_at, 'Y-m-d, h:m'),
                'updated_at'    => date_format($value->updated_at, 'Y-m-d, h:m'),
            ]);
        }
        return $array;
    }
}
