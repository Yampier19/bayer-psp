<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Location extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            $this->mergeWhen($this->name, [
                'name' => $this->name,
            ]),
            $this->mergeWhen($this->country_id, [
                'name'      => $this->nombre,
                'country'   => $this->country_id ? $this->country->name: null,
            ]),
            $this->mergeWhen($this->departamento_id, [
                'name'          => $this->nombre,
                'department'    => $this->departamento_id ? $this->department->nombre: null,
                'country'       => $this->departamento_id ? $this->department->country->name: null,
            ]),
            'status'                => 'ACTIVO',
            'created_at'            => date_format($this->created_at, 'Y-m-d, h:m'),
            'updated_at'            => date_format($this->updated_at, 'Y-m-d, h:m'),
        ];
    }
}
