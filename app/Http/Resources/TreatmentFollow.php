<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TreatmentFollow extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                                => $this->id,
            'initial_visit_schedule'            => $this->initial_visit_schedule,
            'effective_initial_visit'           => $this->effective_initial_visit,
            'claim'                             => $this->claim,
            'medication_date'                   => $this->medication_date,
            'comunication'                      => $this->comunication,
            'reason_communication'              => $this->reason_communication,
            'contact_medium'                    => $this->contact_medium,
            'call_type'                         => $this->call_type,
            'reason_not_communication'          => $this->reason_not_communication,
            'number_attemps'                    => $this->number_attemps,
            'ips'                               => $this->ips->name,
            'treatment'                         => $this->treatment->product->name,
            'status'                            => $this->status,
            'created_at'                        => date_format($this->created_at, 'Y-m-d, h:m'),
            'updated_at'                        => date_format($this->created_at, 'Y-m-d, h:m'),
        ];
    }
}
