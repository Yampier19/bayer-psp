<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Treatment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                            => $this->id,
            'claim'                         => $this->claim,
            $this->mergeWhen($this->clain == "NO", [
                'date_no claim'  => $this->__date_no_claim(),

            ]),
            'claim_date'                    => $this->claim_date,
            'cause_no_claim_id'             => $this->cause_no_claim_id,
            'reason'                        => $this->reason,
            'patient'                       => $this->patient->name.' '.$this->patient->last_name,
            'claim_history'                 => new ClaimHistoryCollection($this->claimsHistorys),
            'education'                     => $this->education,
            'ips'                           => $this->ips,
            'education_date'                => $this->education_date,
            'number_box'                    => $this->number_box,
            'dose'                          => $this->dose,
            'dose_start'                    => $this->dose_start,
            'pathological_classification'   => $this->pathologicalClassification ? $this->pathologicalClassification->name: null,
            'education_theme'               => $this->educationTheme ? new Util($this->educationTheme): null,
            'education_reason'              => $this->educationReason ? new Util($this->educationReason): null,
            'cause_no_claim'                => $this->causeNoClaim ? new Util($this->causeNoClaim):null,
            'treatment_statu'               => $this->treatmentStatu ? new Util($this->treatmentStatu):null,
            'previous_treatment'            => $this->treatmentPrevious ? new Util($this->treatmentPrevious):null ,
            'therapy_start_date'            => $this->therapy_start_date,
            'regime'                        => $this->regime,
            'consent'                       => $this->consent,
            'units'                         => $this->units,
            'last_claim_date'               => $this->last_claim_date,
            'other_operator'                => $this->other_operator,
            'delivery_point'                => $this->delivery_point,
            'means_acquisition'             => $this->means_acquisition,
            'date_next_call'                => $this->date_next_call,
            'paramedic'                     => $this->paramedic,
            'zone'                          => $this->zone,
            'code'                          => $this->code,
            'patient_part_PAAP'             => $this->patient_part_PAAP,
            'add_application_information'   => $this->add_application_information,
            'shipping_type'                 => $this->shipping_type,
            'product'                       => $this->product ? $this->product->name:null,
            'product_id'                    => $this->product_id,
            'consecutive_product'           => $this->consecutive_product_id,
            'insurance'                     => $this->insurance ? new Util($this->insurance):null,
            'logistic_operator'             => $this->logisticOperator ? new Util($this->logisticOperator):null,
            'user'                          => $this->user->name,
            'user_id'                       => $this->user->id,
            'doctor'                        => $this->doctor->name,
            'doctor_id'                     => $this->doctor_id,
            'specialty'                     => $this->specialty->name,
            'specialty_id'                  => $this->specialty_id,
            'patient_statu'                 => $this->patientStatu ? new Util($this->patientStatu):null,
            'city'                          => new Location($this->city),
            'department_id'                 => $this->city->departamento_id,
            'status'                        => $this->status,

            // *************
            'formulation_date'              => $this->formulation_date,
            'active_for_change'             => $this->active_for_change,
            'initial_visit_schedule'        => $this->initial_visit_schedule,
            'effective_initial_visit'       => $this->effective_initial_visit,
            'medication_date'               => $this->medication_date,
            'comunication'                  => $this->comunication,
            'reason_communication'          => $this->reason_communication,
            'reason_not_communication'      => $this->reason_not_communication,
            'number_attemps'                => $this->number_attemps,
            'contact_type'                  => $this->contact_type,
            'contact_medium'                => $this->contact_medium,
            'call_type'                     => $this->call_type,
            'authorization_number'          => $this->authorization_number,
            'difficulty_access'             => $this->difficulty_access,
            'type_difficulty'               => $this->type_difficulty,
            'generate_request'              => $this->generate_request,
            'adverse_event'                 => $this->adverse_event,
            'date_next_call'                => $this->date_next_call,
            'reason_next_call'              => $this->reason_next_call,
            'observation_next_call'         => $this->observation_next_call,
            'consecutive'                   => $this->consecutive,
            'generate_requirement'          => $this->generate_requirement,
            'Shipping_request'              => $this->Shipping_request,
            'communication_description'     => $this->communication_description,
            'note'                          => $this->note,
            'require_support'               => $this->require_support,
            'support_is_provided'           => $this->support_is_provided,
            'number_lots'                   => $this->number_lots,
            'number_tablet'                => $this->number_tablet,
            'transfer_type'                 => $this->transfer_type,
            'doctor_formulator'             => $this->doctor_formulator,
            'product_dose'                  => $this->productDose,

            // ************
            'created_at'                    => date_format($this->created_at, 'Y-m-d, h:m'),
            'updated_at'                    => date_format($this->updated_at, 'Y-m-d, h:m'),
            'patient'                       => new PatientIndex($this->patient),
            // 'treatmentFollows'              => new TreatmentFollowCollection($this->treatmentFollows),
            'treatmentFollowsComunication'  => new TreatmentFollowComunicationCollection($this->treatmentFollowCommunications),
        ];
    }
    function __date_no_claim(){
        return 'Han pasado '.Carbon::now()->diffInDays($this->created_at). ' dias desde '.$this->created_at;
    }
}
