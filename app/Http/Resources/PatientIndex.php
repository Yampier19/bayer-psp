<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PatientIndex extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                                => $this->id,
            'name'                              => $this->name,
            'last_name'                         => $this->last_name,
            'document_type'                     => $this->document_type,
            'document_number'                   => $this->document_number,
            'date_active'                       => $this->date_active,
            'email'                             => $this->email,
            'city'                              => $this->city->nombre,
            'city_id'                           => $this->city->id,
            'department_id'                     => $this->city->departamento_id,
            'country'                           => $this->city->department->country->name,
            'country_id'                        => $this->city->department->country->id,
            'user'                              => $this->user->name,
            'patient_statu'                     => $this->patientStatu->name,
            'patient_statu_id'                  => $this->patient_statu_id,
            'address'                           => $this->address,
            'neighborhood'                      => $this->neighborhood,
            'gender'                            => $this->gender,
            'date_birth'                        => $this->date_birth,
            'age'                               => Carbon::parse($this->date_birth)->age,
            'guardian'                          => $this->guardian,
            'guardian_phone'                    => $this->guardian_phone,
            'note'                              => $this->note,
            'image'                             => $this->image,
            'status'                            => $this->status,
            'change_state_patient'              => $this->change_state_patient,
            'retirement_date'                   => $this->retirement_date,
            'retirement_reason'                 => $this->retirement_reason,
            'retirement_reason_observations'    => $this->retirement_reason_observations,
            'consecutive_application_system'    => $this->consecutive_application_system,
            'system_aplication'                 => $this->applicationSystem->full_name,
            'system_aplication_id'              => $this->applicationSystem->id,
            'created_at'                        => date_format($this->created_at, 'Y-m-d, h:m'),
            'updated_at'                        => date_format($this->updated_at, 'Y-m-d, h:m'),
            'phones'                            => $this->__phones($this->patientPhones),
        ];
    }

    function __phones($data){
        $array = [];
        foreach ($data as $value) {
            array_push($array, [
                'id'            => $value->id,
                'number'        => $value->number,
                'status'        => $value->status,
                'created_at'    => date_format($value->created_at, 'Y-m-d, h:m'),
            ]);
        }
        return $array;
    }

}
