<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ClaimHistory extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                                    => $this->id,
            'claim'                                 => $this->claim,
            $this->mergeWhen($this->clain == "NO", [
                'date_no_claim'  => $this->__date_no_claim(),

            ]),
            'claim_date'                            => $this->claim_date,
            // 'claim_date_month'                      => Carbon::parse($this->claim_date)->toFormattedDateString('m:h'),
            'reason'                                => $this->reason,
            'cause_no_claim'                        => $this->causeNoClaim,
            'treatment_id'                          => $this->treatment_id,
            'status'                                => $this->status,
            'created_at'                            => date_format($this->created_at, 'Y-m-d, h:m'),
            'updated_at'                            => date_format($this->updated_at, 'Y-m-d, h:m'),
        ];
    }

    function __date_no_claim(){
        return 'Han pasado '.Carbon::now()->diffInDays($this->created_at). ' dias desde '.$this->created_at;
    }
}
