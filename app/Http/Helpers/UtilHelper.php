<?php

namespace App\Http\Helpers;

use App\Models\Patient;
use App\Models\Provider;
use App\Models\Treatment;
use App\Models\UserAction as ActionUser;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Models\SupplieExpense;
use App\Models\SupplieIncome;
use App\Models\Supplie;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Util;
use App\Exceptions\NonExistentException;
use App\Models\ClaimsHistory;
use App\Models\Product;

class UtilHelper
{

    const ACTIVE    = 'ACTIVO';
    const DISABLED  = 'DESACTIVADO';

    static function UserAction($id, $action){
        ActionUser::create([
            'action'    => $action,
            'user_id'   => $id,
        ]);
    }

    static function handleProvider($provider, $country_id){
        $handle =  Provider::name($provider)->first();
        if ($handle) {
            return $handle->id;
        }
        return Provider::create([
            'name'          => $provider,
            'country_id'    => $country_id,
        ])->id;

    }

    //averiguar el consecutivo que tiene cada tratamiento por productos
    static function ConsecutiveCodeTreatment($product_id){
        $treatment =  Treatment::productTreatment($product_id)->get()->last();
        if ($treatment) {
            return $treatment->consecutive_product_id + 1;
        }
        return 1;
    }

    //averiguar el consecutivo que tiene cada paciente por systema de aplicativo
    static function ConsecutiveCodePatientSystemAplication($id){
        $patient =  Patient::getSystemApp($id)->get()->last();
        if ($patient) {
            return $patient->consecutive_application_system + 1;
        }
        return 1;
    }

    // averiguar si un paciente tiene un tratamiento activo según el producto
    static function StatuPatientTreatment($patient_id, $product_id){
        $treatments =  Treatment::patientTreatment($patient_id)->productTreatment($product_id)->patientTreatmentStatu()->get();
        // $treatments =  Treatment::patientTreatment($patient_id)->productTreatment($product_id)->active()->get();
        if ($treatments->count()) {
            return true;
        }
        return false;
    }

    static function __permissions($data){
        $array = [];
        foreach ($data as $value) {
            array_push($array, [
                'name'      => $value->name,
                'theme'     => $value->theme_permission ? $value->theme_permission: 'todos los permisos',
                'view'      => $value->pivot->view ? $value->pivot->view: 'SI',
                'edit'      => $value->pivot->edit ? $value->pivot->edit: 'SI',
            ]);
        }
        return $array;
    }

    // validar datos unicos de un paciente por sistema de aplicación
    static function handleDataPersonal($email, $documento_number, $system_app, array $phones){
        $sw         = true;
        $message    = null;
        $patients   = Patient::active()->getSystemApp($system_app)->get();

        foreach ($patients as $patient) {
            if ($patient->document_number == $documento_number) {
                $sw         = false;
                $message    = 'El numero de documento ya se encuentra registrado';
                break;
            }
            if ($patient->email == $email ) {
                $sw         = false;
                $message    = 'El email ya se encuentra registrado';
                break;
            }
            foreach ($phones as $number) {
                foreach ($patient->patientPhones as $phone) {
                    if ($number == $phone->number) {
                        $sw         = false;
                        $message    = 'Uno o más telefonos ya se encuentra registrado';
                        break;
                    }
                }
            }
        }
        return [
            'sw'      => $sw,
            'message' => $message
        ];
    }

    // validar datos unicos de un paciente por sistema de aplicación update
    static function handleDataPersonalEdit($email = null, $documento_number = null, $system_app, $phones = null, $id){
        $sw         = true;
        $message    = null;
        $patients   = Patient::active()->getSystemApp($system_app)->getId($id)->get();

        foreach ($patients as $patient) {
            if ($patient->document_number == $documento_number) {
                $sw         = false;
                $message    = 'El numero de documento ya se encuentra registrado';
                break;
            }
            if ($patient->email == $email ) {
                $sw         = false;
                $message    = 'El email ya se encuentra registrado';
                break;
            }
            if ($phones) {
                foreach ($phones as $number) {
                    foreach ($patient->patientPhones as $phone) {
                        if ($number == $phone->number) {
                            $sw         = false;
                            $message    = 'Uno o más telefonos ya se encuentra registrado';
                            break;
                        }
                    }
                }
            }
        }
        return [
          'sw'      => $sw,
          'message' => $message
        ];
    }

    // validar si el rol con los permisos existen
    static function handleRol($data){
        $sw         = false;
        $role       = Role::find($data['id']);
        $permission = UtilHelper::__permissions($role->getAllPermissions());

        // validar si el rol ya está creado en DB
        foreach ($permission as $key => $value) {
            if ($value != $data['permissions'][$key] ) {
                $sw = true;
                break;
            }
        }

        //si el rol no existe con los mismo permisos, creamos uno nuevo
        if($sw){
            $role = Role::create(['name' => $data['name'].' '.rand(10000,50000000)]);
            foreach ($data['permissions'] as $value) {
                $permission = Permission::where('name', $value['name'])->first();
                if ($permission) {
                    $permission->roles()->attach($role->id, ['view' => $value['view'],  'edit' => $value['edit']]);
                }
            }
        }
        return $role->id;
    }

    // editar el rol de un usuario
    static function handleRolEdit($data){
        $role           = Role::find($data['id']);
        $permissions    = $role->getAllPermissions();

        foreach ($data['permissions'] as $value) {
            foreach($permissions as $permission){
                if ( $value['name'] ==  $permission->name && $value['theme'] == $permission->theme_permission) {
                    $role->permissions()->updateExistingPivot($permission->id, ["view" => $value['view'], "edit" => $value['edit']]);
                }
            }
        }
    }

    static function uploadFile($file, $url){
        $name = time().$file->getClientOriginalName();
        $path = $file->move(public_path().$url, $name);
        $data = Util::normalizePath($path);

        return $data;
    }

    static function EgressStore($request){
        try{
            DB::beginTransaction();
            $data = $request;
            $supplie_income = SupplieIncome::referral($request['referral_number'])->first();

            if(!$supplie_income) throw new NonExistentException("El producto con número de remisión ".$request['referral_number']." no ha sido ingresado aún");

            $supplie = Supplie::findOrFail($supplie_income->supplie_id);
            $available = $supplie->quantity_available;

            if($available < $request['quantity']){
                if($available == 1){
                    return 'Solo tienes '.$available." cantidad disponible";
                }else if($available == 0){
                    return "Ya no tienes cantidades disponibles";
                }else{
                    return 'Solo tienes '.$available." cantidades disponibles";
                }
            }
            $supplie->update([
                "quantity_available" => $available - $request['quantity']
            ]);
            $data["supplie_income_id"] = $supplie_income->id;
            if($request->hasFile('product_image')){
                $file = $request->file('product_image');
                $name = time().$file->getClientOriginalName();
                $path = $file->move(public_path().'/images/products/', $name);
                $data['product_image'] = Util::normalizePath($path);;
            };
            $supplie_expense = SupplieExpense::create($data);

            UtilHelper::UserAction(\request()->user()->id, 'Creó un egreso con número de remisión '.$supplie_expense->referral_number);

            DB::commit();
            return 'Egreso creado correctamente';

        }catch(NonExistentException $e){
            DB::rollBack();
            return $e->getMessage();
        }
    }

    // agregar consecutivo al codigo xofigo
    static function handleMedicineConsecutive($productName){
        $products = Product::handleProduct($productName)->get();
        $array = [];
        foreach ($products as $product) {
            array_push($array, $product->id);
        }

        $treatment = Treatment::handleProduct($array)->get()->last();
        if ($treatment) {
            return $treatment->code_xofigo + 1;
        }
        return 1;
    }

    // crear hostorial de reclamación
    static function ClaimHistoryStore($request)
    {
        $data = $request;
        try {
            DB::beginTransaction();

            $claim = ClaimsHistory::create($data);
            UtilHelper::UserAction(\request()->user()->id, 'Agregó un historial de reclamación al paciente '.$claim->treatment->patient->name.' '.$claim->treatment->patient->last_name);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw new NonExistentException($e->getMessage());
        }
    }

}
