<?php

namespace App\Models;

use App\Http\Helpers\UtilHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Treatment extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'treatments';

    protected $fillable = [
        'claim',
        'claim_date',
        'cause_no_claim_id',
        'reason',
        'education',
        'education_date',
        'number_box',
        'units',
        'dose',
        'dose_start',
        'pathological_classification_id',
        'consent',
        'therapy_start_date',
        'regime',
        'product_id',
        'insurance_id',
        'logistic_operator_id',
        'user_id',
        'patient_id',
        'doctor_id',
        'specialty_id',
        'other_operator',
        'delivery_point',
        'means_acquisition',
        'date_next_call',
        'paramedic',
        'file',
        'zone',
        'code',
        'patient_part_PAAP',
        'add_application_information',
        'shipping_type',
        'ips',
        'city_id',
        'education_theme_id',
        'education_reason_id',
        'consecutive_product_id',
        'treatment_statu_id',
        'patient_statu_id',
        'treatment_previou_id',
        'status',

        // nuevos atributos
        'formulation_date',
        'active_for_change',
        'initial_visit_schedule',
        'effective_initial_visit',
        'medication_date',
        'comunication',
        'reason_communication',
        'reason_not_communication',
        'number_attemps',
        'contact_type',
        'contact_medium',
        'call_type',
        'authorization_number',
        'difficulty_access',
        'type_difficulty',
        'generate_request',
        'adverse_event',
        'type_adverse_event',
        'date_next_call',
        'cause_no_visit',
        'reason_next_call',
        'observation_next_call',
        'consecutive',
        'code_xofigo',
        'generate_requirement',
        'Shipping_request',
        'communication_description',
        'note',
        'number_tablet',
        'number_lots',
        'require_support',
        'support_is_provided',
        'transfer_type',
        'doctor_formulator',
        'product_dose_id',
    ];

    const ACTIVE = 'ACTIVO';

    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function causeNoClaim() {
        return $this->belongsTo(CauseNoClaim::class, 'cause_no_claim_id');
    }

    public function insurance() {
        return $this->belongsTo(Insurance::class, 'insurance_id');
    }

    public function logisticOperator() {
        return $this->belongsTo(LogisticOperator::class, 'logistic_operator_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function patient() {
        return $this->belongsTo(Patient::class, 'patient_id');
    }

    public function doctor() {
        return $this->belongsTo(Doctor::class, 'doctor_id');
    }

    public function treatmentStatu() {
        return $this->belongsTo(TreatmentStatu::class, 'treatment_statu_id');
    }

    public function ips() {
        return $this->belongsTo(Ips::class, 'ips_id');
    }

    public function city() {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function pathologicalClassification() {
        return $this->belongsTo(PathologicalClassification::class, 'pathological_classification_id');
    }

    public function patientStatu() {
        return $this->belongsTo(PatientStatu::class, 'patient_statu_id');
    }

    public function educationTheme() {
        return $this->belongsTo(EducationTheme::class, 'education_theme_id');
    }

    public function educationReason() {
        return $this->belongsTo(EducationReason::class, 'education_reason_id');
    }

    public function treatmentPrevious() {
        return $this->belongsTo(TreatmentPrevious::class, 'treatment_previou_id');
    }

    public function specialty() {
        return $this->belongsTo(Specialty::class, 'specialty_id');
    }

    public function productDose() {
        return $this->belongsTo(ProductDose::class, 'product_dose_id');
    }

    public function doctorFormulator() {
        return $this->belongsTo(Doctor::class, 'doctor_formulator');
    }


    public function treatmentFollows() {
        return $this->hasMany(TreatmentFollow::class);
    }

    public function treatmentFollowCommunications() {
        return $this->hasMany(TreatmentFollowCommunication::class);
    }

    public function claimsHistorys() {
        return $this->hasMany(ClaimsHistory::class);
    }

    public function informationApplications() {
        return $this->hasMany(InformationApplication::class);
    }


    // scope
    public function scopeActive($query){
        return $query->where('status', Treatment::ACTIVE);
    }

    public function scopeProductTreatment($query, $id){
        return $query->where('product_id', $id);
    }

    public function scopePatientTreatment($query, $id){
        return $query->where('patient_id', $id);
    }

    // public function scopePatientTreatmentStatu($query){
    //     return $query->whereIn('status', UtilHelper::ACTIVE);
    // }

    public function scopePatientTreatmentStatu($query){
        return $query->whereIn('patient_statu_id', [PatientStatu::ACTIVO, PatientStatu::EN_SERVICIO, PatientStatu::SECUENCIACION, PatientStatu::PROCESO]);
    }

    public function scopeHandleProduct($query, $array){
        return $query->whereIn('product_id', $array);
    }

}
