<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogisticOperator extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'logistic_operators';

    protected $fillable = [
        'name',
        'insurance_id',
        'status',
    ];

    const NAME = [
        'AUDIFARMA S.A.',
        'DROGUERIAS Y FARMACIAS CRUZ VERDE',
        'ETICOS SERRANO GOMEZ LTDA',
        'COSMITET LTDA CORP. DE SERVICIOS',
        'INSTITUTO NACIONAL DE CANCEROLOGIA',
        'INSTITUTO NACIONAL DE CANCEROLOGIA',
        'ORGANIZACION CLINICA GENERAL DEL NORTE'
    ];

    const ACTIVE = 'ACTIVO';


    public function insurance() {
        return $this->belongsTo(Insurance::class, 'insurance_id');
    }

    public function treatments() {
        return $this->hasMany(Treatment::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', LogisticOperator::ACTIVE);
    }

    public function scopeHandleInsurance($query, $id){
        return $query->where('insurance_id', $id);
    }
}
