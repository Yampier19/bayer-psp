<?php

namespace App\Models;

use App\Http\Helpers\UtilHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Zone extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'zones';

    protected $fillable = [
        'name',
        'country_id',
        'status',
    ];

    const NAME = [
        'Atlántico',
        'Bogotá',
        'Eje cafetero',
    ];

    public function cities() {
        return $this->hasMany(City::class);
    }

    public function country() {
        return $this->belongsTo(Country::class, 'country_id');
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', UtilHelper::ACTIVE);
    }

    public function scopeByCountry($query, $id){
        return $query->where('country_id', $id);
    }
}
