<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Insurance extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'insurances';

    protected $fillable = [
        'name',
        'department_id',
        'status',
    ];

    const NAME = [
        'CAPITAL SALUD EPS-S S.A.S',
        'ALIANSALUD EPS',
        'NUEVA EPS',
        'DIRECCION DE SANIDAD POLICIA NACIONAL',
        'FOPEP',
        'COOSALUD ESS EPS-S',
        'EPS SANITAS',
    ];

    const ACTIVE = 'ACTIVO';


    public function department() {
        return $this->belongsTo(Department::class, 'department_id');
    }

    public function logisticOperator() {
        return $this->hasMany(LogisticOperator::class);
    }

    public function treatments() {
        return $this->hasMany(Treatment::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', Insurance::ACTIVE);
    }

    public function scopeHandleDepartment($query, $id){
        return $query->where('department_id', $id);
    }
}
