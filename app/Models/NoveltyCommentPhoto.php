<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NoveltyCommentPhoto extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'novelty_comment_photos';

    protected $fillable = [
        'file',
        'novelty_comment_id',
    ];

    public function noveltyComment() {
        return $this->belongsTo(NoveltyComment::class, 'novelty_comment_id');
    }
}
