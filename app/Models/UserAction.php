<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAction extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'users_actions';

    protected $fillable = [
        'action',
        'user_id',
        'status',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
