<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Exceptions\NonExistentException;

class Supplie extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'supplies';

    protected $fillable = [
        'id',
        'reference_number',
        'medicine_without_commercial_value',
        'product_id',
        'product_name',
        'product_doses_id',
        'expiration_date',
        'city_id',
        'address',
        'quantity_available',
        'status',
    ];

    public function city() {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function product_doses(){
        return $this->belongsTo(ProductDose::class, 'product_doses_id');
    }

    public function supplieIncomes() {
        return $this->hasMany(SupplieIncome::class, 'supplie_id', 'id');
    }

    public function supplieExpenses() {
        return $this->hasMany(SupplieExpense::class);
    }

    public function supplieProviders() {
        return $this->hasMany(SupplieProvider::class);
    }

    public function scopeExistSupplie($query, $reference_number, $city_id){
        return $query->where('reference_number', $reference_number)
        ->where('city_id', $city_id);
    }

    public function scopeProductAvailable($query, $product_id){
        return $query->where('product_id', $product_id)
        ->where("quantity_available", ">", 0);
    }

    public function scopeMedicineWithoutCommercialValue($query){
        return $query->where('medicine_without_commercial_value', "NO");
    }
    //
    /*public static function existSupplie($reference_number, $city_id){
        $supplie = self::where('reference_number', $reference_number)
        ->where('city_id', $city_id)->first();

        if(!$supplie) throw new NonExistentException("El producto con referencia ".$reference_number." no ha sido ingresado aún");

        return $supplie;
    }*/
}
