<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'departments';

    protected $fillable = [
        'nombre',
        'activo',
        'country_id',
    ];

    const NAME = [
        'Magdalena',
        'Cundinamarca',
        'Cesar',
        'Antioquia',
        'Manizales',
        'Atlantico',
        'Boyaca',
    ];

    const ACTIVE = 1;


    public function country() {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function cities() {
        return $this->hasMany(City::class);
    }

    public function insurances() {
        return $this->hasMany(Insurance::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('activo', Department::ACTIVE);
    }

    public function scopeCountry($query, $id){
        return $query->where('country_id', $id);
    }
}
