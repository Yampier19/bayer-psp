<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExamDiagnosticSupportPsP extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'exams_diagnostic_support_psp';

    protected $fillable = [
        'name',
        'price',
        'order',
        'status',
        'diagnostic_support_id'
    ];

    public function diagnosticSupportPsp(){
        return $this->belongsTo(DiagnosticSupportPsP::class, 'diagnostic_support_id', 'id');
    }
}
