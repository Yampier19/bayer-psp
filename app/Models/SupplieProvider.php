<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupplieProvider extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'supplie_providers';

    protected $fillable = [
        'supplie_income_id',
        'patient_id',
        'quantity',
        'user_id',
        'supplie_provider_date',
        'who_return',
        'reason_for_return',
        'product_image_provider',
        'observations',
        'status',
    ];

    public function patient() {
        return $this->belongsTo(Patient::class, 'patient_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function supplie_income() {
        return $this->belongsTo(SupplieIncome::class, 'supplie_income_id');
    }
}
