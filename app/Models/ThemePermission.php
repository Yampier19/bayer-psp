<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Contracts\Permission;

class ThemePermission extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'theme_permissions';

    protected $fillable = [
        'name',
    ];

    public function permission() {
        return $this->hasMany(Permission::class);
    }
}
