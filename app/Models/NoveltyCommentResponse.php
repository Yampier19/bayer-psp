<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NoveltyCommentResponse extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'novelty_comment_responses';

    protected $fillable = [
        'description',
        'user_id',
        'novelty_comment_id',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }


    public function NoveltyComment() {
        return $this->belongsTo(NoveltyComment::class, 'novelty_comment_id');
    }

    // scope
    public function scopeNoveltyComment($query, $id){
        return $query->where('novelty_comment_id', $id);
    }
}
