<?php

namespace App\Models;

use App\Http\Helpers\UtilHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductDose extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'product_doses';

    protected $fillable = [
        'dose',
        'product_id',
        'status',
    ];

    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', UtilHelper::ACTIVE);
    }

    public function scopeHandleProduct($query, $id){
        return $query->where('product_id', $id);
    }
}
