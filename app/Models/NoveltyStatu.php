<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NoveltyStatu extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'novelty_status';

    protected $fillable = [
        'name',
        'status',
        'colour'
    ];

    const NAME = [
        'iniciada',
        'cancelada',
        'finalizada',
    ];

    const COLOUR = [
        'blue',
        'red',
        'white',
    ];

    const ACTIVE = 'ACTIVO';


    // scope
    public function scopeActive($query){
        return $query->where('status', NoveltyStatu::ACTIVE);
    }
}
