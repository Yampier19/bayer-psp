<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PatientStatu extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'patient_status';

    protected $fillable = [
        'name',
        'status',
    ];

    const ACTIVE        = 'ACTIVO';
    const ACTIVO        = 2;
    const EN_SERVICIO   = 3;
    const SECUENCIACION = 4;
    const PROCESO       = 6;

    const NAME = [
        'Abandono',
        'Activo',
        'En servicio',
        'Secuenciacion',
        'Interrumpido',
        'En proceso',
        'Suspendido',
    ];

    public function patients() {
        return $this->hasMany(Patient::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', PatientStatu::ACTIVE);
    }
}
