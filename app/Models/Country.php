<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'countries';

    protected $fillable = [
        'name',
        'status'
    ];

    const NAME = [
        'COLOMBIA',
        'ECUADOR',
        'PERU',
        'VENEZUELA',
    ];

    const VALUE = [
        'COLOMBIA'  => 1,
        'ECUADOR'   => 2,
        'PERU'      => 3,
    ];

    const ACTIVE = 'ACTIVO';


    public function departments() {
        return $this->hasMany(Department::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', Country::ACTIVE);
    }

}
