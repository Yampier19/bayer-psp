<?php

namespace App\Models;

use App\Http\Helpers\UtilHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TreatmentPrevious extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'treatment_previous';

    protected $fillable = [
        'name',
        'status',
    ];

    const NAME = [
        'Advate',
        'Ambrisentan',
        'Avastin',
        'Avonex',
        'Bosentan',
        'Copaxone',
        'Enoxaparina',
        'Epoprostenol',
        'Gilenya',
        'Lucentis',
        'Natalizumab',
        'No Recuerda Tratamiento Previo',
        'Por Confirmar',
        'Rebif',
        'Sin Tratamiento Previo',
        'Sindenafil',
        'Sutent',
        'Volibris',
        'Warfarina',
        'Xyntha',
        'Otro',
    ];

    public function treatment() {
        return $this->hasMany(Treatment::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', UtilHelper::ACTIVE);
    }
}
