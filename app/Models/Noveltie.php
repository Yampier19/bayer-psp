<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Noveltie extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'novelties';

    protected $fillable = [
        'affair',
        'novelty',
        'report_date',
        'answer_date',
        'observation',
        'product_id',
        'user_id',
        'manager_id',
        'novelty_statu_id',
        'status',
    ];

    const ACTIVE = 'ACTIVO';


    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function manager() {
        return $this->belongsTo(User::class, 'manager_id');
    }

    public function noveltyStatu() {
        return $this->belongsTo(NoveltyStatu::class, 'novelty_statu_id');
    }

    public function noveltyHistories() {
        return $this->hasMany(NoveltyHistory::class);
    }

    public function noveltyComments() {
        return $this->hasMany(NoveltyComment::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', Noveltie::ACTIVE);
    }
}
