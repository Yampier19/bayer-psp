<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NoveltyComment extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'novelty_comments';

    protected $fillable = [
        'description',
        'user_id',
        'novelty_id',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function novelty() {
        return $this->belongsTo(Noveltie::class, 'novelty_id');
    }

    public function noveltyCommentPhotos() {
        return $this->hasMany(NoveltyCommentPhoto::class);
    }

    public function noveltyCommentResponses() {
        return $this->hasMany(NoveltyCommentResponse::class);
    }

    // scope
    public function scopeNoveltyShow($query, $id){
        return $query->where('novelty_id', $id);
    }

}
