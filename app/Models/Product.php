<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'products';

    protected $fillable = [
        'name',
        'reference',
        'reference_number',
        'country_id',
        'status',
    ];

    const ADEMPAS   = 'ADEMPAS';
    const BETAFERON = 'BETAFERON CMBP X 15 VPFS (3750 MCG) MM';
    const EYLIA     = 'Eylia 2MG VL 1x2ML CO INST';
    const NEXAVAR   = 'NEXAVAR 200MGX60C(12000MG)INST';
    const VENTAVIS  = 'VENTAVIS 10 1SOL/2ML X30AMP(Conse) MM';
    const XARELTO   = 'XARELTO 15 MG X 7 TABL MU';
    const NUBEQA    = 'NUBEQA';
    const XOFIGO    = 'Xofigo 1x6 ml CO';

    const ACTIVE = 'ACTIVO';


    public function treatments() {
        return $this->hasMany(Treatment::class);
    }

    public function novelties() {
        return $this->hasMany(Noveltie::class);
    }

    public function country() {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function productDoses() {
        return $this->hasMany(ProductDose::class);
    }

    public function treatmentStatus() {
        return $this->hasMany(TreatmentStatu::class);
    }

    public function replacements() {
        return $this->hasMany(Replacement::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', Product::ACTIVE);
    }

    public function scopeHandleCountry($query, $id){
        return $query->where('country_id', $id);
    }

    public function scopeHandleProduct($query, $product){
        return $query->where('name', $product);
    }

    public function scopeHandleName($query, $id){
        return $query->where('id', $id)
                    ->select('id', 'name');
    }


}
