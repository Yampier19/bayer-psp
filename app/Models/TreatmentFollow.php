<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TreatmentFollow extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'treatment_follows';

    protected $fillable = [
        'initial_visit_schedule',
        'effective_initial_visit',
        'claim',
        'medication_date',
        'comunication',
        'reason_communication',
        'contact_medium',
        'call_type',
        'reason_not_communication',
        'number_attemps',
        'ips_id',
        'treatment_id',
        'status',
    ];

    public function ips() {
        return $this->belongsTo(Ips::class, 'ips_id');
    }

    public function treatment() {
        return $this->belongsTo(Treatment::class, 'treatment_id');
    }

}
