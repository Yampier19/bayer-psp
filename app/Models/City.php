<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'cities';

    protected $fillable = [
        'nombre',
        'prioridad',
        'indicativo',
        'indicativo_internacional',
        'activo',
        'telefonia',
        'departamento_id'
    ];

    const ACTIVE = '1';

    public function department() {
        return $this->belongsTo(Department::class, 'departamento_id');
    }

    public function patients() {
        return $this->hasMany(Patient::class);
    }

    public function supplie() {
        return $this->hasMany(Supplie::class);
    }

    public function replacements() {
        return $this->hasMany(Replacement::class);
    }

    public function UserMedicals() {
        return $this->hasMany(UserMedical::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('activo', City::ACTIVE);
    }

    public function scopeDepartment($query, $id){
        return $query->where('departamento_id', $id);
    }

    public function scopeByZone($query, $id){
        return $query->where('zone_id', $id);
    }
}
