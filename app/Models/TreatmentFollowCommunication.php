<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TreatmentFollowCommunication extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'treatment_follow_communications';

    protected $fillable = [
        'description',
        'next_contact',
        'last_collection',
        'start_paap',
        'code_argus',
        'next_collection',
        'end_paap',
        'image',
        'treatment_id',
        'status',
    ];


    public function treatment() {
        return $this->belongsTo(Treatment::class, 'treatment_id');
    }
}
