<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReplacementComment extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'replacements_comments';

    protected $fillable = [
        'comment',
        'user_id',
        'replacement_id',
        'status',
    ];

    public function replacement() {
        return $this->belongsTo(Replacement::class, 'replacement_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    // scope
    public function scopeHandleReplacement($query, $id){
        return $query->where('replacement_id', $id);
    }

}
