<?php

namespace App\Models;

use App\Http\Helpers\UtilHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provider extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'providers';

    protected $fillable = [
        'name',
        'country_id',
        'status',
    ];

    public function country() {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function User() {
        return $this->hasMany(User::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', UtilHelper::ACTIVE);
    }

    public function scopeName($query, $name){
        return $query->where('name', $name);
    }
}
