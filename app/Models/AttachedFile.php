<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttachedFile extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'attached_files';

    protected $fillable = [
        'claim_request_letter',
        'adverse_event_format',
        'product_sticker',
        'product_delivery_letter',
        'patient_id'
    ];

    public function patient() {
        return $this->belongsTo(Patient::class, 'patient_id');
    }
}
