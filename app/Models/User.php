<?php

namespace App\Models;

use App\Mail\PasswordReset;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_name',
        'type',
        'reason_inactivity',
        'phone',
        'code',
        'password',
        'password_attempt',
        'password_change_date',
        'provider_id',
        'country_id',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    const ROLE_SUPER_ADMIN  = 'Super Admin';
    const ROLE_ADMIN        = 'Admin';

    //JWT
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    // MAIL DE CAMBIAR PASSWORD
    // public function sendPasswordResetNotification($token)
    // {
    //     Mail::to($this->email)->send(new PasswordReset($this, $token));
    // }

    //relaciones
    public function userActions() {
        return $this->hasMany(UserAction::class);
    }

    public function patients() {
        return $this->hasMany(Patient::class);
    }

    public function treatments() {
        return $this->hasMany(Treatment::class);
    }

    public function novelties() {
        return $this->hasMany(Noveltie::class);
    }

    public function supplieIncomes() {
        return $this->hasMany(SupplieIncome::class);
    }

    public function supplieProviders() {
        return $this->hasMany(SupplieProvider::class);
    }

    public function replacements() {
        return $this->hasMany(Replacement::class);
    }

    public function replacementComments() {
        return $this->hasMany(ReplacementComment::class);
    }

    public function UserMedicals() {
        return $this->hasMany(UserMedical::class);
    }

    public function provider() {
        return $this->belongsTo(Provider::class, 'provider_id');
    }

    public function country() {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function applicationSystems() {
        return $this->belongsToMany(ApplicationSystem::class, 'user_has_application_system');
    }



    // scope
    function scopeEmmailUser($query, $email){
        return $query->where('email', $email);
    }

    public function scopeAdmin($query)
    {
        return $query->where('id','<>', 1);
    }

    public function scopeHandleProvider($query, $id)
    {
        return $query->where('provider_id', $id);
    }

    public function scopeByUserName($query, $user_name)
    {
        return $query->where('user_name', $user_name);
    }

}
