<?php

namespace App\Models;

use App\Http\Helpers\UtilHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EducationTheme extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'education_themes';

    protected $fillable = [
        'name',
        'status',
    ];

    const NAME = [
        'GM1 Nutricion',
        'GM2 Auto Cuidado',
        'GM3 Afrontamiento Enfermedades Cronicas',
        'GM4 Derechos y deberes en la salud de los pacientes',
        'GM5 Actitud positiva frente a la enfermedad',
        'GM6 Inteligencia emocional',
        'GM7 Barreras mentales',
        'GM8 Te cuido, me cuido',
        'GM9 Resiliencia',
        'GM10 Apoyo familiar a pacientes con enfermedad cronica',
        'GM11 Regulacion emocional',
        'GM12 Programacion neurolinguistica',
        'GM13',
        'GM14',
        'GM15',
    ];


    public function treatments() {
        return $this->hasMany(Treatment::class);
    }
  
    // scope
    public function scopeActive($query){
        return $query->where('status', UtilHelper::ACTIVE);
    }

}
