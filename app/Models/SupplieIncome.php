<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupplieIncome extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'supplie_incomes';

    protected $fillable = [
        'referral_number',
        'supplie_id',
        'quantity',
        'user_id',
        'supplie_incomes_date',
        'supplier',
        'product_image',
        'observations',
        'status',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function supplie() {
        return $this->belongsTo(Supplie::class, 'supplie_id');
    }

    public function supplieExpenses() {
        return $this->hasMany(SupplieExpense::class, 'supplie_income_id', 'id');
    }
    //Scope

    public function scopeReferral($query, $referral_number){
        return $query->where('referral_number', $referral_number);
    }

    public function scopeAvailable($query){
        return $query->where('quantity_available', '>', 0);
    }
}
