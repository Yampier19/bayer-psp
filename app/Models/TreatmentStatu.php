<?php

namespace App\Models;

use App\Http\Helpers\UtilHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TreatmentStatu extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'treatment_status';

    protected $fillable = [
        'name',
        'product_id',
        'status',
    ];

    const NAME = [
        'Alto riesgo I',
        'Alto riesgo II',
        'Mantenimiento I',
        'Mantenimiento II',
        'Paciente nuevo',
    ];

    public function treatment() {
        return $this->hasMany(Treatment::class);
    }

    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', UtilHelper::ACTIVE);
    }

    public function scopeHandleProduct($query, $id){
        return $query->where('product_id', $id);
    }
}
