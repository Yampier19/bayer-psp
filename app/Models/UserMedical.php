<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserMedical extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'user_medical';

    protected $fillable = [
        'activation_date',
        'full_name',
        'document_type',
        'document_number',
        'address',
        'email',
        'gender',
        'date_birth',
        'city_id',
        'user_id',
        'status',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function city() {
        return $this->belongsTo(City::class, 'city_id');
    }
}
