<?php

namespace App\Models;

use App\Http\Helpers\UtilHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PathologicalClassification extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'pathological_classifications';

    protected $fillable = [
        'name',
        'product_id',
        'country_id',
        'status',
    ];

    const NAME = [
        'Hipertension Arterial Pulmonar',
        'Hipertension Pulmonar Tronboembolica cronica',
        'Por confirmar con paciente',
        'Carcinoma de Celulas Renales Avanzado',
        'Carcinoma de tiroides diferenciado, avanzado refractario a yodo radioactivo',
        'Paciente no recuerda Clasificacion Patologica',
        'Hemofilia A (clasica) Profilaxis del sangrado',
        'Edema Macular Diabetico',
        'Por confirmar con paciente',
        'Hipertension Arterial Pulmonar',
    ];

    public function treatment() {
        return $this->hasMany(Treatment::class);
    }

    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function country() {
        return $this->belongsTo(Country::class, 'country_id');
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', UtilHelper::ACTIVE);
    }

    public function scopeHandleProduct($query, $id){
        return $query->where('product_id', $id);
    }

    public function scopeHandleCountry($query, $id){
        return $query->where('country_id', $id);
    }
}
