<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PatientPhone extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'patient_phones';

    protected $fillable = [
        'number',
        'status',
        'patient_id',
    ];

    public function patient() {
        return $this->belongsTo(Patient::class, 'patient_id');
    }
}
