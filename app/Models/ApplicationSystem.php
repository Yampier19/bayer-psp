<?php

namespace App\Models;

use App\Http\Helpers\UtilHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicationSystem extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'application_system';

    protected $fillable = [
        'name',
        'description',
        'status'
    ];

    const NAME = [
        'PSP',
        'CRS',
        'AD',
    ];

    const DESCRIPTION = [
        'Aplicativo PSP',
        'Programa de compensación y reembolso',
        'Apoyo Diagnóstico'
    ];

    public function users() {
        return $this->belongsToMany(User::class, 'user_has_application_system');
    }

    public function patients() {
        return $this->hasMany(Patient::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('activo', UtilHelper::ACTIVE);
    }

    // ACCESSOR
    public function getFullNameAttribute(){
        return $this->name.', '.$this->description;
    }
}
