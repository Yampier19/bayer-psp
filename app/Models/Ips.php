<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ips extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'eps';

    protected $fillable = [
        'name',
        'insurance_id',
        'status',
    ];

    const NAME = [
        'Coomeva',
        'Salud Total',
        'Colsanita',
        'Sanitas',
        'Coosalud',
    ];

    const ACTIVE = 'ACTIVO';

    public function insurance() {
        return $this->belongsTo(Insurance::class, 'insurance_id');
    }

    public function treatmentFollows() {
        return $this->belongsTo(TreatmentFollow::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', Ips::ACTIVE);
    }

    public function scopeInsurance($query, $id){
        return $query->where('insurance_id', $id);
    }

}
