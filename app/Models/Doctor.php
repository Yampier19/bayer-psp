<?php

namespace App\Models;

use App\Http\Helpers\UtilHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Doctor extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'doctors';

    protected $fillable = [
        'name',
        'status',
        'user_id',
    ];

    const NAME = [
        'Doctor Manuel Pertuz',
        'Doctor Ricardo Ramirez',
        'Doctora Angelica Sanchez',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function treatments() {
        return $this->hasMany(Treatment::class);
    }

    public function specialtys() {
        return $this->belongsToMany(Specialty::class, 'doctor_has_specialty');
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', UtilHelper::ACTIVE);
    }

}
