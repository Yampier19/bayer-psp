<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Exceptions\NonExistentException;

class DiagnosticSupportPsP extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'diagnostic_support_psp';

    protected $fillable = [
        'PAP',
        'product_id',
        'treatment_id',
        'quantity_exams',
        'voucher_number',
        'diagnostic_medical_center',
        'file',
        'management',
        'status'
    ];
    const ACTIVE = 'ACTIVO';

    //Relations
    public function exams(){
        return $this->hasMany(ExamDiagnosticSupportPsP::class, 'diagnostic_support_id');
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function treatment(){
        return $this->belongsTo(Treatment::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', DiagnosticSupportPsP::ACTIVE);
    }

    public function scopeGetTreatment($query, $treatment_id){
        return $query->where('treatment_id', $treatment_id);
    }
    
}
