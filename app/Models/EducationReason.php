<?php

namespace App\Models;

use App\Http\Helpers\UtilHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EducationReason extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'education_reason';

    protected $fillable = [
        'name',
        'status',
    ];

    const NAME = [
        'No permite brindar información',
        'Solicita que sea de forma presencial',
        'No acepta visita',
        'Solicita envio por Email',
        'No Interesada'
    ];


    public function treatments() {
        return $this->hasMany(Treatment::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', UtilHelper::ACTIVE);
    }
}
