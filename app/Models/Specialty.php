<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Specialty extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'specialtys';

    protected $fillable = [
        'name',
        'status',
    ];

    const NAME = [
        'Endocrinología',
        'Reumatología',
        'Hepatología',
        'Urología'
    ];

    public function doctors() {
        return $this->belongsToMany(Doctor::class, 'doctor_has_specialty');
    }

    public function treatments() {
        return $this->hasMany(Treatment::class);
    }

}
