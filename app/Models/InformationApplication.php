<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InformationApplication extends Model
{
    use HasFactory;

    protected $table = 'information_applications';

    protected $fillable = [
        "id",
        "where_eye",
        "have_application",
        "date_application",
        "reason_for_non_application",
        "treatment_id",
        "status"
    ];

}
