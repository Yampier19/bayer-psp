<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupplieExpense extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'supplie_expenses';

    protected $fillable = [
        'supplie_income_id',
        'patient_id',
        'quantity',
        'user_id',
        'supplie_expenses_date',
        'third_party_for_the_distribution_of_the_product',
        'supplier_name',
        'observations',
        'from_follow',
        'product_image',
        'status',
    ];

    public function patient() {
        return $this->belongsTo(Patient::class, 'patient_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function supplie_income() {
        return $this->belongsTo(SupplieIncome::class, 'supplie_income_id');
    }

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function product_doses(){
        return $this->belongsTo(ProductDose::class, 'product_doses_id');
    }

    public function scopePatient($query, $patient_id){
        return $query->where('patient_id', $patient_id);
    }

    public function scopeMedicineWithoutCommercialValue($query){
        return $query->where('medicine_without_commercial_value', 'SI');
    }
}
