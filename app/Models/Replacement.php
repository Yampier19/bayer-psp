<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Replacement extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'replacements';

    protected $fillable = [
        'doctor_code',
        'crs_code',
        'full_name',
        'address',
        'application_date',
        'lote',
        'argus_code',
        'qualification',
        'replacement_date',
        'repositioning_done',
        'repositioning_done_date',
        'email',
        'adverse_event_date',
        'done',
        'product_id',
        'city_id',
        'user_id',
        'status',
    ];

    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function city() {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function replacementComments() {
        return $this->hasMany(ReplacementComment::class);
    }

    // scope

}
