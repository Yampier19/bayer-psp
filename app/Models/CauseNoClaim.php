<?php

namespace App\Models;

use App\Http\Helpers\UtilHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CauseNoClaim extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'cause_no_claims';

    protected $fillable = [
        'name',
        'status',
    ];


    const NAME = [
        'Autorizacion radicada para Cita',
        'Autorizacion radicada para Examenes',
        'Autorizacion radicada para Medicamento',
        'Demora en la Autorizacion Prescripcion',
        'Demora en la Autorizacion Cita Medica',
        'Demora en la autorizacion de medicamento',
        'Error en Papeleria',
        'Falta de medicamento en el punto',
        'Falta cita para examenes',
        'Pendiente Solicitar Cita',
        'Pendiente Subir Formula A Mipres',
        'Sin red prestadora',
        'Cita inoportuna',
        'Falta de cita Reformulacion',
        'Falta de cita Aplicacion',
        'Falta de cita Valoracion',
        'Pago Anticipado',
        'En proceso de Reformulacion',
        'En proceso de Aplicacion',
        'En proceso de entrega',
        'No remision a entidad licenciada',
        'Desafiliacion Al sistema de Salud',
        'Falta de contacto',
        'ILocalizable',
        'Suspendido temporalmente',
        'Titulacion',
        'Voluntario',
        'Abandono',
        'Stock',
        'Suspendido por esquema de Aplicacion',
        'Falta de pago anticipado de EPS a IPS / OPL',
        'A demanda',
        'Traslado de Asegurador',
        'Pendiente Radicar Formula en Farmacia',
    ];

    public function treatments() {
        return $this->hasMany(Treatment::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', UtilHelper::ACTIVE);
    }
}
