<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClaimsHistory extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'claims_history';

    protected $fillable = [
        'claim',
        'claim_date',
        'reason',
        'cause_no_claim_id',
        'treatment_id',
        'status'
    ];

    const ACTIVE = 'ACTIVO';

    public function causeNoClaim() {
        return $this->belongsTo(CauseNoClaim::class, 'cause_no_claim_id');
    }

    public function treatment() {
        return $this->belongsTo(Treatment::class, 'treatment_id');
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', ClaimsHistory::ACTIVE);
    }

    public function scopeHandleTreatement($query, $id){
        return $query->where('treatment_id', $id);
    }
}
