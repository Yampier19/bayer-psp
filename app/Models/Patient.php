<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Patient extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'patients';

    protected $fillable = [
        'name',
        'last_name',
        'document_number',
        'date_active',
        'email',
        'city_id',
        'user_id',
        'patient_statu_id',
        'consecutive_application_system',
        'application_system_id',
        'address',
        'neighborhood',
        'gender',
        'date_birth',
        'guardian',
        'guardian_phone',
        'note',
        'image',
        'age',
        'document_type',
        'change_state_patient',
        'retirement_date',
        'retirement_reason',
        'retirement_reason_observations',
        'status'
    ];

    const ACTIVE = 'ACTIVO';


    public function city() {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function patientStatu() {
        return $this->belongsTo(PatientStatu::class, 'patient_statu_id');
    }

    public function applicationSystem() {
        return $this->belongsTo(ApplicationSystem::class, 'application_system_id');
    }

    public function patientPhones() {
        return $this->hasMany(PatientPhone::class);
    }

    public function treatments() {
        return $this->hasMany(Treatment::class);
    }

    public function supplieExpenses() {
        return $this->hasMany(SupplieExpense::class);
    }

    public function claimsHistory() {
        return $this->hasMany(ClaimsHistory::class);
    }

    public function attachedFiles() {
        return $this->hasMany(AttachedFile::class);
    }

    // scope
    public function scopeActive($query){
        return $query->where('status', Patient::ACTIVE);
    }

    public function scopeGetSystemApp($query, $system_app){
        return $query->where('application_system_id', $system_app);
    }

    public function scopeGetId($query, $id){
        return $query->where('id','<>', $id);
    }

    public function scopeHandleDocument($query, $document){
        return $query->where('document_number', 'like', '%'.$document.'%');
    }

    public function scopeHandleCity($query, $city_id){
        return $query->where('city_id', $city_id);
    }

    public function scopeDocumentNumber($query, $PAP){
        return $query->where('document_number', $PAP);
    }
}
